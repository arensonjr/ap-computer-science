package forum;

import provided.WebHandler;

import java.util.List;
import java.util.Map;

/**
 * Handler to manage posting and listing forum content.
 */
public class ForumHandler extends WebHandler {
  /**
   * The post in this forum (if any). The forum can only hold one post at a
   * time right now.
   */
  private String post;

  public ForumHandler() {
    // Initially there are no posts in the forum.
    post = "";
  }

  /**
   * This handler handles _all_ URLs for the forum.
   */
  @Override
  public String getPrefix() {
    return "";
  }

  /**
   * Handle a request to the forum.
   *
   *   - If the URL is "getposts", return the post on this forum.
   *   - If the URL is "newpost", add a new post to this forum.
   */
  @Override
  public String handle(List<String> url, Map<String, String> data) {
    // Ignore the root URL (no prefix).
    if (url.size() == 0) {
      return "";
    }

    if (url.get(0).equals("getposts")) {
      return post;
    }

    if (url.get(0).equals("newpost")) {
      String newPost = data.get("forumpost");
      post = newPost;
      return "";
    }

    // If we didn't already handle the URL, then I don't know what else to do!
    // Throw an exception (error), and let the web server figure it out.
    throw new RuntimeException("Unrecognized URL: " + url);
  }
}
