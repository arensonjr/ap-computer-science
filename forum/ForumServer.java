package forum;

import provided.StaticFilesHandler;
import provided.WebHandler;
import provided.WebServer;

import java.util.ArrayList;
import java.util.List;

/**
 * Java server to host a website for a basic forum.
 */
public class ForumServer {
  public static void main(String[] args) throws Exception {
    List<WebHandler> handlers = new ArrayList<>();
    handlers.add(new StaticFilesHandler("forum/static"));
    handlers.add(new ForumHandler());
    WebServer server = new WebServer(handlers, "static/forum.html");
  }
}
