/**
 * This is a Javascript file!
 *
 * Parts of it look very much like Java, but they're not quite the same. The
 * syntax is slightly different, there are no "classes", and Javascript has
 * lots of extra helper functions that will help to interact with webpages
 * (everything using "document." and "window.").
 */

// First thing's first: Let's look up the most recent post to the forum.
function refreshPosts() {
  // Make a new request to the URL 'getposts'
  $.ajax({
    url: '/getposts',
    method: 'GET',
    success: function(response) {
      // We got a post from the server! If it's nonempty, put it on the page.
      var postDiv = $('#post');
      if (response === "") {
        postDiv.text("(no posts yet)");
      } else {
        postDiv.text(decodeURIComponent(response));
      }
    }
  });
};
// Immediately call this function, so that the post is loaded when the page is
// loaded.
refreshPosts();


// Second: Let's refresh the posts as soon as a user posts.
$('#newpostbutton').click(function() {
  var newPost = $('#newpost').text();

  // URL-encode the data so it's safe to send in a request
  newPost = encodeURIComponent(newPost);

  // Send the data
  $.ajax({
    url: '/newpost',
    method: 'POST',
    data: { forumpost: newPost },
    success: function() {
      // When it's ready, refresh the posts so we show the newest one.
      refreshPosts();
    }
  });
});
