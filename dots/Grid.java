package dots;

/**
 * Represents a grid of squares and edges for the game of Dots.
 */
public class Grid {
  public Grid(int height, int width) {
    // Initialize your instance variables here.
  }

  /**
   * Returns the board's height.
   */
  public int getHeight() {
    // Write your code here.
    return -1;
  }

  /**
   * Returns the board's width.
   */
  public int getWidth() {
    // Write your code here.
    return -1;
  }

  /**
   * Updates the board by claiming a particular edge for the current player.
   */
  public void claimEdge(int row, int column, char player, int side) {
    // Write your code here.
  }

  /**
   * Gets which player owns a particular edge.
   */
  public char getEdge(int row, int column, int side) {
    // Write your code here.
    // e.g.:
    //     return Provided.PLAYER_UNCLAIMED;
    return 'z';
  }

  /**
   * Gets which player owns a particular square.
   */
  public char getCenter(int row, int column) {
    // Write your code here.
    // e.g.:
    //     return Provided.PLAYER_UNCLAIMED;
    return 'z';
  }
}
