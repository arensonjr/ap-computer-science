package dots;

/**
 * Provided functions and constants for the 'dots' miniproject.
 */
public class Provided {
  public static final char PLAYER_UNCLAIMED = '?';
  public static final char PLAYER_ONE = 'X';
  public static final char PLAYER_TWO = 'O';

  public static final int LEFT_SIDE = 10;
  public static final int RIGHT_SIDE = 20;
  public static final int TOP_SIDE = 30;
  public static final int BOTTOM_SIDE = 40;
}
