const React = require("react");


const Grid = React.createClass({
  render() {
    // We're gonna iterate over a _giant_ grid, of
    // edges, nodes, and squares.
    const numRows = this.props.height * 2 + 1;
    const numCols = this.props.width * 2 + 1;
    const rows = [];
    for (let r = 0; r < numRows; r++) {
      const rowIndex = Math.floor(r / 2);
      const row = [];
      for (let c = 0; c < numCols; c++) {
        // What is this cell? Is it a node,
        // horizontal edge, vertical edge, or square?
        let CurrentComponent;
        if (r % 2 === 0 && c % 2 === 0) {
          CurrentComponent = Node;
        } else if (r % 2 === 0 && c % 2 === 1) {
          CurrentComponent = HorizontalEdge;
        } else if (r % 2 === 1 && c % 2 === 0) {
          CurrentComponent = VerticalEdge;
        } else if (r % 2 === 1 && c % 2 === 1) {
          CurrentComponent = Square;
        }

        // Render that type of thing, passing it
        // the current row/col and all of our props
        // (like game state and event callbacks).
        row.push(<CurrentComponent
          key={c}
          rowIndex={Math.floor(r / 2)}
          colIndex={Math.floor(c / 2)}
          {...this.props}
        />);
      }
      rows.push(<div key={r}>{row}</div>);
    }

    return <div>
      <div className="grid">{rows}</div>
      <p>It's <strong style={{
        color: this.props.gameState.currentPlayer,  // hahahaha hax
      }}>{this.props.gameState.currentPlayer}</strong>'s turn!</p>
    </div>;
  }
});

const Node = React.createClass({
  render() {
    return <div className="node" />;
  }
});


const HorizontalEdge = React.createClass({
  handleClick() {
    this.props.onHorizontalEdgeClick(this.props.rowIndex, this.props.colIndex);
  },

  render() {
    var owner = this.props.gameState.horizontalEdgesOccupied[this.props.rowIndex][this.props.colIndex];
    return <button
      className="horizontal-edge"
      data-owner={owner}
      disabled={owner != '?'}
      onClick={this.handleClick}
    />;
  }
});


const VerticalEdge = React.createClass({
  handleClick() {
    this.props.onVerticalEdgeClick(this.props.rowIndex, this.props.colIndex);
  },

  render() {
    var owner = this.props.gameState.verticalEdgesOccupied[this.props.rowIndex][this.props.colIndex];
    return <button
      className="vertical-edge"
      disabled={owner != '?'}
      data-owner={owner}
      onClick={this.handleClick}
    />;
  }
});


const Square = React.createClass({
  render() {
    return <div
      className="square"
      data-owner={this.props.gameState.squareOwners[this.props.rowIndex][this.props.colIndex]}
    />;
  },
});


module.exports = Grid;
