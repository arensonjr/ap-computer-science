const React = require("react");
const ReactDOM = require("react-dom");

const GridController = require("./grid-controller.jsx");
require("./dots.css");


const target = document.getElementById("target");
ReactDOM.render(<GridController
    gameToken={target.getAttribute("data-game-token")}
    width={target.getAttribute("data-board-size")}
    height={target.getAttribute("data-board-size")}
/>, target);
