const React = require("react");

const Grid = require("./grid.jsx");

/// Helper function (create matrices) ///
var matrix = function(val, width, height) {
  var mat = [];
  for (var row = 0; row < height; row++) {
    var newRow = [];
    for (var col = 0; col < width; col++) {
      newRow.push(val);
    }
    mat.push(newRow);
  }
  return mat;
}


const GridController = React.createClass({
  getInitialState() {
    return {
      horizontalEdgesOccupied: matrix('?', this.props.width, this.props.height + 1),
      verticalEdgesOccupied: matrix('?', this.props.width + 1, this.props.height),
      squareOwners: matrix(null, this.props.width, this.props.height),
      currentPlayer: "red",
    };
  },
  occupyHorizontalEdge(rowIndex, colIndex) {
    console.log('occupying horiz edge (' + rowIndex + ', ' + colIndex + ')');
    if (rowIndex == this.props.height) {
      this.occupy(this.props.height-1, colIndex, 40); // bottom
    } else {
      this.occupy(rowIndex, colIndex, 30); // top
    }
  },
  occupyVerticalEdge(rowIndex, colIndex) {
    console.log('occupying vert edge (' + rowIndex + ', ' + colIndex + ')');
    if (colIndex == this.props.width) {
      this.occupy(rowIndex, this.props.width-1, 20); // right
    } else {
      this.occupy(rowIndex, colIndex, 10); // left
    }
  },
  occupy(row, col, side) {
    var req = new XMLHttpRequest();
    req.open('GET', '/move?' + [this.props.gameToken, row, col, side].join('&'), true);
    req.onreadystatechange = function() {
      if (req.readyState === 4 && req.status !== 200) {
        alert("Uh oh! Looks like your code doesn't work. Check out the Cloud9 console.");
      } else if (req.readyState === 4 && req.status === 200) {
        this.setState(JSON.parse(req.responseText));
      }
    }.bind(this);
    req.send();
  },
  render() {
    return <Grid
      width={this.props.width}
      height={this.props.height}
      onHorizontalEdgeClick={this.occupyHorizontalEdge}
      onVerticalEdgeClick={this.occupyVerticalEdge}
      gameState={this.state}
    />;
  }
});


module.exports = GridController;
