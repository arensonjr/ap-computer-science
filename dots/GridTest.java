package dots;

import dots.Grid;

/**
 * Simple test cases to check if your Grid class is working.
 */
public class GridTest {
    public static void main(String[] args) {
        // Uncomment whichever test you want to run
        // testGetHeight();
        // testGetWidth();
        // testGetEdgeUnclaimed();
        // testClaimAndGetEdge();
    }

    /**
     * Tests whether the height is correctly set in the constructor and returned
     * by getHeight()
     */
    public static void testGetHeight() {
        Grid g = new Grid(5, 4);
        int height = g.getHeight();

        if(height == 5) {
            System.out.println("PASS (testGetHeight): Height is 5, as expected");
        } else {
            System.out.println("FAIL (testGetHeight): Expected height to be 5,"
                + " but instead got " + height + ".");
        }
    }

    /**
     * Tests whether the width is correctly set in the constructor and returned
     * by getWidth()
     */
    public static void testGetWidth() {
        Grid g = new Grid(5, 4);
        int width = g.getWidth();

        if(width == 4) {
            System.out.println("PASS (testGetWidth): Width is 4, as expected");
        } else {
            System.out.println("FAIL (testGetWidth): Expected width to be 4,"
                + " but instead got " + width + ".");
        }
    }

    /**
     * Tests that initially, all spaces are unclaimed, and getEdge returns
     * accordingly.
     */
     public static void testGetEdgeUnclaimed() {
         String incorrectEdges = "";
         Grid g = new Grid(2, 2);
         for (int row = 0; row < 2; row++) {
             for (int column = 0; column < 2; column++) {
                 if(g.getEdge(row, column, Provided.LEFT_SIDE) != Provided.PLAYER_UNCLAIMED) {
                     incorrectEdges +=  "\t(" + row + ", " + column + ") LEFT\n";
                 }
                 if(g.getEdge(row, column, Provided.RIGHT_SIDE) != Provided.PLAYER_UNCLAIMED) {
                     incorrectEdges +=  "\t(" + row + ", " + column + ") RIGHT\n";
                 }
                 if(g.getEdge(row, column, Provided.TOP_SIDE) != Provided.PLAYER_UNCLAIMED) {
                     incorrectEdges +=  "\t(" + row + ", " + column + ") TOP\n";
                 }
                 if(g.getEdge(row, column, Provided.BOTTOM_SIDE) != Provided.PLAYER_UNCLAIMED) {
                     incorrectEdges +=  "\t(" + row + ", " + column + ") BOTTOM\n";
                 }
             }
         }
         if(incorrectEdges.equals("")) {
             System.out.println("PASS (testGetEdgeUnclaimed): All edges unclaimed.");
         } else {
             System.out.println("FAIL (testGetEdgeUnclaimed): Edges below are incorrect:");
             System.out.println(incorrectEdges);
         }
     }

    /**
     * Tests that if I call claimEdge for player X, getEdge now returns 'X'.
     */
    public static void testClaimAndGetEdge() {
        Grid g = new Grid(3, 3);
        g.claimEdge(0, 2, Provided.PLAYER_ONE, Provided.TOP_SIDE);
        char result = g.getEdge(0, 2, Provided.TOP_SIDE);
        if(result == Provided.PLAYER_ONE) {
            System.out.println(
                "PASS (testClaimAndGetEdge): Edge was "
                + Provided.PLAYER_ONE + " as expected.");
        } else {
            System.out.println(
                "FAIL (testClaimAndGetEdge): Expected edge to be '"
                + Provided.PLAYER_ONE + "' but received '" + result + "'.");
        }
    }

    // Add your own test cases here!
    // eg. Write a test for getCenter
}
