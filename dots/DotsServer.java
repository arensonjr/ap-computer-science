package dots;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import dots.Provided;

import java.io.IOException;
import java.math.BigInteger;
import java.net.InetSocketAddress;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Random;

/**
 * Java server to host a website for the Dots game.
 */
public class DotsServer {
  public static void main(String[] args) throws Exception {
    HttpServer server = HttpServer.create();
    server.createContext("/", new NewGameHandler());
    server.createContext("/move", new MoveHandler());
    server.createContext("/static", new StaticFilesHandler());

    int port = Integer.parseInt(System.getenv("C9_PORT"));
    String ip = System.getenv("C9_IP");
    server.bind(new InetSocketAddress(ip, port), port);

    server.start();
    String hostname = System.getenv("C9_HOSTNAME");
    System.out.format("Running at http://%s\n", hostname);
  }

  /**
   * The bundle of Javascript/CSS.
   */
  private static final Path JS = Paths.get("dots", "js", "bundle.js");

  /**
   * The HTML template for the page.
   */
  private static final Path TEMPLATE = Paths.get("dots", "static", "dots_game.html");

  /**
   * All currently running games.
   *
   * TODO(arensonjr): Figure out how to evict these, otherwise a long-running server will just eat RAM?
   */
  private static final HashMap<String, GameState> games = new HashMap<>();
  private static class GameState {
    public Grid game;
    public char currPlayer;
    public GameState(Grid g) {
      game = g;
      currPlayer = Provided.PLAYER_ONE;
    }
  }

  /**
   * For generating new game IDs.
   */
  private static final Random random = new Random();

  /**
   * Generates a new game ID.
   */
  public static String newGameId() {
    return new BigInteger(130, random).toString();
  }

  /**
   * Handle requests for the Dots homepage, by starting a new game.
   */
  private static class NewGameHandler extends AbstractHandler {
    @Override public String doWork(HttpExchange exchange) throws IOException {
      // Create a new game
      int gridSize = 3;
      URI url = exchange.getRequestURI();
      if (url.getQuery() != null) {
        System.err.println("Query is " + url.getQuery());
        System.err.println("Matches? " + url.getQuery().matches("boardsize=[0-9]+"));
        if (url.getQuery().matches("boardsize=[0-9]+")) {
          gridSize = Integer.parseInt(url.getQuery().substring("boardsize=".length()));
        }
      }
      String gameId = newGameId();

      Grid game = new Grid(gridSize, gridSize);
      games.put(gameId, new GameState(game));

      // Serialize the game
      String template =
        new String(Files.readAllBytes(TEMPLATE), StandardCharsets.UTF_8);
      return template
          .replaceAll("\\$\\{boardsize\\}", "" + gridSize)
          .replaceAll("\\$\\{gameid\\}", gameId);
    }
  }

  /**
   * Handle AJAX requests for players making a move.
   */
  private static class MoveHandler extends AbstractHandler {
    @Override public String doWork(HttpExchange exchange) throws IOException {
      String[] body = exchange.getRequestURI().getQuery().split("&");
      String gameId = body[0];
      int moveRow = Integer.parseInt(body[1]);
      int moveCol = Integer.parseInt(body[2]);
      int side = Integer.parseInt(body[3]);

      GameState state = games.get(gameId);
      Grid game = state.game;
      if (game == null) {
        throw new IOException("Unknown game (try refreshing the page)");
      }

      game.claimEdge(moveRow, moveCol, state.currPlayer, side);

      // Also claim the neighboring square, if there is one
      Neighbor neighbor = maybeNeighbor(game, moveRow, moveCol, side);
      game.claimEdge(neighbor.getRow(), neighbor.getCol(), state.currPlayer, neighbor.getSide());

      // If no squares were claimed, change players
      if (game.getCenter(moveRow, moveCol) == Provided.PLAYER_UNCLAIMED
          && game.getCenter(neighbor.getRow(), neighbor.getCol()) == Provided.PLAYER_UNCLAIMED) {
        if (state.currPlayer == Provided.PLAYER_ONE) {
          state.currPlayer = Provided.PLAYER_TWO;
        } else {
          state.currPlayer = Provided.PLAYER_ONE;
        }
      }

      // Serialize the game
      StringBuilder page = new StringBuilder();
      String[][] horizEdges = new String[game.getHeight() + 1][game.getWidth()];
      String[][] vertEdges = new String[game.getHeight()][game.getWidth() + 1];
      String[][] squareOwners = new String[game.getHeight()][game.getWidth()];
      for (int row = 0; row < game.getHeight(); row++) {
        for (int col = 0; col < game.getWidth(); col++) {
          squareOwners[row][col] = chr(game.getCenter(row, col));

          // Sanity Check
          String top = chr(game.getEdge(row, col, Provided.TOP_SIDE));
          if (row > 0 && !horizEdges[row][col].equals(top)) {
            throw new IllegalStateException(String.format(
                "Edges should be the same, but they don't match!\n"
                + "\t(%d,%d)=Square{bottom=%s}\n"
                + "\t(%d,%d)=Square{top=%s}",
                row - 1, col, horizEdges[row][col],
                row, col, top));
          } else if (row == 0) {
            horizEdges[0][col] = top;
          }
          String left = chr(game.getEdge(row, col, Provided.LEFT_SIDE));
          if (col > 0 && !vertEdges[row][col].equals(left)) {
            throw new IllegalStateException(String.format(
                "Edges should be the same, but they don't match!\n"
                + "\t(%d,%d)=Square{right=%s}\n"
                + "\t(%d,%d)=Square{left=%s}",
                row, col - 1, vertEdges[row][col],
                row, col, left));
          } else if (col == 0) {
            vertEdges[row][0] = left;
          }

          // Continue filling
          horizEdges[row + 1][col] = chr(game.getEdge(row, col, Provided.BOTTOM_SIDE));
          vertEdges[row][col + 1] = chr(game.getEdge(row, col, Provided.RIGHT_SIDE));
        }
      }

      page.append("{\"horizontalEdgesOccupied\":")
        .append(Arrays.deepToString(horizEdges))
        .append(",\"verticalEdgesOccupied\":")
        .append(Arrays.deepToString(vertEdges))
        .append(",\"squareOwners\":")
        .append(Arrays.deepToString(squareOwners))
        .append(",\"currentPlayer\":\"")
        .append(state.currPlayer == Provided.PLAYER_ONE ? "Red" : "Blue")
        .append("\"}");

      return page.toString();
    }

    /**
     * Represents a neighbor of an edge.
     */
    private static class Neighbor {
      int row;
      int col;
      int side;
      public Neighbor(int r, int c, int s) {
        row = r;
        col = c;
        side = s;
      }
      public int getRow() { return row; }
      public int getCol() { return col; }
      public int getSide() { return side; }
    }

    /**
     * Returns the x=row, y=col neighbor of this point on the given side.
     *
     * Returns the original point if there is no neighbor.
     */
    private Neighbor maybeNeighbor(Grid game, int row, int col, int side) {
      Neighbor neighbor = new Neighbor(row, col, side);
      if (side == Provided.BOTTOM_SIDE && row < game.getHeight() - 1) {
        neighbor = new Neighbor(row + 1, col, Provided.TOP_SIDE);
      } else if (side == Provided.TOP_SIDE && row > 0) {
        neighbor = new Neighbor(row - 1, col, Provided.BOTTOM_SIDE);
      } else if (side == Provided.RIGHT_SIDE && col < game.getWidth() - 1) {
        neighbor = new Neighbor(row, col + 1, Provided.LEFT_SIDE);
      } else if (side == Provided.LEFT_SIDE && col > 0) {
        neighbor = new Neighbor(row, col - 1, Provided.RIGHT_SIDE);
      }
      return neighbor;
    }
  }

  /**
   * JSONifies a character.
   */
  private static String chr(char c) {
    return "\"" + c + "\"";
  }

  /**
   * Serves static files.
   */
  private static final class StaticFilesHandler extends AbstractHandler {
    @Override public String doWork(HttpExchange exchange) throws IOException {
      return new String(Files.readAllBytes(JS), StandardCharsets.UTF_8);
    }
  }

  /**
   * Class that does HTTP error handling.
   */
  private static abstract class AbstractHandler implements HttpHandler {
    @Override public void handle(HttpExchange exchange) throws IOException {
      URI url = exchange.getRequestURI();
      try {
        byte[] pageBytes = doWork(exchange).getBytes();
        exchange.sendResponseHeaders(200, pageBytes.length);
        exchange.getResponseBody().write(pageBytes);
        exchange.close();
        System.err.format("[200] [%s] OK\n", url);
      } catch (Throwable t) {
        t.printStackTrace();
        exchange.sendResponseHeaders(500, 0);
        exchange.close();
        System.err.format("[500] [%s] Error: %s\n", url, t);
      }
    }

    /**
     * Actually does work.
     */
    public abstract String doWork(HttpExchange exchange) throws IOException;
  }
}
