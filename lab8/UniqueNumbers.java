package lab8;

// Note: This should be really cool! We're finally getting to reuse some of the
//       code that we wrote earlier in the class. Do you remember writing the
//       ParseInput class? Well, we're going to make use of it all over again to
//       read the integers for this program.
//
//       We're using the lab solutions here by default, because we don't want
//       to assume that you finished the lab (and had everything correct).
import lab4.solutions.ParseInput;

import java.util.ArrayList;

/**
 * Takes a list of integers as input, and prints out only those that are unique (only appear once).
 */
class UniqueNumbers {
  public static void main(String[] args) {
    // Write your code here.
  }
}

