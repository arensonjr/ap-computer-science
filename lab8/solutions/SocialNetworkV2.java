package lab8.solutions;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Prints out a list of the input person's friends
 */
public class SocialNetworkV2 {
  public static void main(String[] args) {
    // Build the friendship graph
    HashMap<String, ArrayList<String>> friendships = buildFriendshipGraph();

    // Print out the first input's friends
    String name = args[0];
    ArrayList<String> friends = friendships.get(name);
    System.out.println(friends);
  }

  /**
   * Builds the friendship graph, so we can use it to print people's friends.
   */
  private static HashMap<String, ArrayList<String>> buildFriendshipGraph() {
    HashMap<String, ArrayList<String>> friendships = new HashMap<>();

    // First, create a friendship list for each person
    friendships.put("Joe", new ArrayList<>());
    friendships.put("Bob", new ArrayList<>());
    friendships.put("Tracy", new ArrayList<>());
    friendships.put("Danny", new ArrayList<>());
    friendships.put("Sue", new ArrayList<>());
    friendships.put("Bill", new ArrayList<>());
    friendships.put("Englebert", new ArrayList<>());

    // 1. Joe is friends with Bob
    friendships.get("Joe").add("Bob");
    friendships.get("Bob").add("Joe");

    // 2. Joe is friends with Tracy
    friendships.get("Joe").add("Tracy");
    friendships.get("Tracy").add("Joe");

    // 3. Joe is friends with Sue
    friendships.get("Joe").add("Sue");
    friendships.get("Sue").add("Joe");

    // 4. Bob is friends with Tracy
    friendships.get("Bob").add("Tracy");
    friendships.get("Tracy").add("Bob");

    // 5. Bob is friends with Danny
    friendships.get("Bob").add("Danny");
    friendships.get("Danny").add("Bob");

    // 6. Tracy is friends with Bill
    friendships.get("Tracy").add("Bill");
    friendships.get("Bill").add("Tracy");

    // 7. Danny is friends with Bill
    friendships.get("Danny").add("Bill");
    friendships.get("Bill").add("Danny");

    // 8. (Englebert is friends with nobody) -- do nothing

    return friendships;
  }
}

