package lab8.solutions;

import lab8.Provided;

import java.util.ArrayList;

/**
 * Prints out all words from an input corpus that start with the specified letter.
 */
public class WordSearchBonus {
  public static void main(String[] args) {
    char firstCharacter = args[0].charAt(0);
    String corpusName = args[1];

    // Read in all of the words from the corpus
    ArrayList<String> words = Provided.getWords(corpusName);

    // Figure out which ones start with firstCharacter
    ArrayList<String> wordsWithFirstChar = new ArrayList<>();
    for (String word : words) {
      if (word.charAt(0) == firstCharacter) {
        // Only add the word if we haven't seen it before!
        if (!wordsWithFirstChar.contains(word)) {
          wordsWithFirstChar.add(word);
        }
      }
    }
    System.out.println(wordsWithFirstChar);
  }
}
