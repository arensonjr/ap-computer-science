package lab8.solutions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.TreeMap;
import java.util.Map;

/**
 * Provided code for Lab 7.
 */
public class Provided {
  /**
   * Pretty-prints a map of frequencies.
   *
   * For instance, if the input is
   *
   *   {1: 5, 10: 3, 100: 7, 1000: 2}
   *
   * then this function might print out something that looks like this:
   *
   *   |
   *   |                X
   *   |                X
   *   |    X           X
   *   |    X           X
   *   |    X     X     X
   *   |    X     X     X     X
   *   |    X     X     X     X
   *   +-----------------------------
   *        1     1     1     1
   *              0     0     0
   *                    0     0
   *                          0
   *
   * ---------------------
   * Note to students who are confused by this function:
   * ---------------------
   * Don't worry about it! This function is some advanced stuff. Just use it in your
   * code; you don't have to understand how it works.
   *
   * Oh, you want to learn anyway? Read on, then!
   *
   * This function takes a "Map" as input, not a "HashMap"! If this confuses you,
   * that's OK. For now, just know that "Map" is a variable type that applies to
   * all different types of maps (for instance, we use a TreeMap inside this function).
   * All maps share the same properties (lookup by key, associate keys with values,
   * etc.) but they're each implemented slightly differently. In this case, we're
   * using a TreeMap inside the function because it keeps the keys in a nice
   * stable order, whereas HashMaps might change the order of things without telling
   * you. Normally this doesn't matter, but we need things to stay in the same column
   * when we iterate multiple times in a row!
   */
  public static void printAsBarChart(Map<Integer, Integer> frequencies) {
    // The height of our chart should be the maximum value plus one (so that we can have
    // at least a little bit of empty space on top)
    int chartHeight = Collections.max(frequencies.values()) + 1;

    // We need a stable ordering for the keys in the map, so that we're not relying on
    // the random ordering provided by hash maps (for more info, see the javadoc comment)
    frequencies = new TreeMap<>(frequencies);

    // Unfortunately, printing output goes by *line*, not by column, so we have to get a
    // little tricky in order to print everything correctly. We need to go in layers down
    // from the top.
    //
    // From looking at the example in this function's Javadoc, we see that for each layer
    // (from the top down), we only print an X for each key whose value is at least this
    // tall. We'll iterate down from the top column and do that:
    for (int column = chartHeight; column > 0; column--) {
      // At the start of every row, print the left sidebar of the chart
      System.out.print('|');

      // You can iterate over the keys in a map the same way you iterate over a list or
      // array, as long as you call the ".keySet()" method on it. This references a new
      // data type, Sets, which we haven't talked about yet in class -- if you're curious,
      // go look up the Javadoc page for Set or HashSet.
      for (int key : frequencies.keySet()) {
        // Padding
        System.out.print("  ");

        Integer value = frequencies.get(key);
        if (value == null) {
          // Oops! You put a null into your map; this isn't OK, and it means
          // your program is wrong, so we're going to break and fail
          // immediately.
          throw new RuntimeException("Your map has a null value in it for the key " + key
              + "! Input map: " + frequencies);
        }

        // Only output an X if the bar is this tall
        if (value >= column) {
          System.out.print('X');
        } else {
          System.out.print(' ');
        }

        // Padding
        System.out.print("  ");
      }

      // After all the X's for this row have been printed, move on to the next line
      System.out.println();
    }

    // Cool, we finished the main body of the chart! Let's print out a divider
    // (with the right width, of course) to separate the bars from the legend (keys).
    System.out.print('+');
    printMultiple(frequencies.size(), "------");
    System.out.println();

    // Now we have to print out the legend along the bottom...
    //
    // We need to print them vertically, otherwise they might overlap with each other. This
    // is once again going to be pretty confusing, because printing numbers vertically is
    // hard.
    //
    // There are N rows for this part, where N is the number of digits of the largest key.
    int numRows = (int) Math.ceil(Math.log10(Collections.max(frequencies.keySet()))) + 1;
    for (int row = 0; row < numRows; row++) {
      // Padding at the beginning of the row (to make up for the extra '|' in the chart part)
      System.out.print(' ');

      // Go through each map key, and see if it's done yet
      for (int key : frequencies.keySet()) {
        // Padding
        System.out.print("  ");

        // Print out the row'th digit (if there is one; otherwise, print a
        // blank-space to keep the horizontal spacing correct)
        ArrayList<Integer> digits = getDigitsFor(key);
        if (digits.size() > row) {
          System.out.print(digits.get(row));
        } else {
          System.out.print(' ');
        }

        // Padding
        System.out.print("  ");
      }

      // After all of the digits for this row are printed, move on to the next line
      System.out.println();
    }
  }

  /**
   * Pretty-prints a map of frequencies.
   *
   * For instance, if the input is
   *
   *   {1: 5, 10: 3, 100: 7, 1000: 2}
   *
   * then this function might print out something that looks like this:
   *
   *        |
   *      1 | X X X X X
   *        |
   *     10 | X X X
   *        |
   *    100 | X X X X X X X
   *        |
   *   1000 | X X
   *        |
   *        +----------------
   *
   * ---------------------
   * Note to students who are confused by this function:
   * ---------------------
   * Don't worry about it! This function is some advanced stuff. Just use it in your
   * code; you don't have to understand how it works.
   *
   * Oh, you want to learn anyway? Read on, then!
   *
   * This function takes a "Map" as input, not a "HashMap"! If this confuses you,
   * that's OK. For now, just know that "Map" is a variable type that applies to
   * all different types of maps (for instance, we use a TreeMap inside this function).
   * All maps share the same properties (lookup by key, associate keys with values,
   * etc.) but they're each implemented slightly differently. In this case, we're
   * using a TreeMap inside the function because it keeps the keys in a nice
   * stable order, whereas HashMaps might change the order of things without telling
   * you. Normally this doesn't matter, but we need things to stay in the same column
   * when we iterate multiple times in a row!
   */
  public static void printAsHorizontalBarChart(Map<Integer, Integer> frequencies) {
    // Let's get all of the keys in order, which makes our chart prettier than if
    // we rely on HashMap's arbitrary ordering.
    frequencies = new TreeMap<>(frequencies);

    // How much padding do we need to the left? We need at least as many characters as
    // there are digits in the biggest number, plus one so we're not crammed right up
    // against the graph.
    int biggestKey = Collections.max(frequencies.keySet());
    int leftPadding = getDigitsFor(biggestKey).size() + 1;

    // Print each key/value frequency, one by onej
    for (int key : frequencies.keySet()) {
      Integer frequency = frequencies.get(key);
      if (frequency == null) {
        // Oops! You put a null into your map; this isn't OK, and it means
        // your program is wrong, so we're going to break and fail
        // immediately.
        throw new RuntimeException("Your map has a null value in it for the key " + key
            + "! Input map: " + frequencies);
      }

      // Print a blank line, to make it look nicer
      printMultiple(leftPadding, " ");
      System.out.println('|');

      // Now print the real line, with the number
      int numSpacesNeeded = leftPadding - 1 - getDigitsFor(key).size();
      printMultiple(numSpacesNeeded, " ");
      System.out.print(key + " |");

      // On the real line, we also need to print the right number of X's
      printMultiple(frequency, " X");
 
      // And now we can end the line.
      System.out.println();
    }

    // When we're done printing all of the rows, then print the bottom of the chart
    printMultiple(leftPadding, " ");
    System.out.print("+");
    printMultiple(Collections.max(frequencies.values()) + 1, "--");
  }

  /**
   * Internal helper function:
   * Prints N of the desired string, without going onto a new line.
   */
  private static void printMultiple(int n, String toPrint) {
    for (int i = 0; i < n; i++) {
      System.out.print(toPrint);
    }
  }

  /**
   * Internal helper function:
   * Returns a list of all of the digits of {@code number}, in order.
   */
  private static ArrayList<Integer> getDigitsFor(int number) {
    ArrayList<Integer> digits = new ArrayList<>();
    while (number > 0) {
      // We're building the list backwards, so we need to add all of the digits
      // to the front instead of the end (just because it's easier this way).
      digits.add(0, number % 10);
      number /= 10;
    }
    return digits;
  }
}
