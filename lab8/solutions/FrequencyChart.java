package lab8.solutions;

import lab4.solutions.ParseInput;

import lab8.Provided;

import java.util.HashMap;

/**
 * Prints the frequencies of each input integer.
 */
public class FrequencyChart {
  public static void main(String[] args) {
    // I want this main program to just worry about input and output (commonly referred to as I/O).
    //
    // All of the actual logic, the "smarts" of my program, is going to be inside of the helper method
    // computeFrequencies. I'm doing this so that I can reuse this code later if I want to
    // (hint, hint...).
    int[] numbers = ParseInput.asIntegers(args);
    HashMap<Integer, Integer> frequencies = computeFrequencies(numbers);
    Provided.printAsHorizontalBarChart(frequencies);
  }

  /**
   * Computes the frequencies of each number in the input array.
   */
  public static HashMap<Integer, Integer> computeFrequencies(int[] numbers) {
    // Create an empty container to count frequencies
    HashMap<Integer, Integer> frequencies = new HashMap<>();

    // Increment each number's count by 1 for each time we see it in the input array
    for (int number : numbers) {
      Integer previousCount = frequencies.get(number);

      // If we haven't seen this number before, then frequencies.get(number)
      // will return null! We need to be aware of this, and enter a count of one
      // instead of blatantly trying to add '1 + null' to get the new count.
      if (previousCount == null) {
        frequencies.put(number, 1);
      } else {
        frequencies.put(number, previousCount + 1);
      }
    }

    // We've finished counting all of the numbers, since our loop is over.
    return frequencies;
  }
}
