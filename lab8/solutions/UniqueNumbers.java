package lab8.solutions;

// Note: This should be really cool! We're finally getting to reuse some of the
//       code that we wrote earlier in the class. Do you remember writing the
//       ParseInput class? Well, we're going to make use of it all over again to
//       read the integers for this program.
//
//       We're using the lab solutions here by default, because we don't want
//       to assume that you finished the lab (and had everything correct).
import lab4.solutions.ParseInput;

import java.util.ArrayList;

/**
 * Takes a list of integers as input, and prints out only those that are unique (only appear once).
 */
class UniqueNumbers {
  public static void main(String[] args) {
    int[] numbers = ParseInput.asIntegers(args);

    // Create a new list, for only the unique numbers
    ArrayList<Integer> unique = new ArrayList<>();

    // Add each input number to that list only if it appears exactly once in the input array
    for (int number : numbers) {
      if (count(numbers, number) == 1) {
        unique.add(number);
      }
    }

    System.out.println(unique);
  }

  /**
   * Returns the number of times {@code needle} appears in the array {@code haystack}.
   */
  public static int count(int[] haystack, int needle) {
    int total = 0;
    for (int number : haystack) {
      if (number == needle) {
        total++;
      }
    }
    return total;
  }
}
