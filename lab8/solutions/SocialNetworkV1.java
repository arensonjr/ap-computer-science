package lab8.solutions;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Prints out a list of the input person's friends
 */
public class SocialNetworkV1 {
  public static void main(String[] args) {
    // Build the friendship graph
    HashMap<String, ArrayList<String>> friendships = buildFriendshipGraph();

    // Print out the first input's friends
    String name = args[0];
    ArrayList<String> friends = friendships.get(name);
    System.out.println(friends);
  }

  /**
   * Builds the friendship graph, so we can use it to print people's friends.
   */
  private static HashMap<String, ArrayList<String>> buildFriendshipGraph() {
    HashMap<String, ArrayList<String>> friendships = new HashMap<>();

    // Joe is friends with Bob, Tracy, and Sue
    ArrayList<String> joesFriends = new ArrayList<>();
    joesFriends.add("Bob");
    joesFriends.add("Tracy");
    joesFriends.add("Sue");
    friendships.put("Joe", joesFriends);

    // Bob is friends with Joe, Tracy, and Danny
    ArrayList<String> bobsFriends = new ArrayList<>();
    bobsFriends.add("Joe");
    bobsFriends.add("Tracy");
    bobsFriends.add("Danny");
    friendships.put("Bob", bobsFriends);

    // Tracy is friends with Joe, Bob, and Bill
    ArrayList<String> tracysFriends = new ArrayList<>();
    tracysFriends.add("Joe");
    tracysFriends.add("Bob");
    tracysFriends.add("Bill");
    friendships.put("Tracy", tracysFriends);

    // Danny is friends with Bob and Bil
    ArrayList<String> dannysFriends = new ArrayList<>();
    dannysFriends.add("Bob");
    dannysFriends.add("Bill");
    friendships.put("Danny", dannysFriends);

    // Sue is friends with Joe
    ArrayList<String> suesFriends = new ArrayList<>();
    suesFriends.add("Joe");
    friendships.put("Sue", suesFriends);

    // Bill is friends with Tracy and Danny
    ArrayList<String> billsFriends = new ArrayList<>();
    billsFriends.add("Tracy");
    billsFriends.add("Danny");
    friendships.put("Bill", billsFriends);

    // Englebert is friends with nobody
    friendships.put("Englebert", new ArrayList<>());

    return friendships;
  }
}
