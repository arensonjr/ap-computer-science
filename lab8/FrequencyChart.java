package lab8;

import java.util.HashMap;

import lab4.solutions.ParseInput;

/**
 * Prints the frequencies of each input integer.
 */
public class FrequencyChart {
  public static void main(String[] args) {
    // I want this main program to just worry about input and output (commonly referred to as I/O).
    //
    // All of the actual logic, the "smarts" of my program, is going to be inside of the helper method
    // computeFrequencies. I'm doing this so that I can reuse this code later if I want to
    // (hint, hint...).
    int[] numbers = ParseInput.asIntegers(args);
    HashMap<Integer, Integer> frequencies = computeFrequencies(numbers);
    Provided.printAsBarChart(frequencies);
  }

  /**
   * Computes the frequencies of each number in the input array.
   */
  public static HashMap<Integer, Integer> computeFrequencies(int[] numbers) {
    // Replace the following line with your code.
    return new HashMap<>();
  }
}


