package lab3.solutions;

/**
 * Prints out report cards for two classes of students.
 */
class ReportCard {
    // Student names for the 6 students in each class.
    static final String[] CLASS0_NAMES = {
        "Janey", "Bobby", "Jessie", "Tommy", "Joey", "Susie"
    };
    static final String[] CLASS1_NAMES = {
        "Jane", "Bob", "Jess", "Tom", "Joe", "Sue"
    };

    // These are the indices of each score in the scores array; for instance,
    // Janey's math score is at index 0 of the 0th row, and Jessie's history
    // score is at index 3 of the 2nd row (for the class #1 scores).
    static final int MATH_COLUMN = 0;
    static final int READING_COLUMN = 1;
    static final int WRITING_COLUMN = 2;
    static final int HISTORY_COLUMN = 3;

    // Scores for each class. These are arranged such that each row is every
    // score for a single student, and the columns are scores for a single
    // subject (as denoted by the column constants above).
    // 
    // The row indices correspond to the indices in the 'names' arrays above
    // (e.g. index 0 is "Janey", so row 0 is Janey's scores).
    static final int[][] CLASS0_SCORES = {
        {  90 ,  95 ,  88 ,  42 }, // Janey's scores
        {  77 ,  99 ,  77 , 100 }, // Bobby's scores
        { 100 , 100 , 100 , 100 }, // etc.
        {  50 ,  56 ,  57 ,  58 },
        {  84 ,  72 ,  91 ,  88 },
        {  46 ,  89 ,  83 ,  28 }
    };
    static final int[][] CLASS1_SCORES = {
        {  80 ,  85 ,  78 ,  52 }, // Jane's scores
        {  72 ,  69 ,  74 ,  80 }, // Bob's scores
        {  80 ,  66 ,  67 ,  78 }, // etc.
        {  10 ,  12 ,  16 ,  27 },
        { 100 , 100 , 100 ,  99 },
        {  95 ,  94 ,  96 ,  91 }
    };

    // I'll create arrays for all of the classes, so that I can refer to a
    // single class by its index instead of passing around multiple variables.
    static final String[][] ALL_CLASSES_NAMES = { CLASS0_NAMES, CLASS1_NAMES };
    static final int[][][] ALL_CLASSES_SCORES = { CLASS0_SCORES, CLASS1_SCORES };


    /**
     * Prints grades for either class #0 or class #1, depending on the input
     * argument provided by the user.
     */
    public static void main(String[] args) {
        int whichClass = Integer.parseInt(args[0]);
        printClass(whichClass);
    }

    /**
     * Print out all grades and names for an entire class.
     *
     * @param classIndex which class to print (0 or 1)
     */
    public static void printClass(int classIndex) {
        String headerDivider  = "========================================";
        String studentDivider = "----------------------------------------";

        System.out.println(headerDivider);
        System.out.println("Grades for class #" + classIndex + ":");
        System.out.println(headerDivider);

        // Column headers, so we know which scores are which
        System.out.println("Mth | Rdg | Wrt | Hst | Student Name");

        // Print out data for each student (with dividers in between
        System.out.println(studentDivider);
        printStudent(classIndex, 0);
        System.out.println(studentDivider);
        printStudent(classIndex, 1);
        System.out.println(studentDivider);
        printStudent(classIndex, 2);
        System.out.println(studentDivider);
        printStudent(classIndex, 3);
        System.out.println(studentDivider);
        printStudent(classIndex, 4);
        System.out.println(studentDivider);
        printStudent(classIndex, 5);
        System.out.println(studentDivider);
    }

    /**
     * Print out all of the student's scores, and their name, in a single line.
     *
     * @param classIndex which class the student is in
     * @param studentIndex the index of the student within the class
     */
    public static void printStudent(int classIndex, int studentIndex) {
        String scoreDivider = " | ";

        // We want to print all four scores, with dividers between the numbers,
        // then print the student's name.
        //
        // I'm also careful to use print() here instead of print*ln*(), because
        // I want all of these printouts to be on the same line!
        printScore(classIndex, studentIndex, MATH_COLUMN);
        System.out.print(scoreDivider);
        printScore(classIndex, studentIndex, READING_COLUMN);
        System.out.print(scoreDivider);
        printScore(classIndex, studentIndex, WRITING_COLUMN);
        System.out.print(scoreDivider);
        printScore(classIndex, studentIndex, HISTORY_COLUMN);
        System.out.print(scoreDivider);
        System.out.println(ALL_CLASSES_NAMES[classIndex][studentIndex]);
    }

    /**
     * Print out the student's letter grade.
     *
     * @param classIndex which class the student is in
     * @param studentIndex the index of the student within the class
     * @param scoreColumn the column index of this particular score
     */
    public static void printScore(int classIndex, int studentIndex, int scoreColumn) {
        int score = ALL_CLASSES_SCORES[classIndex][studentIndex][scoreColumn];

        // I'm careful to use print() here instead of print*ln*(), because I
        // want all of these printouts to be on the same line!

        char letterGrade;
        if (score > 90) {
            letterGrade = 'A';
        } else if (score > 80) {
            letterGrade = 'B';
        } else if (score > 70) {
            letterGrade = 'C';
        } else if (score > 60) {
            letterGrade = 'D';
        } else {
            letterGrade = 'F';
        }

        // I add two spaces here so that the column stays aligned with the
        // subject headers.
        System.out.print("  " + letterGrade);
    }
}
