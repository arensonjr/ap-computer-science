package lab3.solutions;

/**
 * A collection of arithmetic functions.
 */
class Arithmetic {

    /**
     * We've written the main() function for you. All that you need to do for
     * this assignment is to fill in all of the functions below.
     */
    public static void main(String[] args) {
        int first = Integer.parseInt(args[0]);
        int second = Integer.parseInt(args[1]);
        System.out.println("Initial numbers: " + first + ", " + second);

        System.out.println("Sum:             " + add(first, second));
        System.out.println("Difference:      " + subtract(first, second));
        System.out.println("Product:         " + multiply(first, second));
        System.out.println("Quotient:        " + divide(first, second));
        System.out.println("Remainder:       " + mod(first, second));
        System.out.println("Square (first):  " + square(first));
        System.out.println("Square (second): " + square(second));
        System.out.println("Sum of squares:  " + sumOfSquares(first, second));
    }

    /**
     * Returns the sum of two numbers.
     */
    public static int add(int first, int second) {
        return first + second;
    }

    /**
     * Returns the difference of two numbers.
     */
    public static int subtract(int first, int second) {
        return first - second;
    }

    /**
     * Returns the product of two numbers.
     */
    public static int multiply(int first, int second) {
        return first * second;
    }

    /**
     * Returns the quotient of two numbers.
     */
    public static double divide(int first, int second) {
        return (double) first / (double) second;
    }

    /**
     * Returns the remainder when the first number is divided by the second.
     */
    public static int mod(int first, int second) {
        return first % second;
    }

    /**
     * Returns the square of a number.
     */
    public static int square(int number) {
        return multiply(number, number);
    }

    /**
     * Returns the sum of the squares of two numbers.
     */
    public static int sumOfSquares(int first, int second) {
        return square(first) + square(second);
    }
}
