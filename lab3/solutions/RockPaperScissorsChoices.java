package lab3.solutions;

/**
 * Prints out two Rock-Paper-Scissors choices.
 */
class RockPaperScissorsChoices {
  // Constants, in case you want to use named values instead of literal
  // integers in your code.
  //
  // For more background, see assignment2/RockPaperScissorsLizardSpock.java
  static final int ROCK     = 0;
  static final int PAPER    = 1;
  static final int SCISSORS = 2;
  static final int LIZARD   = 3;
  static final int SPOCK    = 4;

  public static void main(String[] args) {
    // Read what each user chose
    int playerOneChoice = Integer.parseInt(args[0]);
    int playerTwoChoice = Integer.parseInt(args[1]);

    // Print out the english versions
    String playerOneChoiceWord = choiceToString(playerOneChoice);
    String playerTwoChoiceWord = choiceToString(playerTwoChoice);
    System.out.println("Player one chose " + playerOneChoiceWord);
    System.out.println("Player two chose " + playerTwoChoiceWord);
  }

  /**
   * Given a Rock-Paper-Scissors choice, as an integer, returns the word that describes that choice.
   *
   * For example:
   *   choiceToString(1) should return "paper"
   */
  public static String choiceToString(int choice) {
    if (choice == ROCK) {
        return "rock";
    } else if (choice == PAPER) {
        return "paper";
    } else if (choice == SCISSORS) {
        return "scissors";
    } else if (choice == LIZARD) {
        return "lizard";
    } else if (choice == SPOCK) {
        return "spock";
    } else {
      return "(unknown choice)";
    }
  }
}
