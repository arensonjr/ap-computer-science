package lab3;

/**
 * A collection of arithmetic functions.
 */
class Arithmetic {

    /**
     * We've written the main() function for you. All that you need to do for
     * this assignment is to fill in all of the functions below.
     */
    public static void main(String[] args) {
        int first = Integer.parseInt(args[0]);
        int second = Integer.parseInt(args[1]);
        System.out.println("Initial numbers: " + first + ", " + second);

        System.out.println("Sum:             " + add(first, second));
        System.out.println("Difference:      " + subtract(first, second));
        /*
         * Uncomment the following lines when you have implemented those functions:
         */
        //System.out.println("Product:         " + multiply(first, second));
        //System.out.println("Quotient:        " + divide(first, second));
        //System.out.println("Remainder:       " + mod(first, second));
        //System.out.println("Square (first):  " + square(first));
        //System.out.println("Square (second): " + square(second));
        //System.out.println("Sum of squares:  " + sumOfSquares(first, second));
    }

    /**
     * Returns the sum of two numbers.
     */
    public static int add(int first, int second) {
        // Replace the following line with your code.
        return 0;
    }

    /**
     * Returns the difference of two numbers.
     */
    public static int subtract(int first, int second) {
        // Replace the following line with your code.
        return 0;
    }

    // Write code for multiply() here

    // Write code for divide() here

    // Write code for mod() here

    // Write code for square() here

    // Write code for sumOfSquares() here
}
