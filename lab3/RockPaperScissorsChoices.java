package lab3;

/**
 * Prints out two Rock-Paper-Scissors choices.
 */
class RockPaperScissorsChoices {
  // Constants, in case you want to use named values instead of literal
  // integers in your code.
  //
  // For more background, see assignment2/RockPaperScissorsLizardSpock.java
  static final int ROCK     = 0;
  static final int PAPER    = 1;
  static final int SCISSORS = 2;
  static final int LIZARD   = 3;
  static final int SPOCK    = 4;

  public static void main(String[] args) {
    // Read the arguments, then call your function (defined below)
  }

  /**
   * Given a Rock-Paper-Scissors choice, as an integer, returns the word that describes that choice.
   *
   * For example:
   *   choiceToString(1) should return "paper"
   */
  //            Fix the return type so that it returns the right type of
  //            variable (remember, you want to return a word)
  //            |
  //            |
  //            |                    Add the correct type of input here
  //            |                    (remember, you want to examine the
  //            |                    user's choice as an integer)
  //            |                    |
  //            v                    v
  public static void choiceToString(   ) {
    // Write your function here
  }
}
