package lab3;

/**
 * Prints out report cards for two classes of students.
 */
class ReportCard {
    // Student names for the 6 students in each class.
    static final String[] CLASS0_NAMES =
            { "Janey", "Bobby", "Jessie", "Tommy", "Joey", "Susie" };
    static final String[] CLASS1_NAMES =
            { "Jane", "Bob", "Jess", "Tom", "Joe", "Sue" };

    // These are the indices of each score in the scores array; for instance,
    // Janey's math score is at index 0 of the 0th row, and Jessie's history
    // score is at index 3 of the 2nd row (for the class #1 scores).
    static final int MATH_COLUMN = 0;
    static final int READING_COLUMN = 1;
    static final int WRITING_COLUMN = 2;
    static final int HISTORY_COLUMN = 3;

    // Scores for each class. These are arranged such that each row is every
    // score for a single student, and the columns are scores for a single
    // subject (as denoted by the column constants above).
    // 
    // The row indices correspond to the indices in the 'names' arrays above
    // (e.g. index 0 is "Janey", so row 0 is Janey's scores).
    static final int[][] CLASS0_SCORES = {
        {  90 ,  95 ,  88 ,  42 }, // Janey's scores
        {  77 ,  99 ,  77 , 100 }, // Bobby's scores
        { 100 , 100 , 100 , 100 }, // etc.
        {  50 ,  56 ,  57 ,  58 },
        {  84 ,  72 ,  91 ,  88 },
        {  46 ,  89 ,  83 ,  28 }
    };
    static final int[][] CLASS1_SCORES = {
        {  80 ,  85 ,  78 ,  52 }, // Jane's scores
        {  72 ,  69 ,  74 ,  80 }, // Bob's scores
        {  80 ,  66 ,  67 ,  78 }, // etc.
        {  10 ,  12 ,  16 ,  27 },
        { 100 , 100 , 100 ,  99 },
        {  95 ,  94 ,  96 ,  91 }
    };

    public static void main(String[] args) {
        // Write your code here. You may find it useful to write some helper methods, e.g.
        //   - printClass(<information for class to print>)
        //   - printStudent(<information for student / scores to print>)
    }
}
