package classexamples;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Sorts the input args using a comparator.
 */
public class StringSorter {
  public static void main(String[] args) {
    List<String> argsList = new ArrayList<>();
    for (String arg : args) {
      argsList.add(arg);
    }

    // Sort them! Alternatively, instead of StringComparator, try using:
    //   - ReverseStringComparator
    //   - StringLengthComparator
    Comparator<String> ordering = new StringComparator();
    Collections.sort(argsList, ordering);
    System.out.println("Sorted: " + argsList);
  }
}
