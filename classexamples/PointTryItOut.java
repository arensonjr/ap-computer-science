package classexamples;

import java.awt.Point; // 1. Import

public class PointTryItOut {
  public static void main(String[] args) {
    // Try it out: Using the Point object
    //
    // Write code to find the distance between these two sets of points:
    //   * (0, 4) and (11, -6)
    //   * (-5, -1) and (3, 12)
    //
    // Which set of points is closer together?
  }
}
