package classexamples;

/**
 * Represents a race time.
 */
public class RaceResult {
  private int minutes;
  private int seconds;
  private double miles;

  public RaceResult(int min, int s, double mi) {
    minutes = min;
    seconds = s;
    miles = mi;
  }

  /**
   * Returns the race pace in seconds per mile.
   */
  public double getSecondsPerMile() {
    return totalSeconds() / miles;
  }

  /**
   * Returns the total number of minutes this race took, including the seconds
   * as a fraction.
   */
  public double getFractionalMinutes() {
    return totalSeconds() / 60.0;
  }

  // Helper method: Returns the total time of the race in seconds
  private int totalSeconds() {
    return seconds + (minutes * 60);
  }

  public String toString() {
    return miles + " miles in " + minutes + ":" + seconds;
  }
}
