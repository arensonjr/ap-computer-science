package classexamples;

/**
 * A work of literature.
 */
public class Book {
  private String title;

  public Book(String t) {
    title = t;
  }

  public String getTitle() {
    return title;
  }
}
