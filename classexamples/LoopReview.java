package classexamples;

/**
 * Review of loops (week 4).
 */
public class LoopReview {
  public static void main(String[] args) {
    System.out.println("=== Review One: ===");
    reviewOne();

    System.out.println();

    System.out.println("=== Review Two: ===");
    reviewTwo();

    System.out.println();

    System.out.println("=== Review Three: ===");
    reviewThree();
  }

  /**
   * Prints out the following output:
   *
   * 3: last
   * 2: third
   * 1: second
   * 0: first
   */
  public static void reviewOne() {
    String[] words = { "first", "second", "third", "last" };

    // Write your code here
  }

  /**
   * Prints out the following output:
   *
   * 1 2 3 4 5
   * 2 3 4 5
   * 3 4 5
   * 4 5
   * 5
   */
  public static void reviewTwo() {
    // Write your code here
  }

  /**
   * Prints out the following output:
   *
   * Max: 7
   * Index Of Max: 6
   */
  public static void reviewThree() {
    int[] numbers = { 2, 6, -1, 3, 5, 5, 7, 1, 4, 0, 5, 2 };
    // Write your code here
  }
}
