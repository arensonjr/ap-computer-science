package classexamples;

import java.util.Comparator;

/**
 * Compares strings.
 */
public class StringComparator implements Comparator<String> {
  public int compare(String left, String right) {
    return left.compareTo(right);
  }
}
