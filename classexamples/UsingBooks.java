package classexamples;

public class UsingBooks {
  public static void main(String[] args) {
    // Create two copies of the same book
    Book copyOne = new Book("The Lord of the Rings");
    Book copyTwo = new Book("The Lord of the Rings");

    System.out.println("Copy One: " + copyOne);
    System.out.println("Copy Two: " + copyTwo);
    System.out.println("Equal?    " + copyOne.equals(copyTwo));
  }
}
