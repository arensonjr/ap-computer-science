package classexamples;

/**
 * Creates and inspects some Person objects.
 */
public class UsingPerson {
  public static void main(String[] args) {
    Person jeff = new Person(24, "Jeff");

    // Uh-oh! Why can anyone who uses our code make someone's age negative?
    jeff.age = -10;
    System.out.println(jeff);
  }
}
