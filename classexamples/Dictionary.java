package classexamples;

/**
 * Prints a map of the input words keyed by their starting letter.
 */
public class Dictionary {
  public static void main(String[] args) {
    // Write your code here.
    //
    // 0. Imports!
    // 1. Create a HashMap<Character, ArrayList<String>> to list words keyed by their starting
    //    letter
    // 2. Iterate through the args array:
    //     a. Add each word (each arg) to the correct list-value in the map
    // 3. Print out your (partial) dictionary
    //
    // Example input:
    //   one two three four five
    // Example output:
    //   {
    //       f --> [four, five]
    //       o --> [one]
    //       t --> [two, three]
    //   }
  }
}
