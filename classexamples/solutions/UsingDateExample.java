package classexamples.solutions;

import classexamples.solutions.Date;

/**
 * An example using the Date class.
 */
public class UsingDateExample {
  public static void main(String[] args) {
    Date birthday = new Date(1992, 2, 16);
    int year = birthday.getYear();
    System.out.println("I was born in " + year);
  }
}
