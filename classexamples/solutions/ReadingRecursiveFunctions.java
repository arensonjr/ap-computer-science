package classexamples.solutions;

/**
 * Practice reading recursive functions by figuring out what each of these mystery functions does!
 */
public class ReadingRecursiveFunctions {
  public static void main(String[] args) {
    System.out.println("========= INTEGER FUNCTIONS =========");
    try {
      int x = Integer.parseInt(args[0]);
      System.out.println("mystery1(" + x + ") = " + mystery1(x));
      System.out.println("mystery2(" + x + ") = " + mystery2(x));
      System.out.println("mystery3(" + x + ") = " + mystery3(x));
    } catch (NumberFormatException e) {
      System.out.println("(" + args[0] + " is not an integer)");
    }


    String s = args[0];
    System.out.println("\n\n========== STRING FUNCTIONS ==========");
    System.out.println("mystery4(" + s + ") = " + mystery4(s));
    System.out.println("mystery5(" + s + ") = " + mystery5(s));

    if (args.length < 2) {
      System.out.println("(not enough args to run mystery6)");
    } else {
      String s2 = args[1];
      System.out.println("mystery6(" + s + ", " + s2 + ") = " + mystery6(s, s2));
    }
  }

  /**
   * Mystery function #1.
   */
  public static boolean mystery1(int num) {
    if (num == 0) {
      return true;
    }
    if (num == 1) {
      return false;
    }
    return mystery1(num - 2);
  }

  /**
   * Mystery function #2.
   */
  public static int mystery2(int num) {
    if (num == 0) {
      return 0;
    }
    return mystery2(num - 1) + num;
  }

  /**
   * Mystery function #3.
   */
  public static int mystery3(int num) {
    if (num < 10) {
      return num;
    }
    return (num % 10) + mystery3(num / 10);
  }

  /**
   * Mystery function #4.
   */
  public static String mystery4(String str) {
    if (str.isEmpty()) {
      return "";
    }
    return mystery4(str.substring(1)) + str.charAt(0);
  }

  /**
   * Mystery function #5.
   */
  public static int mystery5(String str) {
    if (str.isEmpty()) {
      return 0;
    }
    if (str.charAt(0) == 'x') {
      return 1 + mystery5(str.substring(1));
    }
    return mystery5(str.substring(1));
  }

  /**
   * Mystery function #6.
   */
  public static boolean mystery6(String one, String two) {
    if (one.isEmpty()) {
      return two.isEmpty();
    }
    if (two.isEmpty()) {
      return false;
    }
    if (one.charAt(0) != two.charAt(two.length() - 1)) {
      return false;
    }
    String restOne = one.substring(1);
    String restTwo = two.substring(0, two.length() - 1);
    return mystery6(restOne, restTwo);
  }
}
