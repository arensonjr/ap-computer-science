package classexamples.solutions;

public class StringTryItOut {
  public static void main(String[] args) {
    String input = args[0];
    System.out.println("First character: " + input.charAt(0));
    int lastIndex = input.length() - 1;
    System.out.println("Last character:  " + input.charAt(lastIndex));
  }
}
