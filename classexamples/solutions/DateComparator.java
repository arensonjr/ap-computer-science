package classexamples.solutions;

import java.util.Comparator;

import classexamples.solutions.Date;

/**
 * Compares dates chronologically.
 */
public class DateComparator implements Comparator<Date> {
  public int compare(Date left, Date right) {
    // First, look at the years
    int yearDiff = left.getYear() - right.getYear();
    if (yearDiff != 0) {
      return yearDiff;
    }

    // Same year -- look at months
    int monthDiff = left.getMonth() - right.getMonth();
    if (monthDiff != 0) {
      return monthDiff;
    }

    // Same month -- compare day
    return left.getDay() - right.getDay();
  }
}
