package classexamples.solutions;

import provided.WordPair;

public class WordPairExample {
  public static void main(String[] args) {
    WordPair w = new WordPair("bear", "platypus");

    String first = w.getFirstWord();
    String second = w.getSecondWord();

    System.out.println("A " + first + " is better than a " + second);
  }
}
