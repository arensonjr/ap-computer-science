package classexamples.solutions;

/**
 * Mutates (changes) every element in an array.
 */
public class Loops {
  public static void main(String[] args) {
    int[] myArray = { 9, 8, 7 };

    for (int i = 0; i < myArray.length; i++) {
      // Print out the element at position i before
      System.out.print("Decreasing the number at position " + i + " from " + myArray[i] + " ...");

      myArray[i]--;

      // Print out the element at position i *after*, so we can see how it changed!
      System.out.println(" to " + myArray[i]);
    }
  }
}

