package classexamples.solutions;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Finds the indices of every letter in a sentence.
 */
public class LetterIndices {
  public static void main(String[] args) {
    String sentence = "lots of letters";
    // Instantiate!
    HashMap<Character, ArrayList<Integer>> letterIndices = new HashMap<>();

    // Loop through each position/letter in the sentence:
    for (int i = 0; i < sentence.length(); i++) {
      char current = sentence.charAt(i);

      // Make sure this letter is already in our map. If it's not, then we haven't seen it yet: give
      // it an empty list of indices, which we can fill over time.
      if (!letterIndices.containsKey(current)) {
        letterIndices.put(current, new ArrayList<>());
      }

      // We know what letter (current) is at index i: look up that letter in our map, and add index
      // i to its list of indices.
      ArrayList<Integer> thisLettersIndices = letterIndices.get(current);
      thisLettersIndices.add(i);
    }

    // Print out the result
    System.out.println(letterIndices);
  }
}
