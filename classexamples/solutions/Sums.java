package classexamples.solutions;

/**
 * Prints the sum of numbers from 0 to N, where N is the program arg.
 */
public class Sums {
  public static void main(String[] args) {
    int N = Integer.parseInt(args[0]);

    int sumSoFar = 0;
    for (int num = 0; num <= N; num++) {
      sumSoFar += num;
    }

    System.out.println("The sum of 0 through " + N + " is " + sumSoFar);
  }
}
