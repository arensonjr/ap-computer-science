package classexamples.solutions;

import java.util.ArrayList;

/**
 * Example of using ArrayLists.
 */
public class ArrayListExample {
  public static void main(String[] args) {
    ArrayList<String> names = new ArrayList<String>();
    names.add("Julie");
    names.add("Jeff");

    if (names.contains("Julie")) {
      System.out.println("Julie is in names");
    }
  }
}
