package classexamples.solutions;

import java.util.Random;

/**
 * An example of using the Random class.
 */
public class RandomExample {
  public static void main(String[] args) {
    Random r = new Random();
    int num1 = r.nextInt();
    System.out.println("Random integer #1: " + num1);

    int num2 = r.nextInt();
    System.out.println("Random integer #2: " + num2);

    double d = r.nextDouble();
    System.out.println("Random double: " + d);

    boolean b = r.nextBoolean();
    System.out.println("Random boolean: " + b);
  }
}
