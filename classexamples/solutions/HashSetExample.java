package classexamples.solutions;

import java.util.HashSet;

/**
 * An example of using HashSets.
 */
public class HashSetExample {
  public static void main(String[] args) {
    HashSet<String> marsupials = new HashSet<>();
    marsupials.add("kangaroo");
    marsupials.add("koala");
    marsupials.add("kangaroo");

    System.out.println(marsupials);
  }
}
