package classexamples.solutions;

class FunctionExample {
  public static void main(String[] args) {
    // 1. Call the function with the program's arguments as input
    int num1 = Integer.parseInt(args[0]);
    int num2 = Integer.parseInt(args[1]);
    int sum = add(num1, num2);
    System.out.println("Sum #1 is " + sum);

    // 2. Call the function with variables as input
    //    (note that this one's a different function -- we're
    //    adding doubles, not integers!)
    double num3 = 7.6;
    double num4 = 5.0;
    double sumDouble = addDoubles(num3, num4);
    System.out.println("Sum #2 is " + sumDouble);

    // 3. Call the function with integer literals as input
    //    (notice that we're redefining the sum variable here!)
    sum = add(13, 15);
    System.out.println("Sum #3 is " + sum);

    // 4. Call the function as part of a print statement
    System.out.println("Sum #4 is " + add(-4, 14));
  }

  /**
   * Returns the sum of two numbers.
   */
  public static int add(int first, int second) {
    return first + second;
  }

  /**
   * Returns the sum of two doubles.
   */
  public static double addDoubles(double first, double second) {
    return first + second;
  }
}
