package classexamples.solutions;

import java.util.HashSet;

/**
 * An example of some more of HashSet's methods.
 */
public class HashSetMethods {
  public static void main(String[] args) {
    HashSet<String> marsupials = new HashSet<>();
    marsupials.add("kangaroo");
    marsupials.add("koala");
    marsupials.add("kangaroo");

    if (marsupials.contains("koala")) {
      System.out.println("koala is a marsupial");
    }

    int size = marsupials.size();
    System.out.println("The size is " + size);
  }
}
