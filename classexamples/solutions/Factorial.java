package classexamples.solutions;

/**
 * Prints N!, where N is the command line argument.
 */
public class Factorial {
  public static void main(String[] args) {
    int n = Integer.parseInt(args[0]);
    System.out.println(factorial(n));
  }

  /**
   * Recursively computes the factorial of a number.
   */
  public static int factorial(int num) {
    if (num == 0) {
      return 1;
    } else {
      return num * factorial(num - 1);
    }
  }
}
