package classexamples.solutions;

class Sentences {
  public static void main(String[] args) {
    String sentenceForward = concatenate(args[0], args[1], args[2]);
    System.out.println(sentenceForward);

    String sentenceBackward = concatenate(args[2], args[1], args[0]);
    System.out.println(sentenceBackward);
  }

  /**
   * Combines three words into a sentence.
   */
  public static String concatenate(String firstWord, String secondWord, String thirdWord) {
    return firstWord + " " + secondWord + " " + thirdWord + "!";
  }
}
