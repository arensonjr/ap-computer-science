package classexamples.solutions;

/**
 * Prints out whether the second input string starts with the first input string.
 */
public class StringStartsWithV2 {
  public static void main(String[] args) {
    String part = args[0];
    String whole = args[1];

    for (int i = 0; i < part.length(); i++) {
      if (part.charAt(i) != whole.charAt(i)) {
        System.out.println("no");
        return;
      }
    }
    System.out.println("yes");
  }
}

