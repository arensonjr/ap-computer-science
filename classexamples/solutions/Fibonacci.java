package classexamples.solutions;

/**
 * Prints the Nth fibonacci number, where N is the command line argument.
 */
public class Fibonacci {
  public static void main(String[] args) {
    int n = Integer.parseInt(args[0]);
    System.out.println(fibonacci(n));
  }

  /**
   * Recursively computes the num'th fibonacci number.
   */
  public static int fibonacci(int num) {
    if (num == 0 || num == 1) {
      return 1;
    } else {
      return fibonacci(num - 1) + fibonacci(num - 2);
    }
  }
}
