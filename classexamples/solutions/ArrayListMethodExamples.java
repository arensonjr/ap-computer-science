package classexamples.solutions;

import java.util.ArrayList;

/**
 * Examples of ArrayList methods.
 */
public class ArrayListMethodExamples {
  public static void main(String[] args) {
    ArrayList<Double> nums = new ArrayList<Double>();
    nums.add(1.0);
    nums.add(2.2);
    nums.add(-0.3);

    double first = nums.get(0);
    System.out.println("First element is " + first);

    int size = nums.size();
    System.out.println("Nums contains " + size + " elements");

    nums.set(0, 555.55);
    first = nums.get(0);
    System.out.println("First element is now " + first);
  }
}
