package classexamples.solutions;

import java.util.ArrayList;

/**
 * Computes all primes less than 100.
 */
public class ArrayListPrimes {
  public static void main(String[] args) {
    ArrayList<Integer> primes = new ArrayList<Integer>();
    for (int num = 2; num < 100; num++) {
      if (isPrime(num)) {
        primes.add(num);
      }
    }
    System.out.println(primes);
  }

  /**
   * Returns whether or not num is prime.
   */
  public static boolean isPrime(int num) {
    for (int factor = 2; factor < num; factor++) {
      if (num % factor == 0) {
        return false;
      }
    }
    return true;
  }
}
