package classexamples.solutions;

/**
 * Prints out whether the command line arg is a palindrome or not.
 */
public class Palindrome {
  public static void main(String[] args) {
    System.out.println("Is '" + args[0] + "' a palindrome?  " + palindrome(args[0]));
  }

  /**
   * Recursively checks whether s is a palindrome.
   */
  public static boolean palindrome(String s) {
    // Base case: is 's' just a single character (or empty)? Those are always palindromes.
    if (s.length() <= 1) {
      return true;
    }

    // Recursive case:
    //   - If the two characters on the end of the string are the same, check the rest of the
    //     string.
    //   - If the two characters on the end of the string are different, then it's not a palindrome.
    else {
      char first = s.charAt(0);
      char last = s.charAt(s.length() - 1);
      if (first != last) {
        return false;
      } else {
        return palindrome(s.substring(1, s.length() - 1));
      }
    }
  }
}
