package classexamples.solutions;

/**
 * Prints out whether the second input string starts with the first input string.
 */
public class StringStartsWithV3 {
  public static void main(String[] args) {
    String part = args[0];
    String whole = args[1];

    if (whole.startsWith(part)) {
      System.out.println("yes");
    } else {
      System.out.println("no");
    }
  }
}

