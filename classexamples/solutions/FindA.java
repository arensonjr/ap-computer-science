package classexamples.solutions;

public class FindA {
  public static void main(String[] args) {
    // Get the String from the input
    String word = args[0];

    // Loop over each character in the string, printing the index only
    // if the letter is an 'a'
    for (int index = 0; index < word.length(); index++) {
      if (word.charAt(index) == 'a') {
        System.out.print(index + " ");
      }
    }
  }
}
