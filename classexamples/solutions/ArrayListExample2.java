package classexamples.solutions;

import java.util.ArrayList;

/**
 * Example #2 of using ArrayLists.
 */
public class ArrayListExample2 {
  public static void main(String[] args) {
    ArrayList<String> names = new ArrayList<String>();
    names.add("Julie");
    names.add("Jeff");
    names.add("Matt");
    names.add("Dan");

    System.out.println(names);
  }
}
