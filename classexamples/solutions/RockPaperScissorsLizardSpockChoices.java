package classexamples.solutions;

public class RockPaperScissorsLizardSpockChoices {
  // Constants
  //
  // For more background, see assignment2/RockPaperScissorsLizardSpock.java
  static final int ROCK = 0;
  static final int PAPER = 1;
  static final int SCISSORS = 2;
  static final int LIZARD = 3;
  static final int SPOCK = 4;

  public static void main(String[] args) {
    int firstChoice = Integer.parseInt(args[0]);
    int secondChoice = Integer.parseInt(args[1]);

    System.out.println("First player's choice is " + choiceToString(firstChoice));
    System.out.println("Second player's choice is " + choiceToString(secondChoice));
  }

  /**
   * Given a Rock-Paper-Scissors choice, as an integer, returns the word that describes that choice.
   *
   * For example:
   *   choiceToString(1) should return "paper"
   */
  public static String choiceToString(int inputChoice) {
    String[] choices = {"Rock", "Paper", "Scissors", "Lizard", "Spock"};
    return choices[inputChoice];
  }
}
