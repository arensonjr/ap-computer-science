package classexamples.solutions;

import java.util.ArrayList;

/**
 * Examples of loops over ArrayLists.
 */
public class ArrayListLoops {
  public static void main(String[] args) {
    ArrayList<Integer> nums = new ArrayList<Integer>();
    nums.add(5);
    nums.add(1);
    nums.add(12);
    nums.add(-5);
    nums.add(9);

    System.out.println(maxValue(nums));
    System.out.println(maxIndex(nums));
  }

  /**
   * Returns the maximum number in a list.
   */
  public static int maxValue(ArrayList<Integer> nums) {
    int max = nums.get(0);
    for (int num : nums) {
      if (num > max) {
        max = num;
      }
    }
    return max;
  }

  /**
   * Returns the index of the maximum number in the list.
   */
  public static int maxIndex(ArrayList<Integer> nums) {
    int max = nums.get(0);
    int maxIndex = 0;
    for (int i = 0; i < nums.size(); i++) {
      int num = nums.get(i);
      if (num > max) {
        max = num;
        maxIndex = i;
      }
    }
    return maxIndex;
  }
}
