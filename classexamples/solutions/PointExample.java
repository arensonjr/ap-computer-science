package classexamples.solutions;

import java.awt.Point;

public class PointExample {
  public static void main(String[] args) {

    Point origin = new Point(0, 0);

    double x = origin.getX();
    double y = origin.getY();

    System.out.println("The origina has X value: " + x);
    System.out.println("The origina has Y value: " + y);

  }
}
