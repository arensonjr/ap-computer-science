package classexamples.solutions;

import java.awt.Point; // 1. Import

public class PointExample2 {
  public static void main(String[] args) {

    Point q1 = new Point(1, 9); // 2. Instantiate
    double x1 = q1.getX(); // 3. Call method
    System.out.println(x1 + " = 1");
    System.out.println(q1.getY() + " = 9"); // 3. Call method

    Point q2 = new Point(-2, 5); // 2. Instantiate
    double dist = q1.distance(q2); // 3. Call method
    System.out.println(dist + " = 5");

  }
}

