package classexamples.solutions;

import classexamples.solutions.HasYear;
import classexamples.solutions.Date;
import classexamples.solutions.Movie;

public class BehindTheTimes {

  public static void main(String[] args) {
    // Date: year, month, day
    HasYear watched = new Date(2009, 05, 24);

    // Movie: title, length, year, rating
    HasYear donnieDarko = new Movie("Donnie Darko", 113, 2001, 8.1);

    int difference = yearDifference(donnieDarko, watched);
    System.out.println(
        "I saw Donnie Darko " + difference + " years after it came out.");
  }

  public static int yearDifference(HasYear thing1, HasYear thing2) {
    return thing2.getYear() - thing1.getYear();
  }
}
