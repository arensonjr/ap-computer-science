package classexamples.solutions;

public interface HasYear {
  /**
   * Returns this object's year, whatever that may be.
   */
  int getYear();
}
