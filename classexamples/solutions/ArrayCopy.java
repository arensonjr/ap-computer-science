package classexamples.solutions;

public class ArrayCopy {
  public static void main(String[] args) {
    String[] a = { "hello", "AP", "class" };
    String[] b = copyStringArray(a);

    System.out.println("a is:");
    for (String word : a) {
      System.out.println("    " + word);
    }

    System.out.println();

    System.out.println("b is:");
    for (String word : b) {
      System.out.println("    " + word);
    }
  }

  /**
   * Creates a copy of original with the same input.
   */
  public static String[] copyStringArray(String[] original) {
    // Create a whole new array with the same size (length) as the original.
    String[] copied = new String[original.length];

    // For each element of original, put it into the copy.
    //
    // We need to loop over the indices here, because the only way we can add stuff to an array is
    // by using indices. That means we can't use a foreach loop here.
    int index = 0;
    while (index < original.length) {
      copied[index] = original[index];
      index++;
    }

    // Now that we have a completely separate copy of the array, we can return it.
    return copied;
  }
}
