package classexamples.solutions;

/**
 * Extracts the first word from a sentence.
 */
public class ExtractFirstWordV1 {
  public static void main(String[] args) {
    String sentence = "This is a pretty interesting sentence, if I do say so myself.";

    int firstSpaceIndex = sentence.indexOf(' ');
    String firstWord = sentence.substring(0, firstSpaceIndex);
    System.out.println(firstWord);
  }
}

