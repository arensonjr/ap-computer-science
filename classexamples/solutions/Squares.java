package classexamples.solutions;

public class Squares {
  public static void main(String[] args) {
    int userInput = Integer.parseInt(args[0]);

    // Version 1:
    /*
    double[] squares = { 0, 1, 4, 9, 16, 25 };
    System.out.println("The square of " + userInput + " is " + squares[userInput]);
    */

    // Version 2:
    System.out.println("The square of " + userInput + " is " + square(userInput));
  }

  public static double square(int input) {
    return input * input;
  }
}
