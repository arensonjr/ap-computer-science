package classexamples.solutions;

public class Movie implements HasYear {
  private String title;
  private int minutes;
  private int year;
  private double rating;

  public Movie(String t, int m, int y, double r) {
    title = t;
    minutes = m;
    year = y;
    rating = r;
  }

  public int getYear() {
    return year;
  }
}
