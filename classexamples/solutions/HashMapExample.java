package classexamples.solutions;

import provided.Maps;

import java.util.HashMap;

/**
 * A HashMap example, using presidents.
 */
public class HashMapExample {
  public static void main(String[] args) {
    HashMap<Integer, String> presidents = new HashMap<>();
    presidents.put(1, "Washington");
    presidents.put(3, "Jefferson");

    System.out.println(presidents);

    Maps.prettyPrint(presidents);
  }
}
