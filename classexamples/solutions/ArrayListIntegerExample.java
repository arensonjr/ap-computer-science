package classexamples.solutions;

import java.util.ArrayList;

/**
 * Prints out Fibonacci numbers.
 */
public class ArrayListIntegerExample {
  public static void main(String[] args) {
    ArrayList<Integer> fib = new ArrayList<Integer>();
    fib.add(1);
    fib.add(1);
    fib.add(2);
    fib.add(3);
    fib.add(5);

    System.out.println(fib);
  }
}
