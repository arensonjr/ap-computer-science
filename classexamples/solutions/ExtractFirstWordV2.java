package classexamples.solutions;

/**
 * Extracts the first word from a sentence.
 */
public class ExtractFirstWordV2 {
  public static void main(String[] args) {
    String sentence = "This is a pretty interesting sentence, if I do say so myself.";

    String firstWord = "";
    for (int index = 0; index < sentence.length(); index++) {
      char nextChar = sentence.charAt(index);
      if (nextChar == ' ') {
        // We found a space -- everything we've already seen is the first word
        System.out.println(firstWord);
        return;
      } else {
        // Not a space -- it's part of the first word
        firstWord += nextChar;
      }
    }
  }
}
