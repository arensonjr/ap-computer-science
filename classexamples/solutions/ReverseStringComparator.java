package classexamples.solutions;

import java.util.Comparator;

/**
 * Compares strings in reverse (closer to A = greater-than).
 */
public class ReverseStringComparator implements Comparator<String> {
  public int compare(String left, String right) {
    return right.compareTo(left);
  }
}
