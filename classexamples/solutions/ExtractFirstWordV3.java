package classexamples.solutions;

/**
 * Extracts the first word from a sentence.
 */
public class ExtractFirstWordV3 {
  public static void main(String[] args) {
    String sentence = "This is a pretty interesting sentence, if I do say so myself.";

    String[] words = sentence.split(" ");
    String firstWord = words[0];
    System.out.println(firstWord);
  }
}

