package classexamples.solutions;

/**
 * Calculates the greatest common denominator of two numbers.
 */
public class Euclid {

  // Algorithm (written on the whiteboard during class):
  //
  // * Loop until remainder == 0:
  //   - Set 'remainder' equal to the remainder of dividing largest by smallest
  //     ("largest mod smallest")
  //   - Set smallest to be the new largest
  //   - Set remainder to be the new smallest
  // * Return largest


  public static void main(String[] args) {
    // Input: Two integers
    int numOne = Integer.parseInt(args[0]);
    int numTwo = Integer.parseInt(args[1]);
    int gcd = greatestCommonDenominator(numOne, numTwo);

    System.out.println(
        "The greatest common denominator of "
        + numOne + " and " + numTwo + " is " + gcd);
  }

  /**
   * Calculates the greatest common denominator of two numbers.
   */
  public static int greatestCommonDenominator(int numOne, int numTwo) {
    // Figure out which input is largest vs. smallest
    int largest;
    int smallest;
    if (numOne > numTwo) {
      largest = numOne;
      smallest = numTwo;
    } else {
      largest = numTwo;
      smallest = numOne;
    }

    // Copied directly from our algorithm
    int remainder;
    while (smallest > 0) {
      remainder = largest % smallest;

      // Comment out these print statements to inspect what's happening on each
      // iteration of the loop:
      /*
      System.out.println("Iterating:");
      System.out.println("  largest = " + largest);
      System.out.println("  smallest = " + smallest);
      System.out.println("  remainder = " + remainder);
      */

      largest = smallest;
      smallest = remainder;
    }
    return largest;
  }
}
