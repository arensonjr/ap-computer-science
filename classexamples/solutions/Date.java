package classexamples.solutions;

/**
 * Represents a calendar date.
 */
public class Date implements HasYear {

  int year;
  int month;
  int day;

  public Date(int y, int m, int d) {
    year = y;
    month = m;
    day = d;
  }

  public int getYear() {
    return year;
  }

  public int getMonth() {
    return month;
  }

  public int getDay() {
    return day;
  }
}
