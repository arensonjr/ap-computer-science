package classexamples.solutions;

public class StringExample {
  public static void main(String[] args) {
    String greeting = "hello";

    char firstLetter = greeting.charAt(0);
    int numChars = greeting.length();

    System.out.println(greeting + " has " + numChars
        + " letters and starts with " + firstLetter + ".");
  }
}

