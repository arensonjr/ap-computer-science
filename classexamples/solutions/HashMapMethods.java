package classexamples.solutions;

import java.util.HashMap;

/**
 * Examples of HashMap's methods.
 */
public class HashMapMethods {
  public static void main(String[] args) {
    HashMap<String, Double> times = new HashMap<>();
    times.put("Usain", 9.58);
    times.put("Tyson", 9.69);
    times.put("Asafa", 9.72);

    System.out.println("Asafa Powell's 100m time is " + times.get("Asafa"));

    boolean hasATimeForBob = times.containsKey("Bob");
    if (hasATimeForBob) {
      System.out.println("Bob's 100m time is " + times.get("Bob"));
    }

    int numRunners = times.size();
    System.out.println("We have times for " + numRunners + " runners");
  }
}
