package classexamples.solutions;

/**
 * Prints out whether the second input string starts with the first input string.
 */
public class StringStartsWithV1 {
  public static void main(String[] args) {
    String part = args[0];
    String whole = args[1];

    String startOfWhole = whole.substring(0, part.length());
    if (startOfWhole.equals(part)) {
      System.out.println("yes");
    } else {
      System.out.println("no");
    }
  }
}
