package classexamples.solutions;

import java.util.Comparator;

/**
 * Compares strings by length.
 */
public class StringLengthComparator implements Comparator<String> {
  public int compare(String left, String right) {
    return left.length() - right.length();
  }
}
