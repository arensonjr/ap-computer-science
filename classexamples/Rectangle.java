package classexamples;

import java.awt.Point;

/**
 * Represents a rectangle on a coordinate plane.
 */
public class Rectangle {
  // Bottom-left corner of the rectangle
  Point bottomLeft;

  // Top-right corner of the rectangle
  Point topRight;

  /**
   * Creates a new rectangle based on its bottom-left corner and its size.
   */
  public Rectangle(Point bl, int width, int height) {
    bottomLeft = bl;
    topRight = new Point((int) bl.getX() + width, (int) bl.getY() + height);
  }

  /**
   * Returns the width of the rectangle.
   */
  public int getWidth() {
    return (int)(topRight.getX() - bottomLeft.getX());
  }

  /**
   * Returns the height of the rectangle.
   */
  public int getHeight() {
    return (int)(topRight.getY() - bottomLeft.getY());
  }

  /**
   * Returns the area of the rectangle.
   */
  public int getArea() {
    return getWidth() * getHeight();
  }
}
