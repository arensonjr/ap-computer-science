package classexamples;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Practice using lists as an interface, not a concrete class.
 */
public class UseLists {
  public static void main(String[] args) {
    // Convert input to integers
    List<Integer> nums = new ArrayList<>();
    for (String arg : args) {
      nums.add(Integer.parseInt(arg));
    }

    // Count the number of zeros in the list
    int numZeros = countZeros(nums);
    System.out.println(numZeros);
  }

  public static int countZeros(List<Integer> numbers) {
    // Write your code here
  }
}
