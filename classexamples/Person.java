package classexamples;

/**
 * Represents a person.
 */
public class Person {
  // The person's age, in years.
  public int age;

  // The person's first name.
  public String name;

  public Person(int a, String n) {
    age = a;
    name = n;
  }

  /**
   * Changes this person's age.
   *
   * Their age must be nonnegative (nobody can be negative years old).
   */
  public void setAge(int newAge) {
    // A person can't have a negative age -- reset it to the closest
    // legitimate age (0 years old / newborn).
    if (newAge < 0) {
      newAge = 0;
    }
    age = newAge;
  }

  /**
   * Returns the person's age, in years.
   */
  public int getAge() {
    return age;
  }

  /**
   * Returns the person's first name.
   */
  public String getName() {
    return name;
  }

  /**
   * Pretty-prints the person.
   */
  public String toString() {
    return name + " (" + age + " years old)";
  }
}
