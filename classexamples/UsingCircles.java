package classexamples;

import java.awt.Point;

/**
 * Creates and prints the diameter of some circles.
 */
public class UsingCircles {
  public static void main(String[] args) {
    int radius;
    Point center;

    center = new Point(0, 0);
    radius = 5;
    Circle c1 = new Circle(center, radius);
    System.out.println("c1.getCircumference() = " + c1.getCircumference());

    center = new Point(50, 2);
    radius = 100;
    Circle c2 = new Circle(center, radius);
    System.out.println("c2.getCircumference() = " + c2.getCircumference());

    center = new Point(-5, 10);
    radius = 16;
    Cicle c3 = new Circle(center, radius);
    System.out.println("c3.getCircumference() = " + c3.getCircumference());
  }
}
