package classexamples;

// 1. Import
import java.util.HashSet;


/**
 * Prints out whether you've been to a particular state or not.
 */
public class HaveIBeenThere {
  public static void main(String[] args) {
    // Write your code here.
    //
    // 1. Create a HashSet of Strings that contains all the states you've been to.
    //    Pretty print it.
    //    Print out "I've been to X states", where X is the number you've been to.
    //
    // 2. Take the name of a state as input. Print whether you've been to that state.
  }
}
