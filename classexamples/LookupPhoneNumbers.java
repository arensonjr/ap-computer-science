package classexamples;

/**
 * Acts as a basic phonebook.
 */
public class LookupPhoneNumbers {
  public static void main(String[] args) {
    // Write your code here.
    //
    // 0. Import!
    // 1. Instantiate a HashMap from String to Integer
    // 2. Use put to fill the map with at least 5 (name -> phone number) pairs
    // 3. Use Maps.prettyPrint to print your phonebook
    // 4. Take one person's name as input
    // 5. Use containsKey to check if you have the person's phone number
    // 6. If your map contains their number, use get to print it
  }
}
