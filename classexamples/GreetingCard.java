package classexamples;

/**
 * Prints greetings.
 */
public class GreetingCard {
  public static void main(String[] args) {
    // Call printGreetingCard() with multiple different greeters!
    // (Write your code here)
  }

  public static void printGreetingCard(Greeter card) {
    // Write your code here
  }
}
