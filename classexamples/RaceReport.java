package classexamples;

/**
 * Reports some race results.
 */
public class RaceReport {
  public static void main(String[] args) {
    RaceResult result = new RaceResult(6, 30, 1.0);
    System.out.println("Total seconds in a 6:30 race: " + result.totalSeconds());
  }
}
