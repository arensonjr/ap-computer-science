package lab15;

import java.util.ArrayList;

/**
 * Sorts the integers provided in the program arguments... somehow!
 */
public class MysterySort {
  public static void main(String[] args) {
    ArrayList<Integer> numbers = new ArrayList<Integer>();
    for (String arg : args) {
      numbers.add(Integer.parseInt(arg));
    }

    System.out.println("Unsorted: " + numbers);
    mysterySort(numbers);
    System.out.println("Sorted:   " + numbers);
  }

  /**
   * Sorts the input numbers... somehow?
   */
  public static void mysterySort(ArrayList<Integer> numbers) {
    for (int i = 0; i < numbers.size(); i++) {
      int minIndex = i;
      for (int j = i; j < numbers.size(); j++) {
        if (numbers.get(j) < numbers.get(minIndex)) {
          minIndex = j;
        }
      }

      int smaller = numbers.get(minIndex);
      int larger = numbers.get(i);
      numbers.set(i, smaller);
      numbers.set(minIndex, larger);
    }
  }
}
