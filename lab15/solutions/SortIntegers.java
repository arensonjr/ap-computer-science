package lab15.solutions;

import java.util.ArrayList;

/**
 * Sorts the integers provided in the program arguments.
 */
public class SortIntegers {
  public static void main(String[] args) {
    ArrayList<Integer> numbers = new ArrayList<Integer>();
    for (String arg : args) {
      numbers.add(Integer.parseInt(arg));
    }

    System.out.println("Unsorted: " + numbers);
    ArrayList<Integer> sorted = sortIntegers(numbers);
    System.out.println("Sorted:   " + sorted);
  }

  /**
   * Returns a new ArrayList containing the same integers as in the input array list,
   * except sorted from smallest to largest.
   */
  public static ArrayList<Integer> sortIntegers(ArrayList<Integer> unsorted) {
    ArrayList<Integer> sorted = new ArrayList<Integer>();

    // I'm going to use selection sort for this example.

    // Keep taking things from the input list until there are none left
    while (unsorted.isEmpty() == false) {
      // Find the smallest one, and move it from the
      // unsorted list to the sorted list
      int indexOfSmallest = minIndex(unsorted);
      Integer smallest = unsorted.remove(indexOfSmallest);
      sorted.add(smallest);
    }

    return sorted;
  }

  /**
   * Returns the index of the smallest element in the list.
   */
  public static int minIndex(ArrayList<Integer> nums) {
    // We solved this problem in Lecture 4B with arrays, except for max instead of min.
    // We also saw this solved in Lecture 8A (again, for max instead of min, but same idea).
    int min = nums.get(0);
    int minIndex = 0;
    for (int i = 0; i < nums.size(); i++) {
      int num = nums.get(i);
      if (num < min) {
        min = num;
        minIndex = i;
      }
    }
    return minIndex;
  }
}
