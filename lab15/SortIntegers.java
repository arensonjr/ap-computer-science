package lab15;

import java.util.ArrayList;

/**
 * Sorts the integers provided in the program arguments.
 */
public class SortIntegers {
  public static void main(String[] args) {
    ArrayList<Integer> numbers = new ArrayList<Integer>();
    for (String arg : args) {
      numbers.add(Integer.parseInt(arg));
    }

    System.out.println("Unsorted: " + numbers);
    ArrayList<Integer> sorted = sortIntegers(numbers);
    System.out.println("Sorted:   " + sorted);
  }

  /**
   * Returns a new ArrayList containing the same integers as in the input array list,
   * except sorted from smallest to largest.
   */
  public static ArrayList<Integer> sortIntegers(ArrayList<Integer> unsorted) {
    // Write your code here.
    return null;
  }
}
