package assignment2.solutions;

class RockPaperScissorsLizardSpock {
    // These are constants.
    //
    // DON'T WORRY TOO MUCH ABOUT THIS FOR NOW! We're just providing these
    // constants for you so that you don't have to worry about remembering
    // which integer is which in your code! You may read on if you're curious,
    // but you DO NOT need to know the remainder of this information for your
    // assignment.
    //
    //     The "final" means they can't be modified after your program starts
    //     running (if you try to write "ROCK = 5;" in your code, it won't
    //     compile!).
    //
    //     The "static" means that they can be used within your main() method.
    //     We'll explain why farther into the class; for now, just know that
    //     the "static" keyword is required here.
    //     
    //     The typical way of naming constant variables is to give them names
    //     in ALL CAPS, to tell other programmers that these variables are
    //     special and can't be modified.
    //
    //     These variables are outside your main() method because they don't
    //     change from program to program -- they can be defined up front, at
    //     the class-level.
    static final int ROCK     = 0;
    static final int PAPER    = 1;
    static final int SCISSORS = 2;
    static final int LIZARD   = 3;
    static final int SPOCK    = 4;

    public static void main(String[] args) {
        int humanChoice = Provided.humanChoice();
        int computerChoice = Provided.computerChoice();

        String humanChoiceStr;
        if (humanChoice == ROCK) {
            humanChoiceStr = "rock";
        } else if (humanChoice == PAPER) {
            humanChoiceStr = "paper";
        } else if (humanChoice == SCISSORS) {
            humanChoiceStr = "scissors";
        } else if (humanChoice == LIZARD) {
            humanChoiceStr = "lizard";
        } else { // humanChoice == SPOCK
            humanChoiceStr = "spock";
        }

        String computerChoiceStr;
        if (computerChoice == ROCK) {
            computerChoiceStr = "rock";
        } else if (computerChoice == PAPER) {
            computerChoiceStr = "paper";
        } else if (computerChoice == SCISSORS) {
            computerChoiceStr = "scissors";
        } else if (computerChoice == LIZARD) {
            computerChoiceStr = "lizard";
        } else { // computerChoice == SPOCK 
            computerChoiceStr = "spock";
        }

        System.out.println(
                "The human picked " + humanChoiceStr
                + ", and the computer picked " + computerChoiceStr);

        // Did they tie?
        if (humanChoice == computerChoice) {
            System.out.println("Players tie!");
        }

        // Rock beats Lizard and Scissors
        else if (humanChoice == ROCK && (computerChoice == LIZARD || computerChoice == SCISSORS)) {
            System.out.println("Human wins!");
        }

        // Paper beats Rock and Spock
        else if (humanChoice == PAPER && (computerChoice == ROCK || computerChoice == SPOCK)) {
            System.out.println("Human wins!");
        }

        // Scissors beats Paper and Lizard
        else if (humanChoice == SCISSORS && (computerChoice == PAPER || computerChoice == LIZARD)) {
            System.out.println("Human wins!");
        }

        // Lizard beats Paper and Spock
        else if (humanChoice == LIZARD && (computerChoice == PAPER || computerChoice == SPOCK)) {
            System.out.println("Human wins!");
        }

        // Spock beats Scissors and Rock
        else if (humanChoice == SPOCK && (computerChoice == SCISSORS || computerChoice == ROCK)) {
            System.out.println("Human wins!");
        }

        // At this point, we know they didn't tie, and we've exhausted the ways that the human can win,
        // so the only remaining possibility is that the computer wins.
        else {
            System.out.println("Computer wins!");
        }
    }
}
