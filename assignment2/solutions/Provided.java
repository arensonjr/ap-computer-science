package assignment2.solutions;

import java.util.Random;
import java.util.Scanner;
import java.util.regex.Pattern;

/**
 * Provided code for AP Computer Science Assignment #2.
 */
public class Provided {
    /**
     * A regular expression which recognizes valid RPSLS choices.
     *
     * (note to students: You don't need to know what a regular expression is,
     * nor what this variable means. If you're really interested, ask after
     * class one day.)
     */
    private static final Pattern RPSLS_CHOICE_REGEX = Pattern.compile("[01234]");

    /**
     * Asks the human to choose one of ROCK, PAPER, SCISSORS, LIZARD, or SPOCK,
     * and returns their choice (as an integer).
     *
     * The integers returned by this function are as follows:
     *
     *   0 <--> ROCK
     *   1 <--> PAPER
     *   2 <--> SCISSORS
     *   3 <--> LIZARD
     *   4 <--> SPOCK
     *
     * The astute student will notice that these are the same constants defined
     * in the RockPaperScissorsLizardSpock class.
     */
    public static int humanChoice() {
        System.out.println(
                "Choose one of the following by typing the corresponding number\n"
                + "and pressing <Enter>:\n"
                + "\n"
                + "    0: rock\n"
                + "    1: paper\n"
                + "    2: scissors\n"
                + "    3: lizard\n"
                + "    4: spock\n");
        Scanner inputReader = new Scanner(System.in);

        System.out.print("Your choice: ");
        while (!inputReader.hasNext(RPSLS_CHOICE_REGEX)) {
            String nonIntegerToken = inputReader.next();
            System.out.println("Try again -- '" + nonIntegerToken + "' is not a valid choice.");
            System.out.print("Your choice: ");
        }

        // We only exit the loop if the next input token is a valid choice.
        int humanChoice = Integer.parseInt(inputReader.next(RPSLS_CHOICE_REGEX));
        return humanChoice;
    }

    /**
     * Randomly chooses one of ROCK, PAPER, SCISSORS, LIZARD, or SPOCK for the
     * computer, and returns its choice (as an integer).
     *
     * The integers returned by this function are as follows:
     *
     *   0 <--> ROCK
     *   1 <--> PAPER
     *   2 <--> SCISSORS
     *   3 <--> LIZARD
     *   4 <--> SPOCK
     *
     * The astute student will notice that these are the same constants defined
     * in the RockPaperScissorsLizardSpock class.
     */
    public static int computerChoice() {
        // This returns a number between 0 (inclusive) and 5 (exclusive). That
        // is, it returns a random element from the set {0, 1, 2, 3, 4}.
        return new Random().nextInt(5);
    }
}
