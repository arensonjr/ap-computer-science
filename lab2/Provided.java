package lab2;

import java.util.Random;

/**
 * Provided code for AP Computer Science Lab #2.
 */
public class Provided {
    /**
     * Returns a random number between 1 and 10.
     *
     * It is left as an exercise for the students whether these bounds are
     * inclusive, exclusive, or one of each.
     * 
     * An inclusive bound means that the boundary (i.e. 1 or 10) can be
     * returned from this function; an exclusive bound means that the number
     * will never be returned (i.e. only as low as 2, or only as high as 9).
     */
    public static int randomBetween1and10() {
        // Our random number generator generates numbers starting at zero, so we'll obtain one
        // of those and then massage in to be between one and ten.
        Random randomNumberGenerator = new Random();
        int randomBetween0and9 = randomNumberGenerator.nextInt(10);

        return randomBetween0and9 + 1;
    }
}
