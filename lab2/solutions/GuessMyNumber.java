package lab2.solutions;

/**
 * A program which takes a single integer as input, and prints out whether or not
 * that integer is equal to a random integer (between 1 and 10) chosen randomly
 * by the computer.
 */
class GuessMyNumber {
    public static void main(String[] args) {
        int humanChoice = Integer.parseInt(args[0]);
        int computerChoice = Provided.randomBetween1and10();

        if (humanChoice > computerChoice) {
            // This print statement would be really, really long if I didn't
            // split it up into more than one line. Java allows you to break a
            // single statement up into multiple lines in order to make it
            // easier to read -- as long as you break the line where there
            // would normally be a space, this will work!
            //
            // This is one of the reasons we have a semicolon at the end of
            // each statement -- that way, Java can tell the difference between
            // a line of code and a full statement (which may consist of
            // multiple lines).
            System.out.println(
                    "Too high! You chose " + humanChoice
                    + ", and the computer chose " + computerChoice);
        } else if (humanChoice < computerChoice) {
            System.out.println(
                    "Too low! You chose " + humanChoice
                    + ", and the computer chose " + computerChoice);
        } else {
            System.out.println(
                    "Correct! You and the computer both chose " + humanChoice);
        }
    }
}
