package lab2.solutions;

/**
 * A program which takes two integers as input, and prints out the larger one.
 */
class Max {
    public static void main(String[] args) {
        int firstInt = Integer.parseInt(args[0]);
        int secondInt = Integer.parseInt(args[1]);

        // Print only the larger one
        if (firstInt > secondInt) {
            System.out.println(firstInt);
        } else { // (secondInt > firstInt) || (secondInt == firstInt)
            System.out.println(secondInt);
        }
    }
}
