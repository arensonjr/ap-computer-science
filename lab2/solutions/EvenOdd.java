package lab2.solutions;

/**
 * A program which takes two integer arguments, and prints out how many of them
 * are even (evenly divisible by two) or odd (not evenly divisible by two).
 */
class EvenOdd {
    public static void main(String[] args) {
        int firstInt = Integer.parseInt(args[0]);
        int secondInt = Integer.parseInt(args[1]);

        if ((firstInt % 2 == 0) && (secondInt % 2 == 0)) {
            System.out.println("Both numbers are even.");
        } else if ((firstInt % 2 == 1) && (secondInt % 2 == 1)) {
            System.out.println("Both numbers are odd.");
        } else {
            // They're not both even, and they're not both odd -- we must have
            // one of each, otherwise we would have already printed something!
            System.out.println("One number is even, and one is odd.");
        }
    }
}
