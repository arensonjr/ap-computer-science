package lab4;

/**
 * Common functions to parse program input.
 *
 * These functions can be called from any other class, by writing:
 *
 *   ParseInput.asIntegers(args)
 *
 * or
 *
 *   ParseInput.asDoubles(args)
 *
 * in your main() function.
 */
public class ParseInput {
  /**
   * Converts an array of program arguments into an array of doubles.
   */
  public static double[] asDoubles(String[] programArgs) {
    double[] doubles = new double[programArgs.length];
    // Write your code here!
    return doubles;
  }

  /**
   * Converts an array of program arguments into an array of integers.
   */
  public static int[] asIntegers(String[] programArgs) {
    int[] ints = new int[programArgs.length];
    // Write your code here!
    return ints;
  }

  /**
   * This main function will just test that your functions work.
   *
   * Feel free to mess around with the code in this function; your other classes will
   * call either ParseInput.asIntegers() or ParseInput.asDoubles() directly.
   */
  public static void main(String[] args) {
    // First, try printing all input as doubles
    System.out.println("===== Interpreting input as doubles =====");
    double[] doubles = asDoubles(args);
    int i = 0;
    while (i < doubles.length) {
      System.out.println("Input #" + i + ": " + doubles[i]);
      i++;
    }

    // Then, try printing all input as integers
    System.out.println("===== Interpreting input as integers =====");
    int[] integers = asIntegers(args);
    i = 0;
    while (i < integers.length) {
      System.out.println("Input #" + i + ": " + integers[i]);
      i++;
    }
  }
}
