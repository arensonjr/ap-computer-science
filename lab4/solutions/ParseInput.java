package lab4.solutions;

/**
 * Common functions to parse program input.
 *
 * These functions can be called from any other class, by writing:
 *
 *   ParseInput.asIntegers(args)
 *
 * or
 *
 *   ParseInput.asDoubles(args)
 *
 * in your main() function.
 */
public class ParseInput {
  /**
   * Converts an array of program arguments into an array of doubles.
   */
  public static double[] asDoubles(String[] programArgs) {
    double[] doubles = new double[programArgs.length];
    int i = 0;
    while (i < programArgs.length) {
      doubles[i] = Double.parseDouble(programArgs[i]);
      i++;
    }
    return doubles;
  }

  /**
   * Converts an array of program arguments into an array of integers.
   */
  public static int[] asIntegers(String[] programArgs) {
    int[] ints = new int[programArgs.length];
    int i = 0;
    while (i < programArgs.length) {
      ints[i] = Integer.parseInt(programArgs[i]);
      i++;
    }
    return ints;
  }
}
