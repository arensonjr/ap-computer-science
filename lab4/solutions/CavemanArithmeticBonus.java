package lab4.solutions;

class CavemanArithmeticBonus {
    public static void main(String[] args) {
        int firstInput = Integer.parseInt(args[0]);
        int secondInput = Integer.parseInt(args[1]);

        System.out.println("Product:  " + multiply(firstInput, secondInput));
        System.out.println("Quotient: " + divide(firstInput, secondInput));
        System.out.println("Exponent: " + exponent(firstInput, secondInput));
    }

    /**
     * Multiplies two integers, without using the '*' operator.
     */
    public static int multiply(int first, int second) {
        int i = 0;
        int product = 0;

        // first * second = (first + first + ... + first)
        //                = first, added to itself 'second' times
        while (i < second) {
            product += first;
            i++;
        }

        return product;
    }

    /**
     * Divides two integers, without using the '/' operator.
     */
    public static int divide(int first, int second) {
        // first/second is the number of times that second goes into first. So,
        // we'll count the number of times we can subtract second from first.
        int numIterations = 0;
        int remaining = first;
        while (remaining >= 0) {
            remaining -= second;
            numIterations++;
        }

        // Whoops! We went too far -- we only exit the loop when remaining is
        // *less than* zero. In order to stop while remaining is still
        // nonnegative, we need to go back one iteration.
        numIterations -= 1;

        return numIterations;
    }

    /**
     * Exponentiates two integers, raising the first integer to the power of the second.
     */
    public static int exponent(int base, int power) {
      // To calculate base^power, we need to multiply 'base' by itself, and repeat this 'power' times.
      int total = 1;
      while (power > 0) {
        // Luckily, we've already defined a way to multiply two numbers!
        total = multiply(total, base);
        power--;
      }
      return total;
    }

    /**
     * Takes the logarithm of the first input integer, where the second integer is the logarithm
     * base.
     */
    public static double logarithm(int number, int base) {
      // We can't *really* calculate a *true* logarithm here, because that's really hard. Instead,
      // we have to cheat a little bit.
      //
      // We're just going to try raising base to a higher and higher power, until we exceed number
      // -- then, we'll have found the logarithm.
      int power = 1;
      while (exponent(base, power) < number) {
        power++;
      }

      // Whoops! Once we exit the loop, the power is one number too high
      // (since exponent(base, power) >= number). We need to return the previous number.
      return (power - 1);
    }
}

