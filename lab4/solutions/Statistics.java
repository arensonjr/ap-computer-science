package lab4.solutions;

class Statistics {
    public static void main(String[] args) {
        // Read in the input data points
        double[] dataPoints = ParseInput.asDoubles(args);

        System.out.println("Min:      " + min(dataPoints));
        System.out.println("Max:      " + max(dataPoints));
        System.out.println("Mean:     " + average(dataPoints));
        System.out.println("Variance: " + variance(dataPoints));
    }

    /**
     * Returns the minimum element of the array.
     */
    public static double min(double[] data) {
        // We need somewhere to start, so the min so far is the first element
        // of the array.
        // 
        // If I start with "minSoFar = 0", then what about an array of [5, 10,
        // 50]? The number 0 is less than all of those, so we'll never find the
        // true minimum of the array -- we'll just return 0. Okay, you say, so
        // we could start with "minSoFar = 1000"; but now what about the array
        // [2000, 3000]? etc. etc.
        //
        // The only way we can be *sure* to have a starting number that's *at
        // least* as big as any of the numbers in the array is to start with
        // one of the numbers in the array.
        double minSoFar = data[0];

        for (double dataPoint : data) {
            if (dataPoint < minSoFar) {
                minSoFar = dataPoint;
            }
        }
        return minSoFar;
    }

    /**
     * Returns the maximum element of the array.
     */
    public static double max(double[] data) {
        // See comments in min() for why we start with data[0].
        double maxSoFar = data[0];

        for (double dataPoint : data) {
            if (dataPoint > maxSoFar) {
                maxSoFar = dataPoint;
            }
        }
        return maxSoFar;
    }

    /**
     * Returns the arithmetic mean ("average") of all of the numbers in the array.
     */
    public static double average(double[] data) {
        double total = sum(data);
        double numItems = data.length;
        return total / numItems;
    }

    /**
     * Returns the statistical variance of all of the numbers in the array.
     *
     * The variance is the sum of squares of differences from the mean, for
     * each element in the input array. For more information, see
     * http://www.mathsisfun.com/data/standard-deviation.html
     */
    public static double variance(double[] data) {
        double mean = average(data);

        // For each element, compute (mean - element)^2
        double[] squaredDifferences = new double[data.length];
        int i = 0;
        while (i < data.length) {
            double difference = mean - data[i];
            squaredDifferences[i] = difference * difference;
            i++;
        }

        // Take the sum of those numbers, and divide by N (the number of data points)
        double sumOfSquaredDifferences = sum(squaredDifferences);
        double numItems = data.length;
        return sumOfSquaredDifferences / numItems;
    }

    /**
     * Returns the sum of all of the numbers in the array.
     */
    public static double sum(double[] data) {
        // Note: This is just a helper function I wrote to make the rest of the
        // lab easier. Notice how many times we were finding the sum of items
        // in an array!
        double total = 0;
        for (double number : data) {
            total += number;
        }
        return total;
    }
}
