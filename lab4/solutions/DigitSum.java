package lab4.solutions;

class DigitSum {
    public static void main(String[] args) {
        int number = Integer.parseInt(args[0]);
        if (number < 0) {
            System.err.println("Error: " + number + " is negative!");
        } else {
            System.out.println(digitSum(number));
        }
    }

    /**
     * Computes the sum of all of the digits in a positive number.
     */
    public static int digitSum(int number) {
        int remainingDigits = number;

        int total = 0;
        while (remainingDigits > 0) {
            int digit = remainingDigits % 10;
            total += digit;
            
            // Integer division truncates; this means that dividing by 10 is
            // the same as chopping off the final digit (which we already added
            // to our running total)
            remainingDigits = remainingDigits / 10;
        }

        // Once the remaining number has reached zero, we've looked at all of
        // the digits.
        return total;
    }
}
