package lab4.solutions;

class CavemanArithmetic {
    public static void main(String[] args) {
        int firstInput = Integer.parseInt(args[0]);
        int secondInput = Integer.parseInt(args[1]);

        System.out.println("Product:  " + multiply(firstInput, secondInput));
        System.out.println("Quotient: " + divide(firstInput, secondInput));
    }

    /**
     * Multiplies two integers, without using the '*' operator.
     */
    public static int multiply(int first, int second) {
        int i = 0;
        int product = 0;

        // first * second = (first + first + ... + first)
        //                = first, added to itself 'second' times
        while (i < second) {
            product += first;
            i++;
        }

        return product;
    }

    /**
     * Divides two integers, without using the '/' operator.
     */
    public static int divide(int first, int second) {
        // first/second is the number of times that second goes into first. So,
        // we'll count the number of times we can subtract second from first.
        int numIterations = 0;
        int remaining = first;
        while (remaining >= 0) {
            remaining -= second;
            numIterations++;
        }

        // Whoops! We went too far -- we only exit the loop when remaining is
        // *less than* zero. In order to stop while remaining is still
        // nonnegative, we need to go back one iteration.
        numIterations -= 1;

        return numIterations;
    }
}
