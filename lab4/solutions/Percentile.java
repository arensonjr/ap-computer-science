package lab4.solutions;

class Percentile {
    public static void main(String[] args) {
        // My score is the first score.
        int myScore = Integer.parseInt(args[0]);

        // All of the inputs are the class's scores.
        int[] allScores = new int[args.length];
        int i = 0;
        while (i < args.length) {
            allScores[i] = Integer.parseInt(args[i]);
            i++;
        }

        printPercentile(myScore, allScores);
    }

    /**
     * Prints out the percentile of the first number within the array.
     *
     * For example, 5 is in the 50th percentile of [0, 1, 2, 3, 4, 5, 6, 7, 8, 9].
     */
    public static void printPercentile(int first, int[] all) {
        // Where does my score fall in the distribution?
        int numScores = all.length;
        int numLessThanFirst = 0;
        for (int score : all) {
            if (score <= first) {
                numLessThanFirst++;
            }
        }

        double percentile = 100.0 * numLessThanFirst / numScores;
        System.out.println(first + " is in the " + percentile + "th percentile.");
    }
}
