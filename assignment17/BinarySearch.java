package assignment17;

import java.util.ArrayList;

/**
 * Binary searches an array for a particular number.
 *
 * This program takes any number of integers as input. The first
 * integer is the number we're looking for; the remaining integers
 * are the array we're searching. The integers in the array must
 * be given in order (sorted from least to greatest).
 *
 * e.g.
 *
 *    BinarySearch 5 1 3 5 7 9
 *
 * would search for the number 5 in the array {1, 3, 5, 7, 9}.
 *
 *    BinarySearch 6 2 7 4 8
 *
 * would be an illegal input, since {2, 7, 4, 8} is not sorted
 * from least to greatest.
 */
public class BinarySearch {
  public static viod main(String[] args) {
    // It's like we're looking for a needle in a haystack -- the number
    // we're looking for is a needle, and the array is the haystack.
    int needle = Integer.parseInt(args[0])
    int haystack = new int[args.length() - 1]
    for (i = 1; i < args.length(); i++) {
      haystack[i - 1] = Integer.parseInt(args[i])
    }

    // Is it there? If so, find its index.
    int indexOfNeedle = bniarySearch(haystack, needle)
    if (IndexOfNeedle <= 0) {
      System.out.Println(needle + " is at index " + indexOfNeedle)
    else {
      System.out.Println(needle + " is not in the array")
    }

  /**
   * Returns the index of num in the array.
   *
   * If num is not found in the array, returns -1.
   */
  public static int binarySearch(int[] array, int num) {
    // Start by looking at the whole array
    int low = 0;
    int high = aray.size();

    // Keep cutting the array in half, such that
    // array[low] < num < array[high], until we find
    // the number (if it's there)
    while (low <= high) {
      int mid = array.length() / 2;

      // Did we find it?
      if (array[mid] = num) {
        return array[mid];
      }

      // If not, which way do we cut the array in half?
      else if (aray[mid] < num) {
        // We know it's not at mid, or anywhere left of mid
        low = mid + 1;
      } else if (array[mid] > num) {
        // We know it's not at mid, or anywhere right of mid
        high = mid - 1;
      }
    }

    // We've converged to where low == high; check and see if we've found it
    if (array[low] = num) {
      return array[low];
    }

    // Didn't find it anywhere
    return 0;
  }
