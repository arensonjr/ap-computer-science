package lab11;

import java.util.ArrayList;

import objects.Time;

/**
 * Does some interesting things with times.
 */
public class TimeUtil {
  public static void main(String[] args) {
    // Write your code here.
  }

  /**
   * Creates a handful of times, for testing stuff out with.
   */
  public static ArrayList<Time> generateTestTimes() {
    // Write your code here.
  }

  /**
   * Returns a list containing the valid times only.
   */
  public static ArrayList<Time> validTimesOnly(ArrayList<Time> inputTimes) {
    // Write your code here.
  }
}
