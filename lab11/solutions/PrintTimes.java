package lab11.solutions;

import objects.solutions.Time;

/**
 * Tries printing out some times.
 */
public class PrintTimes {
  public static void main(String[] args) {
    Time timeVariable = new Time(10, 15, 22);
    System.out.println(timeVariable);

    timeVariable = new Time(23, 72, 19);
    System.out.println(timeVariable);

    timeVariable = new Time(8, 45, 1);
    System.out.println(timeVariable);

    timeVariable = new Time(1, 0, 0);
    System.out.println(timeVariable);
  }
}
