package lab11.solutions;

/**
 * Class to represent an interesting geographical location.
 */
public class PointOfInterest {
  double latitude;
  double longitude;
  String name;
  int yearCreated;

  public PointOfInterest(double lat, double lon, String n, int yc) {
    latitude = lat;
    longitude = lon;
    name = n;
    yearCreated = yc;
  }

  public double getLatitude() {
    return latitude;
  }

  public double getLongitude() {
    return longitude;
  }

  public String getName() {
    return name;
  }

  public int getYearCreated() {
    return yearCreated;
  }

  public void setLatitude(double newValue) {
    latitude = newValue;
  }

  public void setLongitude(double newValue) {
    longitude = newValue;
  }

  public void setName(String newValue) {
    name = newValue;
  }

  public void setYearCreated(int newValue) {
    yearCreated = newValue;
  }
}
