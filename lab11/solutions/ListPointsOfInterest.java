package lab11.solutions;

import java.util.ArrayList;

/**
 * Lists out some cool points of interest
 */
public class ListPointsOfInterest {
  public static void main(String[] args) {
    ArrayList<PointOfInterest> points = generatePointsOfInterest();
    for (int i = 0; i < points.size(); i++) {
      PointOfInterest point = points.get(i);
      System.out.println(
          i + ") "
          + point.getName()
          + " [opened in " + point.getYearCreated() + "]");
    }

    System.out.println("Wait, the Taj Mahal was built in 1648!");
    points.get(2).setYearCreated(1648);

    for (int i = 0; i < points.size(); i++) {
      PointOfInterest point = points.get(i);
      System.out.println(
          i + ") "
          + point.getName()
          + " [opened in " + point.getYearCreated() + "]");
    }
  }

  /**
   * Generates a few points of interest.
   */
  public static ArrayList<PointOfInterest> generatePointsOfInterest() {
    ArrayList<PointOfInterest> allPoints = new ArrayList<>();
    allPoints.add(new PointOfInterest(37.8, -122.5, "Golden Gate Bridge", 1937));
    allPoints.add(new PointOfInterest(38.6, -90.2, "St. Louis Arch", 1965));
    allPoints.add(new PointOfInterest(27.2, 78.0, "Taj Mahal", 2015));
    return allPoints;
  }
}
