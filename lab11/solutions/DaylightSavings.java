package lab11.solutions;

import objects.solutions.Time;

/**
 * Adjusts an input clock for daylight savings time.
 */
public class DaylightSavings {
  public static void main(String[] args) {
    Time time = new Time(
        Integer.parseInt(args[0]),
        Integer.parseInt(args[1]),
        Integer.parseInt(args[2]));

    System.out.println("Old time: " + time);

    time.advanceHours(1);

    System.out.println("New time: " + time);
  }
}
