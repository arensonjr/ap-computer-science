package lab11.solutions;

import java.util.ArrayList;

import objects.solutions.Time;

/**
 * Does some interesting things with times.
 */
public class TimeUtil {
  public static void main(String[] args) {
    ArrayList<Time> times = generateTestTimes();

    // ArrayList pretty-prints, and now Time pretty-prints too, so all we need
    // to do is this! Awesome!
    System.out.println(times);

    System.out.println(validTimesOnly(times));
  }

  /**
   * Creates a handful of times, for testing stuff out with.
   */
  public static ArrayList<Time> generateTestTimes() {
    ArrayList<Time> allTimes = new ArrayList<>();
    allTimes.add(new Time(10, 15, 22)); // 10:15.22
    allTimes.add(new Time(23, 72, 19)); // 23:72.19
    allTimes.add(new Time(8, 45, 01)); // 08:45.01
    allTimes.add(new Time(1, 0, 0)); // 01:00.00
    allTimes.add(new Time(12, 30, 0)); // 12:30.00
    allTimes.add(new Time(99, 99, 99)); // 99:99.99
    allTimes.add(new Time(4, 59, 59)); // 4:59.59
    return allTimes;
  }

  /**
   * Returns a list containing the valid times only.
   */
  public static ArrayList<Time> validTimesOnly(ArrayList<Time> inputTimes) {
    ArrayList<Time> validTimes = new ArrayList<>();
    for (Time time : inputTimes) {
      if (time.isValid()) {
        validTimes.add(time);
      }
    }
    return validTimes;
  }
}
