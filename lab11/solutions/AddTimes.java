package lab11.solutions;

import objects.solutions.Time;

/**
 * Adds two input times together.
 */
public class AddTimes {
  public static void main(String[] args) {
    Time time1 = new Time(
        Integer.parseInt(args[0]),
        Integer.parseInt(args[1]),
        Integer.parseInt(args[2]));
    Time time2 = new Time(
        Integer.parseInt(args[3]),
        Integer.parseInt(args[4]),
        Integer.parseInt(args[5]));

    Time totalTime = new Time(0, 0, 0);
    totalTime.advanceTime(time1);
    totalTime.advanceTime(time2);

    System.out.println(time1 + " + " + time2 + " = " + totalTime);
  }
}
