package provided;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;


/**
 * Provided class to handle requests to a webpage.
 *
 * Any extensions of this class
 */
public abstract class WebHandler implements HttpHandler {
  /**
   * Returns the URL prefix that this handler should respond to.
   *
   * This must be a string that could come after a URL like
   * "http://www.google.com/". Valid return values include, but are not limited
   * to, strings resembling the following:
   *
   *     - "" (matches all URLs)
   *     - "search"
   *     - "music/artists/coldplay"
   */
  public abstract String getPrefix();

  /**
   * Handles an incoming request.
   *
   * The "url" parameter is a list of URL "breadcrumbs" in the incoming
   * request, split by slashes -- for example, if the URL is
   * http://www.your-server.com/foo/bar/baz, then this list will contain
   * ["foo", "bar", "baz"] -- one for each path piece after the base URL.
   *
   * The "data" parameter is a map of each request parameter, by name, to its
   * value. All data coming from the browser is in the form of strings, and
   * might need to be parsed (translated into some other type) by the
   * implementing class.
   *
   * An example input might be:
   *
   *     {"name"="Jeff", "age"="24", "parents"="[Shelley,Dan]"}
   */
  public abstract String handle(List<String> url, Map<String, String> data);


  // -------------------- IMPLEMENTATION DETAILS BELOW --------------------

  @Override
  public void handle(HttpExchange exchange) throws IOException {
    exchange.getResponseHeaders().set("Access-Control-Allow-Origin", "*");

    // Get the URL to pass to handle()
    URI url = exchange.getRequestURI();
    String[] maybeEmptyUrlParts = url.getPath().split("/");
    List<String> urlParts = new ArrayList<>();
    for (String part : maybeEmptyUrlParts) {
      if (!part.isEmpty()) {
        urlParts.add(part);
      }
    }

    // TODO(arensonjr): For now, ignore "favicon.ico" (it's just noise).
    if (url.getPath().equals("/favicon.ico")) {
      exchange.sendResponseHeaders(404, 0);
      exchange.close();
      return;
    }

    try {
      // Extract the data from the request
      Map<String, String> data = new HashMap<>();
      String rawData;
      if (exchange.getRequestMethod().equalsIgnoreCase("get")) {
        rawData = url.getQuery() != null ? url.getQuery() : "";
      } else if (exchange.getRequestMethod().equalsIgnoreCase("post")) {
        rawData = readStream(exchange.getRequestBody());
      } else {
        throw new IOException("Unrecognized method: " + exchange.getRequestMethod());
      }
      String[] params = rawData.split("&");
      for (String param : params) {
        if (param == null || param.isEmpty()) {
          continue;
        }

        String[] keyValue = param.split("=", 2);

        // Unescape any URL-escaped characters on the way out.
        try {
        data.put(
            URLDecoder.decode(keyValue[0], "UTF-8"),
            URLDecoder.decode(keyValue[1], "UTF-8"));
        } catch (ArrayIndexOutOfBoundsException e) {
          throw new IOException("Bad KV pair: " + param);
        }
      }

      // Let the subclass handle the request
      String response = handle(urlParts, data);
      byte[] pageBytes = response.getBytes(StandardCharsets.UTF_8);
      exchange.getResponseHeaders().set("Access-Control-Allow-Origin", "*");
      exchange.sendResponseHeaders(200, pageBytes.length);
      exchange.getResponseBody().write(pageBytes);
      exchange.close();
      System.err.format("[200] [%s] OK\n", url);
    } catch (Throwable t) {
      t.printStackTrace();
      exchange.sendResponseHeaders(500, 0);
      exchange.close();
      System.err.format("[500] [%s] Error: %s\n", url, t);
    }
  }

  /**
   * Reads all bytes out of a stream, as UTF-8 data.
   */
  private String readStream(InputStream stream) throws IOException {
    char[] buf = new char[1024];
    try (BufferedReader reader = new BufferedReader(new InputStreamReader(stream))) {
      StringBuilder out = new StringBuilder();
      int read;
      while ((read = reader.read(buf)) > 0) {
        out.append(buf, 0, read);
      }
      return out.toString();
    }
  }
}
