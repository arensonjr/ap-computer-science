package provided;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;


/**
 * Web handler that serves static files (i.e. just the content of the files
 * themselves) from within a particular folder.
 *
 * This can be used by passing the folder name to the constructor, for example:
 *
 *     List<WebHandler> handlers;
 *     handlers.add(new StaticFilesHandler("forum/static");
 *     ...
 *     new WebServer(handlers);
 *
 * These files will all be served relative to this path on the "static" URL
 * prefix. For example, to reach the file forum/static/foo/bar/baz.jpg, you'd
 * want the url "static/foo/bar/baz.jpg".
 */
public class StaticFilesHandler extends WebHandler {
  /**
   * The path to the folder whose files we're serving.
   */
  private final Path folderPath;

  public StaticFilesHandler(String folder) {
    folderPath = Paths.get(folder);
  }

  @Override
  public String getPrefix() {
    return "static";
  }

  @Override
  public String handle(List<String> url, Map<String, String> data) {
    // All we need is the URL (but ignore "static/").
    Path filePath = folderPath;
    for (String urlPart : url.subList(1, url.size())) {
      filePath = filePath.resolve(urlPart);
    }

    // Try to read the file contents; if we fail, just give up (loudly)
    try {
      return new String(Files.readAllBytes(filePath), StandardCharsets.UTF_8);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }
}

