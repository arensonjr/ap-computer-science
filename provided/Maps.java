package provided;

import java.util.Map;

/**
 * Helper functions for dealing with maps.
 */
public class Maps {
  /**
   * Pretty-prints a map (HashMap is one kind of map).
   *
   * Suppose you create the following map:
   *
   *   HashMap<String, Integer> m = new HashMap<>();
   *   m.put("hello", 5);
   *   m.put("world", 20);
   *   m.put("!!!", -5);
   *   Maps.prettyPrint(m);
   *
   * This function would print the following:
   *
   *   {
   *     hello --> 5
   *     world --> 20
   *     !!! --> -5
   *   }
   */
  public static <K, V> void prettyPrint(Map<K, V> map) {
    System.out.println("{");
    for (Map.Entry<K, V> entry : map.entrySet()) {
      System.out.println("\t" + entry.getKey() + " --> " + entry.getValue());
    }
    System.out.println("}");
  }
}
