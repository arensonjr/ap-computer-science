package provided;

import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.List;

/**
 * Provided class to connect Java code to a Cloud9 webpage.
 *
 * This class should be constructed with a number of handlers that will respond
 * to a particular URL prefix, and will be tried in order. For example:
 *
 *   public class AllUrlsHandler extends WebHandler {
 *     ...
 *     public String getPrefix() { return ""; }
 *     ...
 *   }
 *
 *   public class GamesHandler extends WebHandler {
 *     ...
 *     public String getPrefix() { return "games"; }
 *     ...
 *   }
 *
 *   List<Handler> handlers = new ArrayList<>();
 *   handlers.add(new GamesHandler());
 *   handlers.add(new AllUrlsHandler());
 *   WebServer server = new WebServer(handlers);
 *
 * Now, this server will send all URLs that start with "/games" to
 * GamesHandler, and all other URLs to AllUrlsHandler.
 *
 * Important: Since it tries all handlers in order, if AllUrlsHandler was added
 * to the list _before_ GamesHandler, then GamesHandler would never get any
 * requests -- they would all match AllUrlsHandler! Make sure you add your
 * handlers in order from most to least specific.
 */
public final class WebServer {
  // Whoah, there are two constructors! One of them actually _calls the other one_ using "this(...)"
  // syntax. That makes it as if whoever constructed this object had actually used the other
  // constructor, with these parameters.
  public WebServer(List<WebHandler> handlers) {
    this(handlers, "");
  }

  /**
   * Takes an explicit homepage to print out, to direct users to the right page.
   */
  public WebServer(List<WebHandler> handlers, String homepage) {
    try {
      // Create the server based on the URLs it can handle
      HttpServer server = HttpServer.create();
      for (WebHandler handler : handlers) {
        server.createContext('/' + handler.getPrefix(), handler);
      }

      // Tell the server to run where Cloud9 knows how to connect to it
      int port = Integer.parseInt(System.getenv("C9_PORT"));
      String ip = System.getenv("C9_IP");
      server.bind(new InetSocketAddress(ip, port), port);
      server.start();

      // Tell the user which URL to use in their browser/applications
      String hostname = System.getenv("C9_HOSTNAME");
      System.out.format("Running at http://%s/%s\n", hostname, homepage);
    } catch (IOException e) {
      // Uh-oh, something went wrong! Exit the program and let the user try again.
      System.err.println("Unable to start server:");
      e.printStackTrace();
      System.exit(1);
    }
  }
}
