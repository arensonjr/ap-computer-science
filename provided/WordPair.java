package provided;

/**
 * Container class that holds a pair of words.
 */
public class WordPair {
  // Internal variable to hold the first and second words.
  private final String first;
  private final String second;

  /**
   * Creates a new word pair out of the two input words.
   */
  public WordPair(String first, String second) {
    this.first = first;
    this.second = second;
  }

  /**
   * Returns the first word of this pair.
   */
  public String getFirstWord() {
    return first;
  }

  /**
   * Returns the second word of this pair.
   */
  public String getSecondWord() {
    return second;
  }

  // --------- Implementation Details (so that WordPair can be put into maps and lists) ---------

  /**
   * Returns a hash code for this WordPair.
   *
   * A "hash code" is a number that must be the same for any two WordPairs to
   * be equal; however, if it *is* the same, that doesn't necessarily say that
   * the WordPairs *are* equal. Essentially, it's a very quick test to tell you
   * if these two objects *might* be equal.
   *
   * You may recognize the word "hash" from the HashMap data structure -- yes,
   * it's the same word. In fact, one of the only reasons we implement a
   * hashCode() method in this class is so that a WordPair can be used as the
   * key in a HashMap.
   */
  @Override
  public int hashCode() {
    return first.hashCode() * 31 + second.hashCode();
  }

  /**
   * Returns whether this WordPair instance is equal to an arbitrary object.
   *
   * WordPairs can only be equal to objects that are also WordPairs, and
   * whose first and second words are the same as this object's first and
   * second words.
   */
  @Override
  public boolean equals(Object other) {
    return (other instanceof WordPair)
      && ((WordPair) other).first.equals(first)
      && ((WordPair) other).second.equals(second);
  }
}

