package codingbat.solutions;

/**
 * Figures out whether or not we should sleep in.
 */
public class SleepIn {
  public static void main(String[] args) {
    testWeekday();
    testWeekdayVacation();
    testWeekend();
    testWeekendVacation();
  }

  public static void testWeekday() {
    if (sleepIn(true, false) == true) {
      System.out.println("[testWeekday] FAIL: You should not sleep in on a weekday");
    } else {
      System.out.println("[testWeekday] PASS");
    }
  }

  public static void testWeekdayVacation() {
    if (sleepIn(true, true) == false) {
      System.out.println("[testWeekdayVacation] FAIL: You should sleep in on a weekday if it's a vacation");
    } else {
      System.out.println("[testWeekdayVacation] PASS");
    }
  }

  public static void testWeekend() {
    if (sleepIn(false, false) == false) {
      System.out.println("[testWeekend] FAIL: You should sleep in on a weekend");
    } else {
      System.out.println("[testWeekend] PASS");
    }
  }

  public static void testWeekendVacation() {
    if (sleepIn(true, true) == false) {
      System.out.println("[testWeekendVacation] FAIL: You should sleep in on a weekend, even if it's a vacation too");
    } else {
      System.out.println("[testWeekendVacation] PASS");
    }
  }

  public static boolean sleepIn(boolean weekday, boolean vacation) {
    return (!weekday || vacation);
  }
}
