package multisearch;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

public class SearchEngine {

  private static final boolean DEBUG = true;

  private Index index;
  private WebPageComparator comparator;
  private boolean printRankingInformation;

  public SearchEngine(Index i, WebPageComparator c) {
    index = i;
    comparator = c;
    printRankingInformation = false;
  }

  /**
   * This is the magic method.
   */
  public List<WebPage> search(String query) {
    // Split the single string query into individual words,
    // sanitizing them in the process.
    List<String> terms = sanitizeSegmentedQuery(segment(query));

    if (DEBUG) {
      System.out.format("SearchEngine: Segmented query: %s\n", terms);
    }

    // Get a union all of the WebPages which contain the searched terms.
    List<WebPage> results = index.getUnionOf(terms);
    if (results != null) {
      // If the index has mappings, return the ranked pages
      return rank(results, terms);
    } else {
      // Otherwise, return an empty list.
      return Collections.emptyList();
    }
  }

  /**
   * Rank a list of results for a given search term.
   *
   * Since Search Engines present their results as most significant first,
   * we reverse the comparator, so that larger numbers appear first.
   *
   * If printRankingInformation is true, this method will output each page's
   * score to the console.
   */
  private List<WebPage> rank(List<WebPage> results, List<String> terms) {
    comparator.setTerms(terms); // Set the comparator's term
    Collections.sort(results, Collections.reverseOrder(comparator)); // Sort

    if (printRankingInformation) {
      for (WebPage page: results) {
        System.out.format("%s: %d\n", page.getUrl(), comparator.score(page));
      }
    }

    return results;
  }

  /**
   * Split a query string by spaces.
   */
  private List<String> segment(String query) {
    return Arrays.asList(query.trim().split("\\s+"));
  }

  /**
   * Sanitize segmented user input.
   */
  private List<String> sanitizeSegmentedQuery(List<String> query) {
    ArrayList<String> tmpList = new ArrayList<>();
    for (String s: query) {
      tmpList.add(s.toLowerCase().trim());
    }
    return tmpList;
  }

  /**
   * Sanitize user input.
   */
  private String sanitizeQuery(String query) {
    return query.toLowerCase().trim();
  }

  /**
   * Set true to have the rank() method print website scores.
   */
  public void setPrintRankingInformation(boolean value) {
    printRankingInformation = value;
  }

  public WebPageComparator getWebPageComparator() {
    return comparator;
  }

  public void setWebPageComparator(WebPageComparator newComparator) {
    comparator = newComparator;
  }

  public void setIndex(Index newIndex) {
    index = newIndex;
  }

}
