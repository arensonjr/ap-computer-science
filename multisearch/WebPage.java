package multisearch;

import java.io.BufferedReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class WebPage {

  // The URL of this page
  private URL url;

  // A list of the words on the page
  private List<String> words;

  public WebPage(URL u) {
    url = u;

    // Get the contents of the page
    try (BufferedReader reader = WebPageIO.read(url)) {
      words = WebPageIO.downloadContents(reader);
    } catch (IOException e) {
      // Fall back to an empty list of words when the webpage is inaccessible
      System.err.println("Failed to read webpage " + url + ", ignoring it.");
      words = new ArrayList<>();
    }
  }

  public URL getUrl() {
    return url;
  }

  public List<String> getWords() {
    return words;
  }

  public String toString() {
    return url.toString();
  }
}
