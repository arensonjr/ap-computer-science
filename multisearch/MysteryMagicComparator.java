package multisearch;

/**
 * Scores/compares pages by your own magic factors. Be creative!
 */
public class MysteryMagicComparator extends WebPageComparator {
  public Integer score(WebPage page) {
    return 13; // Chosen by fair dice roll, guaranteed to be random
  }
}
