package multisearch;

import java.net.URL;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * The Index powering a search engine.
 *
 * This index is built as a posting board; a Map<String, List<WebPage>>
 * of single-word terms to lists of WebPages upon which the terms appear.
 *
 * Note that the keys are normalized, i.e. "  Word" => "word"
 *
 * For example:
 *
 *   Given a WebPage page = WebPage { words: ["This", "page", "is", "cool"] }
 *
 *   The following entries would be made in the Index:
 *
 *     "this": [page]
 *     "page": [page]
 *     "is": [page]
 *     "cool": [page]
 */
public class Index {

  private static final boolean DEBUG = false;

  // The map which backs this index
  private Map<String, List<WebPage>> map = new HashMap<>();

  /**
   * Create a new index from a list of URLs.
   */
  public Index(List<URL> urls) {
    System.out.format("Constructing index from %d urls\n", urls.size());

    int processed = 0;
    for (URL url: urls) {
      WebPage page = new WebPage(url);
      for (String word: page.getWords()) {
        String cleanedWord = WebPageIO.sanitize(word);
        put(cleanedWord, page); // Put the sanitized version of this word
      }
      System.out.format("\033[KProcessed %d of %d [%s]\r",
          ++processed, urls.size(), url);
    }
    // Clear out the line so that there isn't cruft hanging out in the terminal
    System.out.format("\033[KProcessed %d of %d URLs.", processed, urls.size());
    System.out.println(
        "                                                                    ");
  }

  /**
   * Look up a term in the map. Returns null if the term isn't mapped.
   */
  public List<WebPage> get(String term) {
    return map.get(term);
  }

  /**
   * Look up a list of terms in the map, unioning the results.
   *
   * Returns an empty list if the term isn't mapped.
   */
  public List<WebPage> getUnionOf(List<String> terms) {
    // There are more elegant ways to do this, but for simplicity we'll do this
    Set<WebPage> results = new HashSet<>();
    for (String term: terms) {
      if (DEBUG) {
        System.out.format("Index: Unioning results for \"%s\".\n", term);
      }
      List<WebPage> pages = get(term);
      if (pages == null) {
        continue;
      } else {
        for (WebPage page: pages) {
          results.add(page);
        }
      }
    }
    return new ArrayList<WebPage>(results);
  }

  /**
   * Map a term to a page in the index.
   */
  private void put(String term, WebPage page) {
    List<WebPage> mappedPages = map.get(term);
    if (mappedPages == null) {
      List<WebPage> newList = new ArrayList<WebPage>();
      newList.add(page);
      map.put(term, newList);
    } else {
      if (!mappedPages.contains(page)) {  // Avoid duplicates, without a set...
        mappedPages.add(page);
      }
    }
  }

  public String toString() {
    return Arrays.toString(map.entrySet().toArray());
  }

}
