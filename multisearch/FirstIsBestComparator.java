package multisearch;

/**
 * Scores pages by the sum of how early the terms appear.
 * Earlier occurrences score higher.
 */
public class FirstIsBestComparator extends WebPageComparator {

  private static final int MAX_SCORE = 10000;

  /**
   * Here we get the scores for each term in the search using another function,
   * and then we add all of them together (like I noted in the assignment doc)
   */
  public Integer score(WebPage page) {
    int finalScore = 0;
    for (String term: terms) {
      finalScore += scorePageForTerm(page, term);
    }
    return finalScore;
  }

  /**
   * Generate a score for one particular term.
   */
  private Integer scorePageForTerm(WebPage page, String term) {
    return 0;  // TODO: Implement this!
  }
}
