package multisearch;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Utility functions for downloading and processing web page contents.
 */
public class WebPageIO {

  /**
   * Download the contents of a page provided by a BufferedReader.
   *
   * This process normalizes the contents of the page:
   * - HTML tags are stripped
   * - The resulting text is split on whitespace
   * - The split words are sanitized according to the sanitize() function.
   *
   * Finally, the words are added to the words list.
   */
  public static List<String> downloadContents(BufferedReader reader)
      throws IOException {
    List<String> contents = new ArrayList<>();

    String line;
    while ((line = reader.readLine()) != null) {
      String taglessLine = stripHtmlElements(line);
      for (String word: taglessLine.split("\\s+")) {
        String sanitizedWord = sanitize(word);
        if (!"".equals(sanitizedWord)) { // We don't care about empty strings
          contents.add(sanitizedWord);
        }
      }
    }

    return contents;
  }

  public static BufferedReader read(URL url) throws IOException {
    return new BufferedReader(new InputStreamReader(url.openStream()));
  }

  /**
   * Hillbilly HTML element removal.
   */
  public static String stripHtmlElements(String s) {
    return s.replaceAll("<[^>]*>", " ");
  }

  /**
   * Hillbilly word sanitization.
   */
  public static String sanitize(String s) {
    return s
        .toLowerCase()
        .trim()
        .replaceAll("\\s*\\p{Punct}+\\s*", ""); // Remove leading and trailing punctuation
  }

}