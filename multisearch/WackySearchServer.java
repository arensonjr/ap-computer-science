package multisearch;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import java.net.URI;
import java.net.URL;
import java.net.URLDecoder;
import java.net.InetSocketAddress;

import java.nio.charset.StandardCharsets;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.LinkedList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

/**
 * This class wraps the SearchEngine object into a web server.
 */
public class WackySearchServer {

  private static final String DEFAULT_URL_LIST = "this-assignment.txt";

  private static final HashSet<String> ALLOWED_URL_LISTS = new HashSet<>();
  static {
    ALLOWED_URL_LISTS.add("dog-wikiloop.txt");
    ALLOWED_URL_LISTS.add("kitten-wikiloop.txt");
    ALLOWED_URL_LISTS.add("small-url-list.txt");
    ALLOWED_URL_LISTS.add("this-assignment.txt");
    ALLOWED_URL_LISTS.add("top-25-sites.txt");
    ALLOWED_URL_LISTS.add("top-100-sites.txt");
  }

  /**
   * File containing the WackySearch home page HTML template.
   */
  private static final Path MAIN_PAGE_TEMPLATE =
    Paths.get("multisearch", "static", "templates", "wackysearch_main.html");

  /**
   * File containing the WackySearch results page HTML template.
   */
  private static final Path RESULTS_PAGE_TEMPLATE =
    Paths.get("multisearch", "static", "templates", "wackysearch_results.html");

  /**
   * File containing the WackySearch HTML template for an empty results page.
   */
  private static final Path RESULTS_PAGE_TEMPLATE_EMPTY =
    Paths.get("multisearch", "static", "templates", "wackysearch_results_empty.html");

  /**
   * File containing the HTML template for a search result.
   */
  private static final Path SEARCH_RESULT_ITEM =
    Paths.get("multisearch", "static", "templates", "search_result_item.html");

  /**
   * File containing the HTML template for each URL file in the left sidebar.
   */
  private static final Path URL_FILE_LINK_ITEM_TEMPLATE =
    Paths.get("multisearch", "static", "templates", "url_file_link_item.html");

  /**
   * Directory containing static resources to serve.
   */
  private static final Path STATIC_RESOURCES =
      Paths.get("multisearch", "static", "resources");

  /**
   * Directory containing files which specify lists of URLs to explore.
   */
  private static final Path URL_LISTS_DIR =
      Paths.get("multisearch", "static", "url-lists");

  // A number of comparators
  private static WebPageComparator termFrequencyComparator =
      new TermFrequencyComparator();

  private static WebPageComparator firstIsBestComparator =
      new FirstIsBestComparator();

  private static WebPageComparator alphabeticalUrlComparator =
      new AlphabeticalUrlComparator();

  private static WebPageComparator mysteryMagicComparator =
      new MysteryMagicComparator();

  /**
   * The current URL list.
   */
  private static List<URL> urls;

  /**
   * The filename of the current URL list;
   */
  private static String urlListFilename;

  /**
   * The current index.
   */
  private static Index index;

  /**
   * The magic device.
   */
  private static SearchEngine searchEngine;


  public static void main(String[] args) throws IOException {
    // Set up the Search Engine
    urlListFilename = DEFAULT_URL_LIST;
    urls = readUrls(urlListFilename);
    index = new Index(urls);
    searchEngine = new SearchEngine(index, termFrequencyComparator);
    searchEngine.setPrintRankingInformation(true); // For fun and profit

    // Set up the server
    HttpServer server = HttpServer.create();
    server.createContext("/", new WackySearchRootHandler());
    server.createContext("/comparator", new ComparatorSwitchHandler());
    server.createContext("/url-list", new UrlListSwitchHandler());
    server.createContext("/resources", new StaticResourcesHandler());

    int port = Integer.parseInt(System.getenv("C9_PORT"));
    String ip = System.getenv("C9_IP");
    server.bind(new InetSocketAddress(ip, port), port);

    server.start();
    String hostname = System.getenv("C9_HOSTNAME");
    System.out.format("Running at http://%s\n", hostname);
  }

  /**
   * Read a text file in URL_LISTS_DIR, returning the contents as a List<URL>
   *
   * If filename can't be read, this function returns an empty list.
   */
  private static List<URL> readUrls(String filename) {
    System.out.format("Reading URLs from %s...\n", filename);

    List<URL> urls = new ArrayList<>();
    try {
      Path file = URL_LISTS_DIR.resolve(filename);
      for (String line: Files.readAllLines(file, StandardCharsets.UTF_8)) {
        urls.add(new URL(line));
      }
      System.out.format("Read %d URLs.\n", urls.size());
      return urls;
    } catch (IOException e) {
      System.err.format("Failed to read file:\n", filename);
      System.err.println(e);
    }
    return urls;
  }

  /**
   * Decode query string garbage.
   *
   * Courtesy of
   * http://stackoverflow.com/questions/13592236/parse-a-uri-string-into-name-value-collection
   *
   * With some modifications, because never trust code from SO.
   */
  private static Map<String, List<String>> parseQueryString(String query)
      throws UnsupportedEncodingException {
    final Map<String, List<String>> queryPairs =
        new LinkedHashMap<String, List<String>>();
    final String[] pairs = query.split("&");
    for (String pair : pairs) {
      final int idx = pair.indexOf("=");
      final String key = idx > 0
          ? URLDecoder.decode(pair.substring(0, idx), "UTF-8")
          : pair;
      if (!queryPairs.containsKey(key)) {
        queryPairs.put(key, new LinkedList<String>());
      }
      final String value = idx > 0 && pair.length() > idx + 1
          ? URLDecoder.decode(pair.substring(idx + 1), "UTF-8")
          : "";
      queryPairs.get(key).add(value);
    }
    return queryPairs;
  }

  /**
   * Given a HTTP request query string and a key, attempts to extract a value.
   *
   * Will always return a string, either empty if there is no search term to
   * extract, or the term which was extracted.
   */
  private static String extractValueForKey(String key, String query) {
    if (query == null || query.isEmpty()) {
      return "";
    } else { // Try to interpret the query
      Map<String, List<String>> urlQueryMap;
      try {
        urlQueryMap = parseQueryString(query);
      } catch (UnsupportedEncodingException e) {
        System.out.format("Unable to parse query string %s\n", query);
        return "";
      }

      // Pull the value out of the query map, if possible
      String value;
      List<String> values = urlQueryMap.get(key);
      if (values == null) {
        value = "";
      } else if (values.size() == 1) {
        key = values.get(0);
      } else { // Fail-safe
        key = "";
      }
      return key;
    }
  }

  /**
   * Handle requests for the WackySearch homepage.
   */
  private static class WackySearchRootHandler implements HttpHandler {
    @Override public void handle(HttpExchange exchange) throws IOException {
      URI url = exchange.getRequestURI();

      try {
        byte[] page;

        // Interpret the query string in the URL
        String searchTerm = extractValueForKey("q", url.getQuery());

        // If there's nothing to search, just render the home page.
        // Otherwise, render the results page
        if (searchTerm.isEmpty()) {
          page = renderMainPage(exchange);
        } else {
          page = renderResultsPage(exchange, searchTerm);
        }

        // Write the HTML to the user
        exchange.sendResponseHeaders(200, page.length);
        exchange.getResponseBody().write(page);
        System.err.format("[200] [%s] OK\n", url);
      } catch (Throwable t) {
        t.printStackTrace();
        System.err.format("[500] [%s] Error: %s\n", url, t);
      } finally {
        exchange.close();
      }
    }

    /**
     * Handle rendering the main page, for when no query has been made.
     */
    private byte[] renderMainPage(HttpExchange exchange) throws IOException {
      System.out.println("Rendering main page.");
      return new String(Files.readAllBytes(MAIN_PAGE_TEMPLATE))
          .replaceAll(
              "\\$\\{currentcomparator\\}",
              searchEngine.getWebPageComparator().toString())
          .replaceAll("\\$\\{currenturlfile\\}", urlListFilename)
          .getBytes();
    }

    /**
     * Handle rendering the results page, for when a query has been made.
     */
    private byte[] renderResultsPage(HttpExchange exchange, String query)
        throws IOException {
      System.out.format("Getting results for \"%s\"\n", query);

      // Get the results from the engine and render them to the template
      List<WebPage> results = searchEngine.search(query);
      if (results.isEmpty()) {  // Render "nada"
        return new String(Files.readAllBytes(RESULTS_PAGE_TEMPLATE_EMPTY))
            .replaceAll(
                "\\$\\{currentcomparator\\}",
                searchEngine.getWebPageComparator().toString())
            .replaceAll("\\$\\{currenturlfile\\}", urlListFilename)
            .getBytes();
      } else {
        String resultsPageTemplate =
            new String(Files.readAllBytes(SEARCH_RESULT_ITEM));
        StringBuilder resultsPageHtml = new StringBuilder();
        for (WebPage page : results) {
          String html = resultsPageTemplate
              .replaceAll("\\$\\{url\\}", page.getUrl().toString());
          resultsPageHtml.append(html);
        }

        // Render the results to HTML
        return new String(Files.readAllBytes(RESULTS_PAGE_TEMPLATE))
            .replaceAll("\\$\\{resultslist\\}", resultsPageHtml.toString())
            .replaceAll(
                "\\$\\{currentcomparator\\}",
                searchEngine.getWebPageComparator().toString())
            .replaceAll("\\$\\{currenturlfile\\}", urlListFilename)
            .getBytes();
      }
    }

  }

  /**
   * Switch the WebPageComparator being used by the index.
   *
   * Responds to POST requests to /comparator
   */
  private static class ComparatorSwitchHandler implements HttpHandler {
    @Override public void handle(HttpExchange exchange) {
      URI url = exchange.getRequestURI();
      try {
        if ("post".equalsIgnoreCase(exchange.getRequestMethod())) {
          @SuppressWarnings("unchecked")
          BufferedReader reader =
              new BufferedReader(
                  new InputStreamReader(exchange.getRequestBody(),"utf-8"));
          String queryString = reader.readLine();
          String newComparatorName = extractValueForKey("comparator", queryString);

          // Now that we have the requested new comparator,
          // switch based on the possible values:
          //   alpha: AlphabeticalUrlComparator
          //   first: FirstIsBestComparator
          //   myst: MysteryMagicComparator
          //   freq: TermFrequencyComparator
          System.out.format(
              "Switching to comparator \"%s\"\n", newComparatorName);
          switch (newComparatorName) {
            case "alpha":
              searchEngine.setWebPageComparator(alphabeticalUrlComparator);
              break;
            case "first":
              searchEngine.setWebPageComparator(firstIsBestComparator);
              break;
            case "myst":
              searchEngine.setWebPageComparator(mysteryMagicComparator);
              break;
            case "freq":
              searchEngine.setWebPageComparator(termFrequencyComparator);
              break;
            default:
              System.err.format(
                  "Unrecognized comparator \"%s\"\n", newComparatorName);
          }

          // Redirect back to home
          exchange.getResponseHeaders().add("Location", "/");
          exchange.sendResponseHeaders(301, 0);
          System.err.format("[200] [%s] OK\n", url);
        }
      } catch (Throwable t) {
        t.printStackTrace();
        System.err.format("[500] [%s] Error: %s\n", url, t);
      } finally {
        exchange.close();
      }
    }
  }

  /**
   * Switch the URL list being used by the index.
   *
   * Responds to POST requests to /url-list
   */
  private static class UrlListSwitchHandler implements HttpHandler {
    @Override public void handle(HttpExchange exchange) throws IOException {
      URI url = exchange.getRequestURI();
      try {
        if ("post".equalsIgnoreCase(exchange.getRequestMethod())) {
          @SuppressWarnings("unchecked")
          BufferedReader reader =
              new BufferedReader(
                  new InputStreamReader(exchange.getRequestBody(),"utf-8"));
          String queryString = reader.readLine();
          String newUrlListFilename = extractValueForKey("url-list", queryString);

          // Now that we have the requested new URL filename,
          // switch the index if, and only if, the filename if one of:
          //   dog-wikiloop.txt
          //   kitten-wikiloop.txt
          //   small-url-list.txt
          //   this-assignment.txt
          //   top-25-sites.txt
          //   top-100-sites.txt
          if (ALLOWED_URL_LISTS.contains(newUrlListFilename)) {
            System.out.format(
                "Switching to URL List \"%s\"\n", newUrlListFilename);

            List<URL> newUrls = readUrls(newUrlListFilename);
            Index newIndex = new Index(newUrls);
            searchEngine.setIndex(newIndex);

            // Also, don't forget to update this metadata
            urlListFilename = newUrlListFilename;
          } else {
            System.err.format(
                "Unrecognized URL List \"%s\"\n", newUrlListFilename);
          }

          // Redirect back to home
          exchange.getResponseHeaders().add("Location", "/");
          exchange.sendResponseHeaders(301, 0);
          System.err.format("[200] [%s] OK\n", url);
        }
      } catch (Throwable t) {
        t.printStackTrace();
        System.err.format("[500] [%s] Error: %s\n", url, t);
      } finally {
        exchange.close();
      }
    }
  }


  /**
   * Handle requests for static file data.
   */
  private static class StaticResourcesHandler implements HttpHandler {
    @Override public void handle(HttpExchange exchange) throws IOException {
      URI url = exchange.getRequestURI();
      try {
        // Which file are they requesting?
        String request = url.getPath();
        String filename = request.substring(request.lastIndexOf('/') + 1);

        // Write the file out to the client/browser
        exchange.sendResponseHeaders(200, 0 /* chunked encoding */);
        Files.copy(
            STATIC_RESOURCES.resolve(filename),
            exchange.getResponseBody());
        System.err.format("[200] [%s] OK\n", url);
      } catch (Throwable t) {
        t.printStackTrace();
        System.err.format("[500] [%s] Error: %s\n", url, t);
      } finally {
        exchange.close();
      }
    }
  }
}