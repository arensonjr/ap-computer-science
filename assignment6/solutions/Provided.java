package assignment6.solutions;

import java.util.Scanner;

/**
 * Provided code for Assignment 6.
 */
class Provided {
    /**
     * Returns a random integer between 1 and maxValue, inclusive.
     */
    public static int randomNumber(int maxValue) {
        // This gets a random number between 0 and 1, and then multiplies by
        // maxValue to get a random number between 0 and maxValue (exclusive)
        double randomNumber = Math.random() * maxValue;

        // We then add 1 to shift our random number so that it's between 1 and
        // maxValue (inclusive)
        return (int) randomNumber + 1;
    }

    // We need exactly one of these, so it must be declared *outside* of the
    // function.
    private static Scanner input = new Scanner(System.in);

    /**
     * Returns an integer guessed by the user.
     */
    public static int getUserGuess() {
        System.out.print("Guess a number: ");
        while (!input.hasNextInt()) {
            System.err.println(
                    "Error: '" + input.nextLine() + "' is not an integer. Try again.");
        }
        return input.nextInt();
    }

    // Constants representing the user's evaluation of your AI's guesses.
    public static int TOO_LOW = -1;
    public static int JUST_RIGHT = 0;
    public static int TOO_HIGH = 1;

    /**
     * Returns whether the user thinks your guess is too high, too low, or just
     * right.
     */
    public static int checkMyGuess() {
        System.out.println("How's my guess?");
        System.out.println("  -1) Too Low");
        System.out.println("   0) Perfect!");
        System.out.println("   1) Too High");

        // Keep asking for responses until it's either -1, 0, or 1
        int userResponse;
        while (!input.hasNextInt()
                || (userResponse = input.nextInt()) < -1
                || userResponse > 1) {
            System.out.println("Not a valid choice. Try again.");
        }

        // At this point, it must be -1, 0, or 1
        return userResponse;
    }
}
