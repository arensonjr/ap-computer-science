package assignment6.solutions;

/**
 * A program which repeatedly tries to guess the user's secret number (between
 * 1 and 100), making use of hints from the user ("higher" or "lower").
 */
class GuessMyNumberAI {
    public static void main(String[] args) {
        System.out.println("Think of a number between 1 and 100 -- I'm going to try to guess it!");

        // Repeatedly try to guess by binary-searching the user's number
        int low = 1;
        int high = 100;

        int response;
        do {
          response = guess(low, high);
          if (response == Provided.TOO_LOW) {
            low = midpoint(low, high);
          } else if (response == Provided.TOO_HIGH) {
            high = midpoint(low, high);
          }
        } while (response != Provided.JUST_RIGHT);

        // We got it!
        System.out.println("I knew your number was " + midpoint(low, high) + " all along!");
    }

    /**
     * Prints the AI's guess, for the user to evaluate.
     *
     * Returns the user's evaluation of the guess (either
     * Provided.TOO_LOW, Provided.TOO_HIGH, or Provided.JUST_RIGHT).
     */
    public static int guess(int low, int high) {
        int myGuess = midpoint(low, high);
        System.out.println("Is your number " + myGuess + "?");
        return Provided.checkMyGuess();
    }

    /**
     * Returns the average of low and high.
     */
    public static int midpoint(int low, int high) {
        return (low + high) / 2;
    }
}
