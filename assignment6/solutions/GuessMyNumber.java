package assignment6.solutions;

/**
 * A game which prompts the user to guess a random number, given only hints of
 * "higher" and "lower".
 */
class GuessMyNumber {
    public static void main(String[] args) {
        // Generate a number that the user has to guess
        int myNumber = Provided.randomNumber(100);
        System.out.println(
                "I'm thinking of a number between 1 and 100. Can you guess what it is?'");

        // Repeatedly ask the user to guess it
        int userGuess = Provided.getUserGuess();
        while (userGuess != myNumber) {
            if (userGuess < myNumber) {
                System.out.println("Higher!");
            } else {
                System.out.println("Lower!");
            }
        }

        // We only exit the loop when they correctly guess the number.
        System.out.println("That's right, my number was " + myNumber + ".");
        System.out.println("You got it! Congratulations!");
    }
}

