package lab19;

import lab18.HasDimensions;

import java.awt.Point;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents a regular polygon where one vertex points directly upward.
 */
public abstract class RegularPolygon implements Drawable, HasDimensions {
  /**
   * The center of the polygon.
   */
  private Point center;

  /**
   * The number of sides of this polygon.
   */
  private int numSides;

  /**
   * The radius (distance from center to vertex) of the polygon.
   */
  private double radius;

  public RegularPolygon(Point c, int n, double r) {
    numSides = n;
    center = c;
    radius = r;
  }

  /**
   * Returns the vertices of this polygon.
   */
  @Override
  public List<Point2D> getVertices() {
    List<Point2D> vertices = new ArrayList<>();

    // Calculate the location of each vertex
    double centerAngle = 2 * Math.PI / numSides;
    for (int i = 0; i < numSides; i++) {
      double vx = center.getX() + (radius * Math.sin(i * centerAngle));
      double vy = center.getY() + (radius * Math.cos(i * centerAngle));
      vertices.add(new Point2D.Double(vx, vy));
    }

    return vertices;
  }

  /**
   * Returns the area of this polygon.
   */
  public double getArea() {
   // The area of a regular polygon is equal to (1/2) * numSides * apothem * sideLength.
   double centralAngle = 360.0 / numSides;
   double apothem = radius * Math.cos(centralAngle / 2);
   double sideLength = 2 * radius * Math.sin(centralAngle / 2);
   return 0.5 * numSides * apothem * sideLength;
  }

  /**
   * Returns the height of this polygon.
   */
  public double getHeight() {
    // This is easy -- regular polygons have a radius, which is the radius of
    // the circle that circumscribes them. Their max height is equal to twice
    // this radius, just like a circle.
    return radius * 2;
  }

  /**
   * Returns the width of this polygon.
   */
  public double getWidth() {
    // This is easy -- regular polygons have a radius, which is the radius of
    // the circle that circumscribes them. Their max width is equal to twice
    // this radius, just like a circle.
    return radius * 2;
  }
}
