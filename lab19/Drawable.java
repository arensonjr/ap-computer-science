package lab19;

import java.awt.geom.Point2D;
import java.util.List;

/**
 * A polygon, represented by its vertices.
 */
public interface Drawable {
  /**
   * Returns the list of points that comprise this polygon.
   *
   * The points should be returned in a clockwise order, such that if one were to
   * draw the perimeter of the shape by connecting the points in order, that the
   * center of the polygon would always be to the _right_ of the edge being drawn.
   *
   * An N-gon will should exactly N points; there is no need to duplicate the first
   * vertex at the end of the list.
   *
   * Example shape:
   *
   *   ^
   *   |
   *   |  +-------+
   *   |  |       |
   *   |  |       |
   *   |  |       |
   *   |  +-------+
   *   |
   *   +-------------------------->
   *
   * This square might be represented by the list of points:
   *
   *   [(1, 2), (1, 6), (4, 6), (4, 2)]
   */
  List<Point2D> getVertices();
}
