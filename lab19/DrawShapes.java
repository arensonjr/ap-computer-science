package lab19;

import lab19.Drawable;

import java.awt.Point;

/**
 * Draws some shapes to a webpage.
 */
public class DrawShapes {
  public static void main(String[] args) {
    /*
     * 1. Make a drawable object:
    /*
     *    Try uncommenting one of these lines at a time,
     *    and check out what the shape looks like!
     */
    Drawable shape = new Hexagon(new Point(500, 500), 500);
    // Drawable shape = new Diamond(new Point(100, 100), 50);

    /*
     * 2. Draw it to the screen
     */
    Provided.drawToScreen(shape);
  }
}
