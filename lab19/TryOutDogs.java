package lab19;

import java.util.Arrays;
import lab18.HasDimensions;

/**
 * This program is just for experimentation with the Dog class.
 * It has no requirements, so you can mess around with whatever
 * you want here.
 */
public class TryOutDogs {
    public static void main(String[] args) {
        Dog ryley = new Dog(false, "Ryley", "German Shepherd", 25, 36, Arrays.asList("black", "tan", "white"));
        
        System.out.println(ryley);
        
        if(ryley.sheds()) {
            System.out.println("Ryley sheds");
        } else {
            System.out.println("Ryley doesn't shed");
        }
        printDimensions(ryley);
        
    }
    
    public static void printDimensions(HasDimensions hd) {
        System.out.println("Dimensions are: " + hd.getHeight() + " in. tall and "
            + hd.getWidth() + " in. long.");
    }
}