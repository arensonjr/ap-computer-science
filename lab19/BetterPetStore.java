package lab19;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 * Helps the user figure out which pets they might want from a pet store.
 */
public class BetterPetStore {
  public static void main(String[] args) {
    Scanner blueBoxInput = new Scanner(System.in);

    // Ask the user some questions
    boolean mammalPref = yesOrNo("Do you want a mammal?", blueBoxInput);
    boolean shedPref = yesOrNo("Do you want an animal that doesn't shed? ", blueBoxInput);
    boolean colPref = yesOrNo("Do you want a particular color of animal? ", blueBoxInput);
    String color = "";
    if (colPref) {
      System.out.print("What color? ");
      color = blueBoxInput.next();
    }

    // Figure out which dogs they'd like
    List<Animal> pets = allPets();
    List<Animal> preferredPets = new ArrayList<>();
    for (Animal pet : pets) {
      // Only add it to the output list if it matches their preferences
      if ((pet.isMammal() == mammalPref)
          && (!pet.sheds() || !shedPref)
          && (!colPref || pet.getColors().contains(color.trim().toLowerCase()))) {
        preferredPets.add(pet);
      }
    }

    // Pretty-print all the preferred dogs
    System.out.println("Of all our animals, you would prefer these:");
    for (Animal pet : preferredPets) {
      System.out.println("  * " + pet);
    }

  }

  /**
   * Returns a list of all of the dogs our store has to offer.
   */
  public static List<Animal> allPets() {
    List<Animal> pets = new ArrayList<>();
    pets.add(new GermanShepherd("Ryley"));
    pets.add(new GermanShepherd("Jake"));
    pets.add(new Dog(false, "Max", "Bichon Frise", 10, 13, Arrays.asList("white")));
    return pets;
  }

  /**
   * Fetches a yes or no answer from what the user typed into the blue box.
   */
  private static boolean yesOrNo(String prompt, Scanner input) {
    while (true) {
      System.out.print(prompt + " (y/n): ");
      if (!input.hasNext("[yY](es)?|[nN]o?")) {
        System.out.println("Invalid response (expected 'y' or 'n')");
        continue;
      }
      String answer = input.next();
      if (answer.matches("[yY].*")) {
        return true;
      } else {
        return false;
      }
    }
  }
}
