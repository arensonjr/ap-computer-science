package lab19;

import java.util.Arrays;
import lab18.HasDimensions;
import objects.Food;

/**
 * This program is just for experimentation with the Dog class.
 * It has no requirements, so you can mess around with whatever
 * you want here.
 */
public class TryOutAnimals {
    public static void main(String[] args) {
        Animal ryley = new GermanShepherd("Ryley"); // Doesn't work yet
        
        System.out.println(ryley);
        
        Food steak = new Food("steak", "meat", 400, 12);
        if(ryley.eats(steak)) {
            System.out.println("Ryley likes steak!");
        } else {
            System.out.println("Ryley doesn't eat steak.");
        }
        printDimensions(ryley);
        
    }
    
    public static void printDimensions(HasDimensions hd) {
        System.out.println("Dimensions are: " + hd.getHeight() + " in. tall and "
            + hd.getWidth() + " in. long.");
    }
}