package lab19;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 * Helps the user figure out which pets they might want from a pet store.
 */
public class PetStore {
  public static void main(String[] args) {
    Scanner blueBoxInput = new Scanner(System.in);

    // Ask the user some questions
    boolean shedPref = yesOrNo("Do you want a dog that doesn't shed? ", blueBoxInput);
    boolean colPref = yesOrNo("Do you want a particular color of dog? ", blueBoxInput);
    String color = "";
    if (colPref) {
      System.out.print("What color? ");
      color = blueBoxInput.next();
    }

    // Figure out which dogs they'd like
    List<Dog> dogs = allDogs();
    List<Dog> preferredDogs = new ArrayList<>();
    for (Dog dog : dogs) {
      // Only add it to the output list if it matches their preferences
      if ((!dog.sheds() || !shedPref)
          && (!colPref || dog.getColors().contains(color.trim().toLowerCase())) ) {
        preferredDogs.add(dog);
      }
    }

    // Pretty-print all the preferred dogs
    System.out.println("Of all our dogs, you would prefer these:");
    for (Dog dog : preferredDogs) {
      System.out.println("  * " + dog);
    }

  }

  /**
   * Returns a list of all of the dogs our store has to offer.
   */
  public static List<Dog> allDogs() {
    List<Dog> dogs = new ArrayList<>();
    dogs.add(new Dog(false, "Bo", "Portuguese Water Dog", 16, 24, Arrays.asList("black", "white")));
    dogs.add(new Dog(true, "Ryley", "German Shepherd", 25, 36, Arrays.asList("black", "tan", "white")));
    dogs.add(new Dog(true, "Jake", "German Shepherd", 25, 36, Arrays.asList("black", "tan", "white")));
    dogs.add(new Dog(true, "Champ", "German Shepherd", 25, 36, Arrays.asList("black", "tan", "white")));
    dogs.add(new Dog(true, "Westy", "Greyhound", 29, 31, Arrays.asList("tan", "grey")));
    dogs.add(new Dog(true, "Kinto", "Greyhound", 29, 31, Arrays.asList("tan", "grey")));
    dogs.add(new Dog(true, "Mick", "Greyhound", 29, 31, Arrays.asList("tan", "grey")));
    dogs.add(new Dog(false, "Max", "Bichon Frise", 10, 13, Arrays.asList("white")));
    dogs.add(new Dog(false, "Yo-Yo", "Yorkshire Terrier", 9, 11, Arrays.asList("black", "brown")));
    dogs.add(new Dog(false, "Muffy", "Terrier", 11, 14, Arrays.asList("black")));
    dogs.add(new Dog(false, "Clifford", "Big Red Dog", 100, 150, Arrays.asList("red")));
    return dogs;
  }

  /**
   * Fetches a yes or no answer from what the user typed into the blue box.
   */
  private static boolean yesOrNo(String prompt, Scanner input) {
    while (true) {
      System.out.print(prompt + " (y/n): ");
      if (!input.hasNext("[yY](es)?|[nN]o?")) {
        System.out.println("Invalid response (expected 'y' or 'n')");
        continue;
      }
      String answer = input.next();
      if (answer.matches("[yY].*")) {
        return true;
      } else {
        return false;
      }
    }
  }
}
