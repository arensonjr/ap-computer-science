package lab19.solutions;

public class Oval extends Shape {
  private double majorAxis;
  private double minorAxis;
  private boolean isHorizontal;

  public Oval(double maj, double min, boolean h) {
    super(1);
    majorAxis = maj;
    minorAxis = min;
    isHorizontal = h;
  }

  public double getArea() {
    return 3.14159 * majorAxis * minorAxis;
  }

  public double getHeight() {
    if (isHorizontal) {
      return minorAxis * 2;
    } else {
      return majorAxis * 2;
    }
  }

  public double getWidth() {
    if (!isHorizontal) {
      return minorAxis * 2;
    } else {
      return majorAxis * 2;
    }
  }

  public String toString() {
    return "oval";
  }
}
