package lab19.solutions;

import java.util.Arrays;

public class BichonFrise extends Dog {
  public BichonFrise(String name) {
    super(false, name, "Bichon Frise", 10, 13, Arrays.asList("white"));
  }
}

