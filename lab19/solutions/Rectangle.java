package lab19.solutions;

import lab19.Drawable;

import java.awt.Point;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

/**
 * A rectangle (quadrilateral with four 90-degree angles).
 */
public class Rectangle extends Shape implements Drawable {
  // Bounding corners of the rectangle
  private Point bottomLeft;
  private Point topRight;

  public Rectangle(Point bl, Point tr) {
    super(4);
    bottomLeft = bl;
    topRight = tr;
  }

  /**
   * Returns the vertices of this rectangle.
   */
  @Override
  public List<Point2D> getVertices() {
    List<Point2D> vertices = new ArrayList<>();
    vertices.add(bottomLeft);
    vertices.add(new Point((int) bottomLeft.getX(), (int) topRight.getY()));
    vertices.add(topRight);
    vertices.add(new Point((int) topRight.getX(), (int) bottomLeft.getY()));
    return vertices;
  }

  @Override
  public double getArea() {
    return getWidth() * getHeight();
  }

  @Override
  public double getWidth() {
    return topRight.getX() - bottomLeft.getX();
  }

  @Override
  public double getHeight() {
    return topRight.getY() - bottomLeft.getY();
  }

  public String toString() {
    return "rectangle";
  }
}
