package lab19.solutions;

import lab18.solutions.HasSpeed;

import java.util.Arrays;

public class Greyhound extends Dog implements HasSpeed {
  private double speedInMph;

  public Greyhound(String name, double s) {
    super(true, name, "Greyhound", 29, 31, Arrays.asList("tan", "grey"));
    speedInMph = s;
  }

  @Override
  public String toString() {
    return super.toString() + " -- speed: " + speedInMph + "mph";
  }

  public double distanceTraveled(double seconds) {
    double hours = seconds / 3600;
    double miles = speedInMph * hours;
    return miles * 5280;
  }

  public double timeToFinish(double feet) {
    double miles = feet / 5280;
    double hours = miles / speedInMph;
    return hours * 60 * 60;
  }
}

