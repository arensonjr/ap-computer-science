package lab19.solutions;

public class Circle extends Oval {
  public Circle(double radius) {
    super(radius, radius, true);
  }

  public String toString() {
    return "circle";
  }
}
