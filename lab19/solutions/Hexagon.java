package lab19.solutions;

import java.awt.Point;

public class Hexagon extends RegularPolygon {
  public Hexagon(Point center, double height) {
    super(center, 6, height / 2);
  }

  public String toString() {
    return "hexagon";
  }
}

