package lab19.solutions;

import java.util.Arrays;

public class Clownfish extends Fish {
  public Clownfish(String name) {
    super(name, "Clownfish", 1, 4, Arrays.asList("orange", "white"), true);
  }
}
