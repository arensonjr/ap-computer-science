package lab19.solutions;

import java.util.Arrays;

public class Guppy extends Fish {
  public Guppy(String name) {
    super(name, "Guppy", 1, 2, Arrays.asList("orange"), false);
  }
}
