package lab19.solutions;

import java.awt.Point;

public class IsoscelesTriangle extends Triangle {
  public IsoscelesTriangle(Point top, Point bottomLeft) {
    super(
        top,
        bottomLeft,
        new Point(
            (int)(top.getX() + (top.getX() - bottomLeft.getX())),
            (int) bottomLeft.getY()));
  }
}
