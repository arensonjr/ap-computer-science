package lab19.solutions;

import java.awt.Point;

public class RightTriangle extends Triangle {
  public RightTriangle(Point topLeft, Point bottomRight) {
    super(
        topLeft,
        new Point((int) topLeft.getX(), (int) bottomRight.getY()),
        bottomRight);
  }
}
