package lab19.solutions;

import lab18.solutions.HasSpeed;

import objects.Food;

import java.util.List;

public class Fish extends Animal implements HasSpeed {
  private String name;
  private String species;
  private List<String> colors;
  private double feetPerSecond;
  private boolean likesSalt;

  public Fish(String n, String s, double h, double l, List<String> c, boolean ls) {
    super(true, true, h, l);
    name = n;
    species = s;
    colors = c;
    feetPerSecond = 1;
    likesSalt = ls;
  }

  public boolean livesInSaltwater() {
    return likesSalt;
  }

  public String getName() {
    return name;
  }

  public String getSpecies() {
    return species;
  }

  public String toString() {
    return name + " (" + species + ")";
  }

  public boolean eats(Food food) {
    return food.getFoodGroup().equals("meat") || food.getFoodGroup().equals("dairy");
  }

  public List<String> getColors() {
    return colors;
  }

  public double distanceTraveled(double seconds) {
    // Fish travel at 2 feet / second
    return seconds * 2;
  }

  public double timeToFinish(double feet) {
    // Fish travel at 2 feet / second
    return feet / 2;
  }
}

