package lab19.solutions;

import lab19.Drawable;

import java.awt.Point;
import java.awt.geom.Point2D;
import java.util.Arrays;
import java.util.List;

public class Triangle extends Shape implements Drawable {
  private Point top;
  private Point bottomLeft;
  private Point bottomRight;

  public Triangle(Point t, Point bl, Point br) {
    super(3);
    top = t;
    bottomLeft = bl;
    bottomRight = br;
  }

  public double getArea() {
    // Heron's formula
    double side1 = top.distance(bottomLeft);
    double side2 = bottomLeft.distance(bottomRight);
    double side3 = bottomRight.distance(top);
    double s = (side1 + side2 + side3) / 2;

    return Math.sqrt(s * (s - side1) * (s - side2) * (s - side3));
  }

  public double getHeight() {
    return top.getY() - Math.min(bottomLeft.getY(), bottomRight.getY());
  }

  public double getWidth() {
    return Math.abs(bottomLeft.getX() - bottomRight.getX());
  }

  public List<Point2D> getVertices() {
    return Arrays.asList(top, bottomLeft, bottomRight);
  }
}
