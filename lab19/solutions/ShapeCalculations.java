package lab19.solutions;

import lab18.solutions.HasDimensions;

import java.awt.Point;

/**
 * Calculates some stats about shapes.
 */
public class ShapeCalculations {
  public static void main(String[] args) {
    Shape circle = new Circle(150);
    Shape square = new Square(new Point(50, 50), 100);
    Shape triangle = new RightTriangle(new Point(0, 150), new Point(190, 0));
    Shape hexagon = new Hexagon(new Point(300, 300), 60);

    Shape largest = compareAreas(circle, compareAreas(square, compareAreas(triangle, hexagon)));
    System.out.println("The largest shape is " + largest);

  }

  /**
   * Returns the shape with the larger area.
   */
  public static Shape compareAreas(Shape one, Shape two) {
    if (one.getArea() > two.getArea()) {
      return one;
    } else {
      return two;
    }
  }

  /**
   * Returns if the shape fits in the box.
   */
  public static boolean fitsInBox(double boxHeight, double boxWidth, HasDimensions shape) {
    return (shape.getHeight() <= boxHeight) && (shape.getWidth() <= boxWidth);
  }

  /**
   * Returns if the two shapes fit in the box.
   */
  public static boolean fitsInBox(double boxHeight, double boxWidth, HasDimensions one, HasDimensions two) {
    boolean fitsLong =
        (one.getWidth() + two.getWidth() <= boxWidth
         && one.getHeight() < boxHeight
         && two.getHeight() < boxHeight);
    boolean fitsWide =
        (one.getHeight() + two.getHeight() <= boxHeight
         && one.getWidth() < boxWidth
         && two.getWidth() < boxWidth);
    return fitsLong || fitsWide;
  }
}
