package lab19.solutions;

import lab19.Drawable;

import java.awt.Point;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents a regular polygon where one vertex points directly upward.
 */
public abstract class RegularPolygon extends Shape implements Drawable {
  /**
   * The center of the polygon.
   */
  private Point center;

  /**
   * The radius (distance from center to vertex) of the polygon.
   */
  private double radius;

  public RegularPolygon(Point c, int numSides, double r) {
    super(numSides);
    center = c;
    radius = r;
  }

  /**
   * Returns the vertices of this polygon.
   */
  @Override
  public List<Point2D> getVertices() {
    List<Point2D> vertices = new ArrayList<>();

    // Calculate the location of each vertex
    double centerAngle = 2 * Math.PI / getNumSides();
    for (int i = 0; i < getNumSides(); i++) {
      double vx = center.getX() + (radius * Math.sin(i * centerAngle));
      double vy = center.getY() + (radius * Math.cos(i * centerAngle));
      vertices.add(new Point2D.Double(vx, vy));
    }

    return vertices;
  }

  /**
   * Returns the area of this polygon.
   */
  @Override
  public double getArea() {
   // The area of a regular polygon is equal to (1/2) * numSides * apothem * sideLength.
   double centralAngle = 360.0 / getNumSides();
   double apothem = radius * Math.cos(centralAngle / 2);
   double sideLength = 2 * radius * Math.sin(centralAngle / 2);
   return 0.5 * getNumSides() * apothem * sideLength;
  }

  /**
   * Returns the height of this polygon.
   */
  @Override
  public double getHeight() {
    // This is easy -- regular polygons have a radius, which is the radius of
    // the circle that circumscribes them. Their max height is equal to twice
    // this radius, just like a circle.
    return radius * 2;
  }

  /**
   * Returns the width of this polygon.
   */
  @Override
  public double getWidth() {
    // This is easy -- regular polygons have a radius, which is the radius of
    // the circle that circumscribes them. Their max width is equal to twice
    // this radius, just like a circle.
    return radius * 2;
  }
}
