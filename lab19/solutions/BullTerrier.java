package lab19.solutions;

import java.util.Arrays;

public class BullTerrier extends Terrier {
  public BullTerrier(String name) {
    super(name, "Bull", 9, 11, Arrays.asList("black", "brown"));
  }
}


