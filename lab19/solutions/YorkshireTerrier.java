package lab19.solutions;

import java.util.Arrays;

public class YorkshireTerrier extends Terrier {
  public YorkshireTerrier(String name) {
    super(name, "Yorkshire", 9, 11, Arrays.asList("black", "brown"));
  }
}

