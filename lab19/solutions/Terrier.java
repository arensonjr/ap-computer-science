package lab19.solutions;

import java.util.List;

public class Terrier extends Dog {
  public Terrier(String name, String subBreed, double height, double length, List<String> colors) {
    super(false, name, subBreed + " Terrier", height, length, colors);
  }
}
