package lab19.solutions;

import java.awt.Point;

/**
 * A square (equilateral rectangle).
 */
public class Square extends Rectangle {
  public Square(Point bottomLeft, int sideLength) {
    super(
        bottomLeft,
        new Point(
            (int) bottomLeft.getX() + sideLength,
            (int) bottomLeft.getY() + sideLength));
  }

  public String toString() {
    return "square";
  }
}
