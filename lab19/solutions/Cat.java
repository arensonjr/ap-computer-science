package lab19.solutions;

import objects.Food;

import java.util.List;

public class Cat extends Animal {
  private String name;
  private String breed;
  private List<String> colors;

  public Cat(String n, String b, List<String> c) {
    super(true, true, 10, 18);
    name = n;
    breed = b;
    colors = c;
  }

  public String getName() {
    return name;
  }

  public String getBreed() {
    return breed;
  }

  public String toString() {
    return name + " (" + breed + " cat)";
  }

  public boolean eats(Food food) {
    return food.getFoodGroup().equals("meat") || food.getFoodGroup().equals("dairy");
  }

  public List<String> getColors() {
    return colors;
  }
}
