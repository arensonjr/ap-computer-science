package lab19.solutions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Prints the contents of our two fish tanks.
 */
public class FishTank {
  public static void main(String[] args) {
    int fishIndexOne = Integer.parseInt(args[0]);
    int fishIndexTwo = Integer.parseInt(args[1]);

    List<Fish> fishies = new ArrayList<>();
    fishies.add(new Guppy("Steve"));
    fishies.add(new Clownfish("Marlin"));
    fishies.add(new Fish("Bloat", "Puffer Fish", 10, 20, Arrays.asList("blue"), true));
    fishies.add(new Fish("Carrie", "Goldfish", 1, 1, Arrays.asList("gold"), false));
    fishies.add(new Guppy("Goo"));
    fishies.add(new Fish("Pok", "Tetra", 1, 2, Arrays.asList("red"), false));
    fishies.add(new Clownfish("Nemo"));

    List<Fish> fresh = new ArrayList<>();
    List<Fish> salty = new ArrayList<>();
    for (Fish fish : fishies) {
      if (fish.livesInSaltwater()) {
        salty.add(fish);
      } else {
        fresh.add(fish);
      }
    }

    System.out.println("Freshwater Tank:");
    System.out.println(freshwaterTank(fishies));

    System.out.println("Saltwater Tank:");
    System.out.println(saltwaterTank(fishies));

    Fish f1 = fishies.get(fishIndexOne);
    Fish f2 = fishies.get(fishIndexTwo);
    System.out.println("Can " + f1 + " and " + f2 + " live together?");
    System.out.println(canLiveTogether(f1, f2));
  }

  public static boolean canLiveTogether(Fish one, Fish two) {
    return one.livesInSaltwater() == two.livesInSaltwater();
  }

  public static List<Fish> freshwaterTank(List<Fish> allFish) {
    List<Fish> fresh = new ArrayList<>();
    for (Fish fish : allFish) {
      if (!fish.livesInSaltwater()) {
        fresh.add(fish);
      }
    }
    return fresh;
  }

  public static List<Fish> saltwaterTank(List<Fish> allFish) {
    List<Fish> salty = new ArrayList<>();
    for (Fish fish : allFish) {
      if (fish.livesInSaltwater()) {
        salty.add(fish);
      }
    }
    return salty;
  }
}
