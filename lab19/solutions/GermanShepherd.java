package lab19.solutions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GermanShepherd extends Dog {
  private List<String> recognizedScents;

  public GermanShepherd(String name) {
    super(true, name, "German Shepherd", 25, 36, Arrays.asList("black", "tan", "white"));
    recognizedScents = new ArrayList<>();
  }

  public void addScent(String scent) {
    recognizedScents.add(scent);
  }

  public List<String> getRecognizedScents() {
    return recognizedScents;
  }

  @Override
  public String toString() {
    return super.toString() + " -- recognizes " + getRecognizedScents();
  }
}
