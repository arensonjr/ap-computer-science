package lab19.solutions;

import objects.Food;

import java.util.List;

/**
 * Represents a doggie.
 */
public abstract class Dog extends Animal {
  private String name;
  private String breed;
  private List<String> colors;

  public Dog(boolean dis, String n, String b, double h, double l, List<String> c) {
    super(true, dis, h, l);
    name = n;
    breed = b;
    colors = c;
  }

  public String getName() {
    return name;
  }

  public String getBreed() {
    return breed;
  }

  public List<String> getColors() {
    return colors;
  }

  public String toString() {
    return name + " (" + colors + " " + breed + ")";
  }

  public boolean eats(Food food) {
    return food.getFoodGroup().equals("meat");
  }
}
