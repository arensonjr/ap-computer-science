package lab19.solutions;

import java.awt.Point;

public class Diamond extends RegularPolygon {
  public Diamond(Point center, double sideLength) {
    super(center, 4, sideLength / Math.sqrt(2));
  }

  public String toString() {
    return "diamond";
  }
}
