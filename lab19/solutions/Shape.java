package lab19.solutions;

import lab18.HasDimensions;

/**
 * Abstract class representing any shape.
 */
public abstract class Shape implements HasDimensions {
  /**
   * Number of sides the shape has.
   */
  private int numSides;

  public Shape(int n) {
    numSides = n;
  }

  /**
   * Returns the number of sides in this shape.
   */
  public int getNumSides() {
    return numSides;
  }

  /**
   * Returns the area of this shape.
   */
  public abstract double getArea();

  /**
   * Returns the max height of this shape (requried for HasDimensions).
   */
  public abstract double getHeight();

  /**
   * Returns the max width of this shape (required for HasDimensions).
   */
  public abstract double getWidth();
}
