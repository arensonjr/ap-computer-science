package lab19;

import java.util.List;

import lab18.HasDimensions;

/**
 * Represents a doggie.
 */
public class Dog implements HasDimensions {
  private boolean doesItShed;
  private String name;
  private String breed;
  private double heightInches;
  private double lengthInches;
  private List<String> colors;

  public Dog(boolean dis, String n, String b, double h, double l, List<String> c) {
    doesItShed = dis;
    name = n;
    breed = b;
    heightInches = h;
    lengthInches = l;
    colors = c;
  }

  public boolean sheds() {
    return doesItShed;
  }

  public String getName() {
    return name;
  }

  public String getBreed() {
    return breed;
  }

  public double getHeight() {
    return heightInches;
  }

  public double getWidth() {
    return lengthInches;
  }

  public List<String> getColors() {
    return colors;
  }

  public String toString() {
    return name + " (" + colors + " " + breed + ")";
  }
}
