package lab19;

import lab18.HasDimensions;
import objects.Food;
import java.util.List;

public abstract class Animal implements HasDimensions {

  private boolean isMammal;
  private boolean doesItShed;
  private double heightInches;
  private double lengthInches;
        
  public Animal(boolean m, boolean dis, double h, double l) {
    isMammal = m;
    doesItShed = dis;
    heightInches = h;
    lengthInches = l;
  }
  
  public boolean isMammal() {
    return isMammal;
  }
    
  public boolean sheds() {
    return doesItShed;
  }

  public double getHeight() {
    return heightInches;
  }

  public double getWidth() {
    return lengthInches;
  }

  public abstract List<String> getColors();
  
  public abstract boolean eats(Food food);

}