package lab19;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import lab19.Drawable;

import java.awt.geom.Point2D;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * Provided functions to render shapes to a webpage.
 */
public class Provided {

  private static final Path HTML_TEMPLATE = Paths.get("lab19", "static", "draw.html");

  private static boolean alreadyDrawn = false;

  /**
   * Draws the input shape to a webpage.
   *
   * This can only be called ONCE from your main program! Multiple calls will fail.
   */
  public static void drawToScreen(Drawable drawable) {
    if (alreadyDrawn) {
      System.err.println("ERROR: You can only draw something to the screen once per program.");
      System.err.println("       You'll have to run your program again to draw '" + drawable + "'.");
    }
    try {
      HttpServer server = HttpServer.create();
      server.createContext("/", new DrawHandler(drawable));

      int port = Integer.parseInt(System.getenv("C9_PORT"));
      String ip = System.getenv("C9_IP");
      server.bind(new InetSocketAddress(ip, port), port);

      server.start();
      String hostname = System.getenv("C9_HOSTNAME");
      System.out.println("===");
      System.out.println("===");
      System.out.println("===");
      System.out.format("View your Drawable at http://%s\n", hostname);
      System.out.println("===");
      System.out.println("===");
      System.out.println("===");
    } catch (IOException e) {
      System.err.println("Something went wrong; is there a bug in your Drawable object?");
      e.printStackTrace();
      System.exit(1);
    }
  }


  /**
   * Renders a shape to the screen.
   */
  private static class DrawHandler implements HttpHandler {
    private final Drawable drawable;

    public DrawHandler(Drawable d) {
      drawable = d;
    }

    @Override public void handle(HttpExchange exchange) throws IOException {
      URI url = exchange.getRequestURI();
      try {
        // JSON(ish)ify the vertices
        StringBuilder points = new StringBuilder("[");
        List<Point2D> vertices = drawable.getVertices();
        for (int i = 0; i < vertices.size(); i++) {
          if (i > 0) {
            points.append(',');
          }
          Point2D point = vertices.get(i);
          points.append("{x:").append(point.getX())
              .append(",y:").append(point.getY())
              .append("}");
        }
        points.append(']');

        // Stick them in the HTML
        String contents =
            new String(Files.readAllBytes(HTML_TEMPLATE))
                .replaceAll("\\$\\{POINTS\\}", points.toString());

        // Send the HTML to the browser
        byte[] pageBytes = contents.toString().getBytes();
        exchange.sendResponseHeaders(200, pageBytes.length);
        exchange.getResponseBody().write(pageBytes);
        exchange.close();
        System.err.format("[200] [%s] OK\n", url);
      } catch (Throwable t) {
        t.printStackTrace();
        exchange.sendResponseHeaders(500, 0);
        exchange.close();
        System.err.format("[500] [%s] Error: %s\n", url, t);
      }
    }
  }
}
