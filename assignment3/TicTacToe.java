package assignment3;

import java.util.Scanner;

/**
 * A classic game of Tic-Tac-Toe. The game runner is provided, and you'll write
 * the functions it needs to work.
 */
public class TicTacToe {
    // Constants
    static final int PLAYER_ONE = 1;
    static final int PLAYER_TWO = -1;
    static final int UNCLAIMED = 0;

    /**
     * The main method, which runs the program.
     */
    public static void main(String[] args) {
        // +-------------------------------+
        // |     DO NOT EDIT THIS CODE!    |
        // +-------------------------------+
        //
        // You should be writing all of your code in the functions below. Go
        // back and read the assignment document; it describes all of the
        // places that you need to write code, and this time, main() isn't one
        // of them.
        //
        // We've already written the main() function for you, which will step
        // through a game of Tic-Tac-Toe turn-by-turn. You will write all of
        // the helper functions that make the game actually work: Changing the
        // board, printing out the board, checking if a player has won, etc.
        Scanner input = new Scanner(System.in);
        int[][] grid = {
          { UNCLAIMED, UNCLAIMED, UNCLAIMED },
          { UNCLAIMED, UNCLAIMED, UNCLAIMED },
          { UNCLAIMED, UNCLAIMED, UNCLAIMED }
        };
        int player = PLAYER_ONE;
        int row;
        int column;
        do {
            System.out.println("\nPlayer " + playerNumberToString(player) + "'s turn");

            // Ask the current player to choose a position on the board. If
            // they choose a position that's already filled in, keep asking
            // until we get a *real* position.
            row = getCoordinate(input, "row");
            column = getCoordinate(input, "column");
            while (!isPositionAvailable(grid, row, column)) {
                System.out.println("Position not available. Try again.\n");
                row = getCoordinate(input, "row");
                column = getCoordinate(input, "column");
            }

            // Take that position, make a move, and print out the updated game
            // board.
            setPosition(grid, player, row, column);
            System.out.println();
            printGameBoard(grid);

            // Switch players by alternating negative/positive signs.
            player *= -1;

            // Keep doing this until the game is over, and we print who wins
            // (or who ties).
        } while (!gameOver(grid, player, row, column));
    }

    /**
     * Prompts the user to enter a row or column, and parses what is entered.
     *
     * @param input source of the user's input
     * @param indexName name of the index to ask the user for ("row" or "column")
     * @return an integer index (either 0, 1, or 2)
     */
    public static int getCoordinate(Scanner input, String indexName) {
        System.out.print("Choose a " + indexName + " (0, 1, or 2): ");
        int index;
        while (!input.hasNextInt())  {
            System.out.println("'" + input.nextLine() + "' is not a number. Try again.");
            System.out.print("Choose a " + indexName + " (0, 1, or 2): ");
        }

        // At this point, we know the next token is an integer. Make sure it's
        // a legal coordinate.
        index = input.nextInt();
        if (index < 0 || index > 2) {
            System.out.println("'" + index + "' is not a valid coordinate. Try again.");
            return getCoordinate(input, indexName);
        }

        return index;
    }

    /**
     * Converts a player's numeric representation to a string representation.
     * X is 1, O is -1.
     *
     * @param number the numeric representation of the player
     * @return the string representation of the player
     */
    public static String playerNumberToString(int number) {
        // Replace the following line with your code.
        return "?";
    }

    /**
     * Prints out the game board in a human-readable format. This function
     * should print the string representation of X and O, not the numeric
     * representation.
     *
     * Your game board can resemble this:
     *
     *    X | O | X
     *      | X |
     *      |   | O
     *
     * or this:
     *
     *  +-----------+
     *  | X | O | X |
     *  +---|---|---+
     *  |   | X |   |
     *  +---|---|---+
     *  |   |   | O |
     *  +---|---|---+
     *
     * or any other format, as long as it accurately represents the state of
     * the board, with grid[0][0] in the top left, and grid[2][2] in the bottom
     * right.
     *
     * @param grid the game board, represented as a matrix of numbers
     */
    public static void printGameBoard(int[][] grid) {
        // Write your code here. You may find it useful to write a helper
        // method; e.g.
        //  - printRow(int[][] grid, int row)
    }

    /**
     * Determines whether or not a position in the tic tac toe board is still 
     * open.
     *
     * @param grid the game board, represented as a matrix of numbers
     * @param row the row of the position to check
     * @param column the column of the position to check
     * @return true if position has not yet been played in, false otherwise
     */
    public static boolean isPositionAvailable(int[][] grid,  int row, int column) {
        // Replace the following line with your code.
        //
        // To get the number at (row, column), you can write grid[row][column]
        // -- it's just like a normal array, except since there are *two* dimensions
        // (caused by two pairs of square brackets: "int[][]"), you need *two* indices
        // to tell Java which number to fetch.
        return false;
    }

    /**
     * Assigns a position in the grid to a particular player. Returns nothing,
     * but modifies the grid object.
     * 
     * @param grid the game board, represented as a matrix of numbers.
     * @param player the player who is trying to select the position
     * @param row the row of the position to select
     * @param column the column of the position to select
     */
    public static void setPosition(int[][] grid, int player, int row, int column) {
        // Write your code here.
    }

    /**
     * Determines whether or not the tic tac toe game is over. If it has been 
     * won, prints out a message saying who won the game. If the grid is full,
     * prints out a message saying it was a cat's game.
     * 
     * @param grid the game board, represented as a matrix of numbers.
     * @param player the player who most recently made a move
     * @param row the row of the position most recently selected
     * @param column the column of the position most recently selected 
     * @return true if the game is over (winner or cat's game), false if the
     *      game is still going
     */
    public static boolean gameOver(int[][] grid, int player, int row, int column) {
        // Replace the following line with your code. You may find it useful
        // to create some helper methods; e.g.
        //   - winRow(int[][] board, int player, int row)
        //   - winColumn(int[][] board, int player, int column)
        //   - winDiagonals(int[][] board, int player)
        //   - isBoardFull(int[][] board)
        return false;
    }
}
