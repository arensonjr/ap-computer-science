package assignment3.solutions;

import java.util.Scanner;

/**
 * A classic game of Tic-Tac-Toe. The game runner is provided, and you'll write
 * the functions it needs to work.
 */
public class TicTacToe {
    // Constants 
    static final int PLAYER_ONE = 1;
    static final int PLAYER_TWO = -1;
    static final int UNCLAIMED = 0;

    /**
     * The main method, which runs the program.
     */
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int[][] grid = new int[3][3];
        int player = PLAYER_ONE;
        int row;
        int column;
        boolean isGameOver;
        do {
            System.out.println("\nPlayer " + playerNumberToString(player) + "'s turn");

            // Ask the current player to choose a position on the board. If
            // they choose a position that's already filled in, keep asking
            // until we get a *real* position.
            row = getCoordinate(input, "row");
            column = getCoordinate(input, "column");
            while (!isPositionAvailable(grid, row, column)) {
                System.out.println("Position not available. Try again.\n");
                row = getCoordinate(input, "row");
                column = getCoordinate(input, "column");
            }

            // Take that position, make a move, and print out the updated game
            // board.
            setPosition(grid, player, row, column);
            System.out.println();
            printGameBoard(grid);

            // Check whether the game is over
            isGameOver = gameOver(grid, player, row, column);

            // Switch players by alternating negative/positive signs.
            player *= -1;

            // Keep doing this until the game is over, and we print who wins
            // (or who ties).
        } while (!isGameOver);
    }

    /**
     * Prompts the user to enter a row or column, and parses what is entered.
     *
     * @param input source of the user's input
     * @param indexName name of the index to ask the user for ("row" or "column")
     * @return an integer index (either 0, 1, or 2)
     */
    public static int getCoordinate(Scanner input, String indexName) {
        System.out.print("Choose a " + indexName + " (0, 1, or 2): ");
        int index;
        while (!input.hasNextInt())  {
            System.out.println("'" + input.nextLine() + "' is not a number. Try again.");
            System.out.print("Choose a " + indexName + " (0, 1, or 2): ");
        }

        // At this point, we know the next token is an integer. Make sure it's
        // a legal coordinate.
        index = input.nextInt();
        if (index < 0 || index > 2) {
            System.out.println("'" + index + "' is not a valid coordinate. Try again.");
            return getCoordinate(input, indexName);
        }

        return index;
    }
    
    /**
     * Converts a player's numeric representation to a string representation.
     * X is 1, O is -1.
     *
     * @param number the numeric representation of the player
     * @return the string representation of the player
     */
    public static String playerNumberToString(int number) {
        if (number == PLAYER_ONE) {
            return "X";
        } else if (number == PLAYER_TWO) {
            return "O";
        } else if (number == UNCLAIMED) {
            return " ";
        } else {
            return "ERROR: NOT A PLAYER";
        }
    }
    
    /**
     * Prints out the game board in a human-readable format. This function
     * should print the string representation of X and O, not the numeric
     * representation.
     *
     * Your game board can resemble this:
     *
     *    X | O | X
     *      | X |
     *      |   | O
     *
     * or this:
     *
     *  +-----------+
     *  | X | O | X |
     *  +---|---|---+
     *  |   | X |   |
     *  +---|---|---+
     *  |   |   | O |
     *  +---|---|---+
     *
     * or any other format, as long as it accurately represents the state of
     * the board, with grid[0][0] in the top left, and grid[2][2] in the bottom
     * right.
     *
     * @param grid the game board, represented as a matrix of numbers
     */
    public static void printGameBoard(int[][] grid) {
        printRow(grid, 0);
        printRow(grid, 1);
        printRow(grid, 2);
    }

    /**
     * Prints out a row of the game board in a human-readable format.
     *
     * @param board the game board, represented as a 3-by-3 matrix of numbers
     * @param row the index of the row to print (0, 1, or 2)
     */
    public static void printRow(int[][] board, int row) {
        System.out.println(
            playerNumberToString(board[row][0])
            + " | " + playerNumberToString(board[row][1])
            + " | " + playerNumberToString(board[row][2]));
    }
    
    /**
     * Determines whether or not a position in the tic tac toe board is still 
     * open.
     *
     * @param grid the game board, represented as a matrix of numbers
     * @param row the row of the position to check
     * @param column the column of the position to check
     * @return true if position has not yet been played in, false otherwise
     */
    public static boolean isPositionAvailable(int[][] grid,  int row, int column) {
        return grid[row][column] == UNCLAIMED;
    }
    
    /**
     * Assigns a position in the grid to a particular player. Returns nothing,
     * but modifies the grid object.
     * 
     * @param grid the game board, represented as a matrix of numbers.
     * @param player the player who is trying to select the position
     * @param row the row of the position to select
     * @param column the column of the position to select
     */
    public static void setPosition(int[][] grid, int player, int row, int column) {
        grid[row][column] = player;
    }
    
    /**
     * Determines whether or not the tic tac toe game is over. If it has been 
     * won, prints out a message saying who won the game. If the grid is full,
     * prints out a message saying it was a cat's game.
     * 
     * @param grid the game board, represented as a matrix of numbers.
     * @param player the player who most recently made a move
     * @param row the row of the position most recently selected
     * @param column the column of the position most recently selected 
     * @return true if the game is over (winner or cat's game), false if the
     *      game is still going
     */
    public static boolean gameOver(int[][] grid, int player, int row, int column) {
        if (winColumn(grid, player, column)
                || winRow(grid, player, row)
                || winDiagonal(grid, player)) {
            System.out.println("Player " + playerNumberToString(player) + " wins!");
            return true;
        } else if (isBoardFull(grid)) {
            System.out.println("Cat's game");
            return true;
        } else {
            return false;
        }
    }

    /**
     * Determines whether or not the player has won this row.
     *
     * @param board the game board, represented as a matrix of numbers
     * @param player the player to check
     * @param row the index of the row to check
     * @return true if the player wins the row (has claimed all 3 spots),
     *         false otherwise
     */
    public static boolean winRow(int[][] board, int player, int row) {
        return (board[row][0] == player
                && board[row][1] == player
                && board[row][2] == player);
    }

    /**
     * Determines whether or not the player has won this column.
     *
     * @param board the game board, represented as a matrix of numbers
     * @param player the player to check
     * @param column the index of the row to check
     * @return true if the player wins the column (has claimed all 3 spots),
     *         false otherwise
     */
    public static boolean winColumn(int[][] board, int player, int column) {
        return board[0][column] == player
            && board[1][column] == player
            && board[2][column] == player;
    }

    /**
     * Determines whether or not the player has won either diagonal.
     *
     * @param board the game board, represented as a matrix of numbers
     * @param player the player to check
     * @return true if the player has won on either diagonal, false otherwise
     */
    public static boolean winDiagonal(int[][] board, int player) {
        boolean topLeftBottomRight =
            board[0][0] == player
            && board[1][1] == player
            && board[2][2] == player;
        boolean topRightBottomLeft =
            board[2][0] == player
            && board[1][1] == player
            && board[0][2] == player;

        return topLeftBottomRight || topRightBottomLeft;
    }

    /**
     * Determines whether the board is full.
     *
     * @param board the game board, represented as a matrix of numbers
     * @return true if no space on the board is unclaimed, false if there is at
     *         least one unclaimed space remaining
     */
    public static boolean isBoardFull(int[][] board) {
        return board[0][0] != UNCLAIMED
            && board[0][1] != UNCLAIMED
            && board[0][2] != UNCLAIMED 
            && board[1][0] != UNCLAIMED
            && board[1][1] != UNCLAIMED
            && board[1][2] != UNCLAIMED 
            && board[2][0] != UNCLAIMED
            && board[2][1] != UNCLAIMED
            && board[2][2] != UNCLAIMED;
    }
}
