package pix;

import java.io.IOException;

/**
 * Generates some pictures from scratch.
 */
public class Assignment11 {
  public static void main(String[] args) throws IOException {
    // 1. Create a red square.
    Pixel[][] redSquarePixels = new Pixel[3][3];
    // (write your code here)
    Picture redSquare = new Picture("Red Square", redSquarePixels);
    PictureIO.write(redSquare, "redSquare.jpg");

    // 2. Create a bigger green square.
    Pixel[][] bigGreenSquarePixels = new Pixel[100][100];
    // (write your code here)
    Picture bigGreenSquare = new Picture("Big Green Square", bigGreenSquarePixels);
    PictureIO.write(bigGreenSquare, "bigGreenSquare.jpg");

    // 3. BONUS: Create a blue plus.
    Pixel[][] bluePlusPixels = new Pixel[90][90];
    // (write your code here)
    Picture bluePlus = new Picture("Blue Plus", bluePlusPixels);
    PictureIO.write(bluePlus, "bluePlus.jpg");
  }
}
