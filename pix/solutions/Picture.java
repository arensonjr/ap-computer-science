package pix.solutions;

/**
 * Class representing an image (a matrix of pixels).
 */
public class Picture {
  /**
   * The name of the picture.
   *
   * This can be used to attach a short description to the picture -- most commonly, describing what
   * it is a picture of (e.g. "beach" or "arch").
   */
  private String name;

  /**
   * The pixels in the image.
   *
   * Each pixel represents one point of color while drawing the picture.
   */
  private Pixel[][] pixels;

  /**
   * Create a new picture by initializing our internal data structures.
   */
  public Picture(String nameInput, Pixel[][] pixelsInput) {
    name = nameInput;
    pixels = pixelsInput;
  }

  /**
   * Gets the width of the image (number of columns of pixels).
   */
  public int getWidth() {
    return pixels[0].length;
  }

  /**
   * Gets the height of the image (number of rows of pixels).
   */
  public int getHeight() {
    return pixels.length;
  }

  /**
   * Gets the pixel in the specified row and column of the picture.
   */
  public Pixel getPixel(int row, int col) {
    if (row < 0 || row >= getHeight()) {
      throw new RuntimeException(
          "This picture is " + getHeight() + "x" + getWidth() + "; row " + row + " doesn't exist");
    }
    if (col < 0 || col >= getWidth()) {
      throw new RuntimeException(
          "This picture is " + getHeight() + "x" + getWidth() + "; col " + col + " doesn't exist");
    }
    return pixels[row][col];
  }

  /**
   * Returns the name of this picture.
   */
  public String getName() {
    return name;
  }

  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   *                                                                         *
   *                                                                         *
   *                Provided methods only above this line!                   *
   *                                                                         *
   *                                                                         *
   *                Cool, useful methods below this line!                    *
   *                                                                         *
   *                                                                         *
   * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

  /**
   * Swaps the blue and green values of the picture.
   */
  public void swapBlueAndGreen() {
    for (int row = 0; row < getHeight(); row++) {
      for (int column = 0; column < getWidth() / 2; column++) {
        Pixel pix = getPixel(row, column);
        int blue = pix.getBlue();
        int green = pix.getGreen();

        pix.setGreen(blue);
        pix.setBlue(green);
      }
    }
  }
}
