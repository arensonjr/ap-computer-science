package pix.solutions;

/**
 * A single pixel in an image.
 */
public class Pixel {
  /**
   * The RED value of the pixel. Must be in the range 0-255.
   */
  private int red;

  /**
   * The BLUE value of the pixel. Must be in the range 0-255.
   */
  private int blue;

  /**
   * The GREEN value of the pixel. Must be in the range 0-255.
   */
  private int green;

  /**
   * Create a new pixel with the specified colors.
   */
  public Pixel(int redInput, int greenInput, int blueInput) {
    setRed(redInput);
    setGreen(greenInput);
    setBlue(blueInput);
  }

  /**
   * Returns the red value of this pixel.
   */
  public int getRed() {
    return red;
  }

  /**
   * Returns the blue value of this pixel.
   */
  public int getBlue() {
    return blue;
  }

  /**
   * Returns the green value of this pixel.
   */
  public int getGreen() {
    return green;
  }

  /**
   * Changes the red value of this pixel to newValue.
   */
  public void setRed(int newValue) {
    if (newValue < 0 || newValue > 255) {
      throw new RuntimeException(
          "The 'red' value for a pixel must be in the range 0-255. Yours was " + newValue);
    }
    red = newValue;
  }

  /**
   * Changes the blue value of this pixel to newValue.
   */
  public void setBlue(int newValue) {
    if (newValue < 0 || newValue > 255) {
      throw new RuntimeException(
          "The 'blue' value for a pixel must be in the range 0-255. Yours was " + newValue);
    }
    blue = newValue;
  }

  /**
   * Changes the green value of this pixel to newValue.
   */
  public void setGreen(int newValue) {
    if (newValue < 0 || newValue > 255) {
      throw new RuntimeException(
          "The 'green' value for a pixel must be in the range 0-255. Yours was " + newValue);
    }
    green = newValue;
  }
}

