package pix.solutions;

import java.io.IOException;

/**
 * Generates some pictures from scratch.
 */
public class Assignment11 {
  public static void main(String[] args) throws IOException {
    // 1. Create a red square.
    Pixel[][] redSquarePixels = new Pixel[3][3];
    redSquarePixels[0][0] = new Pixel(255, 0, 0);
    redSquarePixels[0][1] = new Pixel(255, 0, 0);
    redSquarePixels[0][2] = new Pixel(255, 0, 0);
    redSquarePixels[1][0] = new Pixel(255, 0, 0);
    redSquarePixels[1][1] = new Pixel(255, 0, 0);
    redSquarePixels[1][2] = new Pixel(255, 0, 0);
    redSquarePixels[2][0] = new Pixel(255, 0, 0);
    redSquarePixels[2][1] = new Pixel(255, 0, 0);
    redSquarePixels[2][2] = new Pixel(255, 0, 0);
    Picture redSquare = new Picture("Red Square", redSquarePixels);
    PictureIO.write(redSquare, "redSquare.jpg");

    // 2. Create a bigger green square.
    Pixel[][] bigGreenSquarePixels = new Pixel[100][100];
    for (int row = 0; row < 100; row++) {
      for (int col = 0; col < 100; col++) {
        bigGreenSquarePixels[row][col] = new Pixel(0, 255, 0);
      }
    }
    Picture bigGreenSquare = new Picture("Big Green Square", bigGreenSquarePixels);
    PictureIO.write(bigGreenSquare, "bigGreenSquare.jpg");

    // 3. BONUS: Create a blue plus.
    Pixel[][] bluePlusPixels = new Pixel[90][90];
    /*
     * We can think of a blue plus as nine little squares:
     *
     * +---+---+---+
     * |   | B |   |
     * +---+---+---+
     * | B | B | B |
     * +---+---+---+
     * |   | B |   |
     * +---+---+---+
     */
    // Fill the whole thing blue
    for (int row = 0; row < 90; row++) {
      for (int col = 0; col < 90; col++) {
        bluePlusPixels[row][col] = new Pixel(0, 0, 255);
      }
    }
    // Make the corners white
    for (int distFromSide = 0; distFromSide < 30; distFromSide++) {
      for (int distFromTop = 0; distFromTop < 30; distFromTop++) {
        bluePlusPixels[distFromTop][distFromSide] = new Pixel(255, 255, 255);
        bluePlusPixels[distFromTop][89 - distFromSide] = new Pixel(255, 255, 255);
        bluePlusPixels[89 - distFromTop][distFromSide] = new Pixel(255, 255, 255);
        bluePlusPixels[89 - distFromTop][89 - distFromSide] = new Pixel(255, 255, 255);
      }
    }
    Picture bluePlus = new Picture("Blue Plus", bluePlusPixels);
    PictureIO.write(bluePlus, "bluePlus.jpg");
  }
}
