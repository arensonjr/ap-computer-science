package pix.solutions;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

import javax.imageio.ImageIO;

/**
 * Utility functions for reading and writing picture files.
 */
public class PictureIO {

  /**
   * The folder where image files are located.
   */
  private static final Path IMAGE_DIR = Paths.get("pix", "images");

  /**
   * Reads an image file and turns it into a Picture.
   *
   * The picture file is assumed to be in the pix/images/ folder -- i.e. if the input to this
   * function is "flag.jpg", we will try to open "pix/images/flag.jpg".
   */
  public static Picture read(String filename) throws IOException {
    // Have Java interpret the image file for us
    Path file = IMAGE_DIR.resolve(filename);
    System.err.format("Reading picture from [%s]\n", file);
    BufferedImage image = ImageIO.read(Files.newInputStream(file));

    // Convert each of the pixels from the image into the type of objects we're going to use
    Pixel[][] pixels = new Pixel[image.getHeight()][image.getWidth()];
    for (int row = 0; row < image.getHeight(); row++) {
      for (int col = 0; col < image.getWidth(); col++) {
        // Java has a funky way of storing pixels; we'll need to do some extra work to pull them out
        // into regular old red-blue-green colors.
        int rgb = image.getRGB(col, row);
        int red   = (rgb >> 16) & 0xff;
        int green = (rgb >>  8) & 0xff;
        int blue  = (rgb      ) & 0xff;

        pixels[row][col] = new Pixel(red, green, blue);
      }
    }

    // Bundle up our decoded Pixels into a Picture object.
    return new Picture(filename, pixels);
  }

  /**
   * Writes a Picture into an image file.
   *
   * The file will be created in the pix/images/ folder -- i.e. if the requested filename is
   * "flag.jpg", we will try to create "pix/images/flag.jpg".
   *
   * If you enter a filename that already exists, we will automatically append a number to the end
   * of the filename to disambiguate (e.g. "pix/images/flag-1.jpg".
   */
  public static void write(Picture picture, String filename) throws IOException {
    // Get a unique filename
    int fileDuplicateIndex = 0;
    String extension = filename.substring(filename.lastIndexOf('.') + 1);
    while (Files.exists(IMAGE_DIR.resolve(filename))) {
      fileDuplicateIndex++;
      filename = filename.replaceFirst(
          "([-][0-9]+)?\\." + extension + "$",
          "-" + fileDuplicateIndex + "." + extension);
    }
    System.err.format("Writing picture to [%s]\n", filename);

    write(
        picture,
        Files.newOutputStream(IMAGE_DIR.resolve(filename), StandardOpenOption.CREATE));
  }

  /**
   * Writes a Picture to any output stream.
   */
  public static void write(Picture picture, OutputStream output) throws IOException {
    BufferedImage image =
        new BufferedImage(
            picture.getWidth(),
            picture.getHeight(),
            BufferedImage.TYPE_INT_RGB);
    for (int row = 0; row < picture.getHeight(); row++) {
      for (int col = 0; col < picture.getWidth(); col++) {
        Pixel pixel = picture.getPixel(row, col);
        // Convert this pixel's color to a format that Java understands
        Color rgb = new Color(pixel.getRed(), pixel.getGreen(), pixel.getBlue());
        image.setRGB(col, row, rgb.getRGB());
      }
    }
    ImageIO.write(image, "jpg", output);
  }
}
