package pix;

import java.io.IOException;
import java.lang.reflect.Method;
import java.net.InetSocketAddress;
import java.net.URI;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

/**
 * Executable program to render images to a webpage.
 */
public class PictureViewer {
  /**
   * Folder containing all of the JPEG images.
   */
  private static final Path IMAGE_DIR = Paths.get("pix", "images");

  /**
   * File containing the HTML template for our picture viewer page.
   */
  private static final Path HTML_PAGE_TEMPLATE =
    Paths.get("pix", "static", "picture_viewer.html");

  /**
   * File containing the HTML template for each linked picture in the left sidebar.
   */
  private static final Path HTML_LINKPICTURE_TEMPLATE =
    Paths.get("pix", "static", "picture_link_item.html");

  /**
   * File containing the HTML template for each method on the Picture class.
   */
  private static final Path HTML_PICTUREMETHOD_TEMPLATE =
    Paths.get("pix", "static", "picture_action_button.html");

  /**
   * Blacklisted Picture methods, that don't count as transformations.
   */
  private static final HashSet<String> BLACKLISTED_METHODS = new HashSet<>();
  static {
    BLACKLISTED_METHODS.add("main");
    BLACKLISTED_METHODS.add("void");
    BLACKLISTED_METHODS.add("wait");
    BLACKLISTED_METHODS.add("notify");
    BLACKLISTED_METHODS.add("notifyAll");
  }

  /**
   * Start an HTTP server that will serve a picture explorer app.
   */
  public static void main(String[] args) throws IOException {
    HttpServer server = HttpServer.create();
    server.createContext("/", new PictureViewerHandler());
    server.createContext("/imagedata", new PictureDataHandler());

    int port = Integer.parseInt(System.getenv("C9_PORT"));
    String ip = System.getenv("C9_IP");
    server.bind(new InetSocketAddress(ip, port), port);

    server.start();
    String hostname = System.getenv("C9_HOSTNAME");
    System.out.format("Running at http://%s\n", hostname);
  }

  /**
   * Handle requests for the picture explorer homepage.
   */
  private static class PictureViewerHandler implements HttpHandler {
    @Override public void handle(HttpExchange exchange) throws IOException {
      URI url = exchange.getRequestURI();
      try {
        // Which picture do we show large?
        String bigPic = url.getQuery();
        if (bigPic == null || bigPic.isEmpty()) {
          bigPic = "arch.jpg";
        }

        // List all the JPEG images in the workspace, in alphabetical order
        Set<String> pictures = new TreeSet<>();
        try (DirectoryStream<Path> files = Files.newDirectoryStream(IMAGE_DIR, "*.jpg")) {
          for (Path file : files) {
            pictures.add(file.getFileName().toString());
          }
        }

        String pictureTemplate = new String(Files.readAllBytes(HTML_LINKPICTURE_TEMPLATE));
        StringBuilder pictureListHtml = new StringBuilder();
        for (String picture : pictures) {
          String html = pictureTemplate
              .replaceAll("\\$\\{filename\\}", picture)
              .replaceAll("\\$\\{picturename\\}", picture);
          pictureListHtml.append(html);
        }

        // List all of the methods that can be performed on the picture
        String methodButtonTemplate = new String(Files.readAllBytes(HTML_PICTUREMETHOD_TEMPLATE));
        StringBuilder methodButtonHtml = new StringBuilder();
        for (Method method : Picture.class.getMethods()) {
          if (!BLACKLISTED_METHODS.contains(method.getName())
              && method.getParameterTypes().length == 0
              && method.getReturnType() == Void.TYPE) {
            String html = methodButtonTemplate
                .replaceAll("\\$\\{newmethod\\}", method.getName());
            methodButtonHtml.append(html);
          }
        }

        // Fill in the webpage template
        byte[] page =
            new String(Files.readAllBytes(HTML_PAGE_TEMPLATE))
                .replaceAll("\\$\\{filename\\}", bigPic)
                .replaceAll("\\$\\{picturename\\}", bigPic)
                .replaceAll("\\$\\{picturelist\\}", pictureListHtml.toString())
                .replaceAll("\\$\\{methodbuttons\\}", methodButtonHtml.toString())
                .getBytes();

        exchange.sendResponseHeaders(200, page.length);
        exchange.getResponseBody().write(page);
        exchange.close();
        System.err.format("[200] [%s] OK\n", url);
      } catch (Throwable t) {
        t.printStackTrace();
        System.err.format("[500] [%s] Error: %s\n", url, t);
      }
    }
  }

  /**
   * Handle requests for static image data.
   */
  private static class PictureDataHandler implements HttpHandler {
    @Override public void handle(HttpExchange exchange) throws IOException {
      URI url = exchange.getRequestURI();
      try {
        // Which picture are they requesting?
        String request = url.getPath();
        String file =
          request
              .substring(request.lastIndexOf('/') + 1);

        // Read the picture into an image
        Picture picture = PictureIO.read(file);

        // Call any requested methods on the picture
        String query = url.getQuery();
        String[] methods = query == null ? new String[0] : query.split("&");
        for (String methodName : methods) {
          // Skip the obvious non-methods
          if (methodName == null || methodName.isEmpty()) {
            continue;
          }

          // Try using the method
          Method method;
          try {
            method = Picture.class.getMethod(methodName);
          } catch (Exception e) {
            System.err.format("Skipping picture transform [%s]\n", methodName);
            continue;
          }
          method.invoke(picture);
        }

        // Write the picture out to the client/browser
        exchange.sendResponseHeaders(200, 0 /* chunked encoding */);
        PictureIO.write(picture, exchange.getResponseBody());
        exchange.close();
        System.err.format("[200] [%s] OK\n", url);
      } catch (Throwable t) {
        t.printStackTrace();
        System.err.format("[500] [%s] Error: %s\n", url, t);
      }
    }
  }
}

