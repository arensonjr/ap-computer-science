package pix;

/**
 * A single pixel in an image.
 */
public class Pixel {

  // Declare your fields here.

  /**
   * Create a new pixel with the specified colors.
   */
  public Pixel(int initialRed, int initialGreen, int initialBlue) {
    // Initialize your fields here.
  }

  // Write your methods here.
}

