package lab10.solutions;

public class ConvertDistances {
  public static void main(String[] args) {
    double input = Double.parseDouble(args[0]);
    System.out.println(kilometersToMiles(input));
  }

  /**
   * Returns the number of miles that is equal to the input distance in kilometers.
   */
  public static double kilometersToMiles(double kilometers) {
    return kilometers * 0.62;
  }
}
