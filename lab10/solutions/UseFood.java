package lab10.solutions;

/**
 * Try using the Food class.
 */
public class UseFood {
  public static void main(String[] args) {
    Food carrot = new Food("carrot", "vegetable", 25, 4.0);
    Food donut = new Food("donut", "grain", 195, 2.0);

    Food healthiestFood = healthier(carrot, donut);
    System.out.println(healthiestFood.getName() + " is healthiest.");
  }

  /**
   * Returns the food that is healthier than the other.
   */
  public static Food healthier(Food one, Food two) {
    if (one.caloriesPerOunce() < two.caloriesPerOunce()) {
      return one;
    } else {
      return two;
    }
  }
}
