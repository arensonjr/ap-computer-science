package lab10.solutions;

public class Numbers {
  public static void main(String[] args) {
    int input = Integer.parseInt(args[0]);
    System.out.println(numberToWords(input));
  }

  /**
   * Returns the English representation of the input number.
   */
  public static String numberToWords(int number) {
    // Notes are taken from the algorithm in the lab solutions doc.

    // Initialize words to an empty string
    String words = "";

    // Repeat the following until number is 0
    while (number > 0) {
      // Get the last digit of number
      int lastDigit = number % 10;

      // Convert the digit to a word
      String word = getWord(lastDigit);

      // Add the word to the front of words
      words = word + " " + words;

      // Remove the last digit of number
      number = number / 10;
    }

    return words;
  }

  /**
   * Converts a digit (integer from 0 to 9) to a word.
   */
  public static String getWord(int digit) {
    String[] digitWords = {
        "zero",
        "one",
        "two",
        "three",
        "four",
        "five",
        "six",
        "seven",
        "eight",
        "nine"
    };
    return digitWords[digit];
  }
}
