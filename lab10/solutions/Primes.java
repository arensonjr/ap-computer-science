package lab10.solutions;

public class Primes {
  public static void main(String[] args) {
    int input = Integer.parseInt(args[0]);
    int[] primes = firstNPrimes(input);
    for (int prime : primes) {
      System.out.print(prime + " ");
    }
  }

  /**
   * Returns the first N primes.
   */
  public static int[] firstNPrimes(int n) {
    int[] primes = new int[n];

    // Keep searching until we've found n primes.
    int numFound = 0;
    int number = 2;
    while (numFound < n) {
      // If this number is prime, count it and add it to our array.
      if (isPrime(number)) {
        primes[numFound] = number;
        numFound++;
      }

      // Either way, move on to the next number
      number++;
    }

    return primes;
  }

  /**
   * Returns whether or not number is prime.
   */
  public static boolean isPrime(int number) {
    // A number is prime if and only if its only divisors are 1 and itself.
    for (int divisor = 2; divisor < number; divisor++) {
      if (number % divisor == 0) {
        // Uh oh, we've found a factor of number -- it can't be prime
        return false;
      }
    }

    // We didn't find any factors of number, so it must be prime
    return true;
  }
}
