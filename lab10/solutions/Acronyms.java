package lab10.solutions;

public class Acronyms {
  public static void main(String[] args) {
    String acronym = acronimify(args);
    System.out.println(acronym);
  }

  /**
   * Returns a string made up of the first characters of each input string.
   */
  public static String acronimify(String[] words) {
    String acronym = "";
    for (String word : words) {
      acronym += word.charAt(0);
    }
    return acronym;
  }
}

