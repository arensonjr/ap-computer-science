package lab10.solutions;

/**
 * Represents a type of food.
 */
public class Food {
  /**
   * Name of the food.
   */
  String name;

  /**
   * The food's group in the food pyramid.
   */
  String category;

  /**
   * Number of calories in this food.
   */
  int numCalories;

  /**
   * Weight of this food, in ounces.
   */
  double weightOunces;

  /**
   * Creates a new type of food.
   */
  public Food(String n, String c, int nc, double w) {
    name = n;
    category = c;
    numCalories = nc;
    weightOunces = w;
  }

  /**
   * Returns the name of the food.
   */
  public String getName() {
    return name;
  }

  /**
   * Returns the number of calories per ounce in this food.
   */
  public double caloriesPerOunce() {
    return numCalories / weightOunces;
  }
}
