package lab10.solutions;

public class Reverse {
  public static void main(String[] args) {
    String[] reversed = reverseStringArray(args);
    for (String word : reversed) {
      System.out.print(word + " ");
    }
  }

  /**
   * Returns the reverse of the input array.
   */
  public static String[] reverseStringArray(String[] strings) {
    String[] reversed = new String[strings.length];
    for (int i = 0; i < strings.length; i++) {
      int iFromEnd = reversed.length - 1 - i;
      reversed[iFromEnd] = strings[i];
    }
    return reversed;
  }
}

