package lab10.solutions;

import lab10.Country;

/**
 * Try using the Country class.
 */
public class UseCountries {
  public static void main(String[] args) {
    Country usa = new Country("USA", "Washington D.C.", 319000000, 16770000000000.0);
    Country canada = new Country("Canada", "Ottawa", 35000000, 1827000000000.0);

    double myMoney = usa.getGdpPerCapita() + canada.getGdpPerCapita();
    System.out.println(
        "If I lived in both the USA and Canada, "
        + "I would have made (on average) "
        + myMoney);
  }
}
