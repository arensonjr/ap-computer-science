package lab10.solutions;

import lab4.solutions.ParseInput;

public class Multiplication {
  public static void main(String[] args) {
    double[] input = ParseInput.asDoubles(args);
    System.out.println(product(input));
  }

  /**
   * Returns the product of all the input numbers.
   */
  public static double product(double[] numbers) {
    double product = 1.0;
    for (double num : numbers) {
      product = product * num;
    }
    return product;
  }
}

