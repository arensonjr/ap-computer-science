package lab10.solutions;

public class Concatenate {
  public static void main(String[] args) {
    String concatenated = concatenateThree(args[0], args[1], args[2]);
    System.out.println(
        "[" + args[0] + ", " + args[1] + ", " + args[2] + "]"
        + " concatenated is \"" + concatenated + "\"");
  }

  /**
   * Combines three strings together with spaces in the middle.
   */
  public static String concatenateThree(String first, String second, String third) {
    return first + " " + second + " " + third;
  }
}

