package lab10;

public class Primes {
  public static void main(String[] args) {
    int input = Integer.parseInt(args[0]);
    int[] primes = firstNPrimes(input);
    for (int prime : primes) {
      System.out.print(prime + " ");
    }
  }

  /**
   * Returns the first N primes.
   */
  public static int[] firstNPrimes(int n) {
    // Write your code here.
  }
}

