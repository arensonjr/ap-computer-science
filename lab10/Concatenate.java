package lab10;

public class Concatenate {
  public static void main(String[] args) {
    String concatenated = concatenateThree(args[0], args[1], args[2]);
    System.out.println(
        "[" + args[0] + ", " + args[1] + ", " + args[2] + "]"
        + " concatenated is \"" + concatenated + "\"");
  }

  // Write your concatenateThree function here.
}

