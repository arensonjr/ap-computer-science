package lab10;

import lab4.solutions.ParseInput;

public class Multiplication {
  public static void main(String[] args) {
    double[] input = ParseInput.asDoubles(args);
    System.out.println(product(input));
  }

  // Write your product function here.
}

