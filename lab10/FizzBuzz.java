package lab10;

public class FizzBuzz {
  public static void main(String[] args) {
    int input = Integer.parseInt(args[0]);
    System.out.println(fizzbuzz(input));
  }

  /**
   * Returns:
   *   - "Fizzbuzz" if the input is divisible by both 3 and 5,
   *   - "Fizz" if it's divisible by only 3,
   *   - "Buzz" if it's divisible by only 5, or
   *   - ":(" if it's divisible by neither.
   */
  public static String fizzbuzz(int number) {
    // Write your code here.
  }
}

