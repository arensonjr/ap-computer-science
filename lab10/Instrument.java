package lab10;

/**
 * Represents a musical instrument.
 */
public class Instrument {
  /**
   * Name of the instrument (eg "bassoon").
   */
  String name;

  /**
   * Musical group that the instrument belongs to (eg "woodwind").
   */
  String group;

  /**
   * Lowest note the instrument can play (eg "B2").
   */
  String lowestNote;

  /**
   * Highest note the instrument can play (eg "D6").
   */
  String highestNote;

  public Instrument(String n, String g, String ln, String hn) {
    name = n;
    group = g;
    lowestNote = ln;
    highestNote = hn;
  }

  /**
   * Returns the name of the instrument.
   */
  public String getName() {
    return name;
  }

  /**
   * Returns the range of the instrument.
   */
  public String getRange() {
    return lowestNote + "-" + highestNote;
  }
}

