package lab10;

/**
 * Represents a country of the world.
 */
public class Country {
  /**
   * Name of the country (eg "USA").
   */
  String name;

  /**
   * Capital of the country (eg "Washington D.C.").
   */
  String capital;

  /**
   * Population of the country (eg 318900000).
   */
  int population;

  /**
   * Gross domestic product (total income per year) of the country.
   */
  double grossDomesticProduct;

  public Country(String n, String c, int p, double gdp) {
    name = n;
    capital = c;
    population = p;
    grossDomesticProduct = gdp;
  }

  /**
   * Returns the country's gross domestic product (total income per year).
   */
  public double getGdp() {
    return grossDomesticProduct;
  }

  /**
   * Returns the average income per-person per-year in this country.
   */
  public double getGdpPerCapita() {
    return grossDomesticProduct / population;
  }
}
