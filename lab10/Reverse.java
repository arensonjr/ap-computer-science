package lab10;

public class Reverse {
  public static void main(String[] args) {
    String[] reversed = reverseStringArray(args);
    for (String word : reversed) {
      System.out.print(word + " ");
    }
  }

  /**
   * Returns the reverse of the input array.
   */
  public static String[] reverseStringArray(String[] strings) {
    // Write your code here.
  }
}

