package assignment1.solutions;

/**
 * Solutions to Assignment 1.
 */
public class Cashier {
    public static void main(String[] args) {
        // Parse the two program inputs: cost, and amount paid (both in dollars)
        double costDollars = Double.parseDouble(args[0]);
        double paidDollars = Double.parseDouble(args[1]);
        double changeDollars = paidDollars - costDollars;

        // Convert to cents, since then we can use integers
        int changeCents = (int)(100 * changeDollars);

        // How many twenties?
        int twenties = changeCents / (20 * 100);
        changeCents -= twenties * 20 * 100;

        // How many tens?
        int tens = changeCents / (10 * 100);
        changeCents -= tens * 10 * 100;

        // How many fives?
        int fives = changeCents / (5 * 100);
        changeCents -= fives * 5 * 100;

        // How many ones?
        int ones = changeCents / (1 * 100);
        changeCents -= ones * 1 * 100;

        // How many quarters?
        int quarters = changeCents / 25;
        changeCents -= quarters * 25;

        // How many dimes?
        int dimes = changeCents / 10;
        changeCents -= dimes * 10;

        // How many nickels?
        int nickels = changeCents / 5;
        changeCents -= nickels * 5;

        // Everything left must be pennies
        int pennies = changeCents;

        // Print out the change in the required format
        System.out.println("The cost is $" + costDollars + ". You paid $" + paidDollars + ". Your change is:");
        System.out.println(twenties + " twenties");
        System.out.println(tens     + " tens");
        System.out.println(fives    + " fives");
        System.out.println(ones     + " ones");
        System.out.println(quarters + " quarters");
        System.out.println(dimes    + " dimes");
        System.out.println(nickels  + " nickels");
        System.out.println(pennies  + " pennies");
    }
}
