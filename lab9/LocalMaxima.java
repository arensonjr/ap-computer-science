package lab9;

import java.util.ArrayList;
import java.util.HashSet;

import lab4.solutions.ParseInput;

/**
 * Prints out each of the input numbers, with duplicates removed.
 */
public class LocalMaxima {
  public static void main(String[] args) {
    int[] numbers = ParseInput.asIntegers(args);

    System.out.println("Original input:");
    for (int num : numbers) {
      System.out.print(num + " ");
    }
    System.out.println();

    System.out.println("Local maxima:");
    // Uncomment the following code once you've chosen a type and
    // implemented the localMaxima() method:
    /*
    System.out.println(localMaxima(numbers));
    */
  }

  /**
   * Returns the local maxima of the input array.
   */
  public static void /* Choose a type! */ localMaxima(int[] numbers) {
    // Write your code here
  }
}
