package lab9;

import java.util.ArrayList;
import java.util.HashSet;

import lab4.solutions.ParseInput;

/**
 * Prints out each of the input numbers, with duplicates removed.
 */
public class Deduplicate {
  public static void main(String[] args) {
    int[] numbers = ParseInput.asIntegers(args);

    System.out.println("Original input:");
    for (int num : numbers) {
      System.out.print(num + " ");
    }
    System.out.println();

    System.out.println("Without duplicates:");
    // Uncomment the following code once you've chosen a type and
    // implemented the deduplicate() method:
    /*
    System.out.println(deduplicate(numbers));
    */
  }

  /**
   * Returns the same numbers in the input array, but without duplicates.
   */
  public static void /* Choose a type! */ deduplicate(int[] numbers) {
    // Write your code here
  }
}

