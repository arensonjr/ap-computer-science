package lab9.solutions;

import java.util.HashSet;

import lab4.solutions.ParseInput;

/**
 * Prints out each of the *even* input numbers.
 */
public class EvensOnly {
  public static void main(String[] args) {
    int[] numbers = ParseInput.asIntegers(args);

    System.out.println("Original input:");
    for (int num : numbers) {
      System.out.print(num + " ");
    }
    System.out.println();

    System.out.println("Even numbers only:");
    System.out.println(evenOnly(numbers));
  }

  /**
   * Returns only one copy of each even number from the input array.
   */
  public static HashSet<Integer> evenOnly(int[] numbers) {
    // We don't want to include duplicates, so a HashSet will make
    // that far easier than a list (it removes duplicates automatically).
    HashSet<Integer> evens = new HashSet<>();
    for (int num : numbers) {
      if (num % 2 == 0) {
        evens.add(num);
      }
    }
    return evens;
  }
}


