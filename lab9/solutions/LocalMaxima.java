package lab9.solutions;

import java.util.ArrayList;

import lab4.solutions.ParseInput;

/**
 * Prints out each of the input numbers, with duplicates removed.
 */
public class LocalMaxima {
  public static void main(String[] args) {
    int[] numbers = ParseInput.asIntegers(args);

    System.out.println("Original input:");
    for (int num : numbers) {
      System.out.print(num + " ");
    }
    System.out.println();

    System.out.println("Local maxima:");
    System.out.println(localMaxima(numbers));
  }

  /**
   * Returns the local maxima of the input array.
   */
  public static ArrayList<Integer> localMaxima(int[] numbers) {
    // We don't care about duplicates (in fact, we want to keep them if
    // they occur) and it's important to get the maxima in order, so we
    // don't want a HashSet.
    ArrayList<Integer> maxes = new ArrayList<>();

    // Only iterate over the indices that have both neighbors -- skip the
    // far left end (index #0), and skip the far right end (index #length-1).
    for (int i = 1; i < numbers.length - 1; i++) {
      int prev = numbers[i - 1];
      int curr = numbers[i];
      int next = numbers[i + 1];

      // Local maximum: greater than both of its neighbors
      if ((curr > prev) && (curr > next)) {
        maxes.add(curr);
      }
    }

    return maxes;
  }
}
