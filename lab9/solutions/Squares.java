package lab9.solutions;

import java.util.ArrayList;

import lab4.solutions.ParseInput;

/**
 * Prints out each of the input numbers, squared.
 */
public class Squares {
  public static void main(String[] args) {
    int[] numbers = ParseInput.asIntegers(args);

    System.out.println("Original input:");
    for (int num : numbers) {
      System.out.print(num + " ");
    }
    System.out.println();

    System.out.println("Squared:");
    System.out.println(squareAll(numbers));
  }

  /**
   * Returns the same numbers in the input array, but without duplicates.
   */
  public static ArrayList<Integer> squareAll(int[] numbers) {
    // We want to keep each number in the list, in order, and include
    // duplicates -- otherwise, our output won't correspond to our input
    // at all! Therefore, we need to use a list.
    ArrayList<Integer> squared = new ArrayList<>();
    for (int num : numbers) {
      squared.add(num * num);
    }
    return squared;
  }
}
