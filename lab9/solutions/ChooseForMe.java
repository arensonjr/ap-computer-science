package lab9.solutions;

import java.util.Random;

/**
 * Randomly prints one of the input arguments.
 */
public class ChooseForMe {
  public static void main(String[] args) {
    Random random = new Random();
    int randomIndex = random.nextInt(args.length);
    System.out.println(args[randomIndex]);
  }
}
