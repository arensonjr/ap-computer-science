package lab9.solutions;

import java.util.Random;

/**
 * Randomly prints a message either liking or disliking the user.
 */
public class DoILikeYou {
  public static void main(String[] args) {
    boolean iLikeYou = new Random().nextBoolean();
    if (iLikeYou) {
      System.out.println("You're one pretty hoopy frood!");
    } else {
      System.out.println(
          "Your mother was a hamster, and your father smelt of elderberries.");
    }
  }
}
