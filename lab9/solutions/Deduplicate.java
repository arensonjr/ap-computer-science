package lab9.solutions;

import java.util.HashSet;

import lab4.solutions.ParseInput;

/**
 * Prints out each of the input numbers, with duplicates removed.
 */
public class Deduplicate {
  public static void main(String[] args) {
    int[] numbers = ParseInput.asIntegers(args);

    System.out.println("Original input:");
    for (int num : numbers) {
      System.out.print(num + " ");
    }
    System.out.println();

    System.out.println("Without duplicates:");
    System.out.println(deduplicate(numbers));
  }

  /**
   * Returns the same numbers in the input array, but without duplicates.
   */
  public static HashSet<Integer> deduplicate(int[] numbers) {
    // HashSets automatically deduplicate for us, so all we have to
    // do is stick everything into a set and return it. Much easier than
    // a list!
    HashSet<Integer> numberSet = new HashSet<>();
    for (int num : numbers) {
      numberSet.add(num);
    }
    return numberSet;
  }
}

