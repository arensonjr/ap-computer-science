package lab9;

import java.util.ArrayList;
import java.util.HashMap;

import lab4.solutions.ParseInput;

/**
 * Prints out each of the input numbers, squared.
 */
public class Squares {
  public static void main(String[] args) {
    int[] numbers = ParseInput.asIntegers(args);

    System.out.println("Original input:");
    for (int num : numbers) {
      System.out.print(num + " ");
    }
    System.out.println();

    System.out.println("Squared:");
    // Uncomment the following code once you've chosen a type and
    // implemented the squared() method:
    /*
    System.out.println(squared(numbers));
    */
  }

  /**
   * Returns the same numbers in the input array, but without duplicates.
   */
  public static void /* Choose a type! */ squareAll(int[] numbers) {
    // Write your code here
  }
}

