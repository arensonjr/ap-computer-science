package lab9;

import java.util.ArrayList;
import java.util.HashSet;

import lab4.solutions.ParseInput;

/**
 * Prints out each of the *even* input numbers.
 */
public class EvensOnly {
  public static void main(String[] args) {
    int[] numbers = ParseInput.asIntegers(args);

    System.out.println("Original input:");
    for (int num : numbers) {
      System.out.print(num + " ");
    }
    System.out.println();

    System.out.println("Even numbers only:");
    // Uncomment the following code once you've chosen a type and
    // implemented the evenOnly() method:
    /*
    System.out.println(evenOnly(numbers));
    */
  }

  /**
   * Returns only the even numbers from the input array.
   */
  public static void /* Choose a type! */ evenOnly(int[] numbers) {
    // Write your code here
  }
}


