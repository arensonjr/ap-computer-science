package lab7.solutions;

/**
 * A program which prints its input back out, censored.
 */
class Censored {
  public static void main(String[] args) {
    // Read each word, either printing it or printing a censored string (if it's 4 letters long)
    for (String arg : args) {
      if (arg.length() == 4) {
        System.out.print("!?*$");
      } else {
        System.out.print(arg);
      }

      // We need to include spaces between (after) each word, too!
      System.out.print(" ");
    }
  }
}
