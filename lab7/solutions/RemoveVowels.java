package lab7.solutions;

/**
 * A program which prints out its input with the vowels removed.
 */
class RemoveVowels {
  public static void main(String[] args) {
    String input = args[0];

    for (int i = 0; i < input.length(); i++) {
      char nextChar = input.charAt(i);

      // Only print the character if it's not a vowel
      if ((nextChar != 'a')
          && (nextChar != 'e')
          && (nextChar != 'i')
          && (nextChar != 'o')
          && (nextChar != 'u')) {
        System.out.print(nextChar);
      }
    }
  }
}
