package lab7.solutions;

/**
 * Prints out whether its input argument is a palindrome.
 */
class PalindromeV1 {
  public static void main(String[] args) {
    String word = args[0];

    // A word is a palindrome it reads the same forward as backwards. For our
    // purposes, this means that it's a palindrome if the character that's N
    // indices after the front of the string is the same as the character N
    // indices before the end of the string.
    //
    // Example: "racecar"
    //
    //  "r   a   c   e   c   a   r"
    //
    //   ^   ^   ^   ^   ^   ^   ^
    //   |   |   |   |   |   |   |
    //   |   |   |   v   |   |   |
    //   |   |   |   3   |   |   |
    //   |   |   |       |   |   |
    //   |   |   v       v   |   |
    //   |   |   2 - (end-2) |   |
    //   |   |               |   |
    //   |   v               v   |
    //   |   1 --------- (end-1) |
    //   |                       |
    //   v                       v
    //   0 ---------------- (end-0)
    //
    //
    // So, let's walk through the string:
    for (int i = 0; i < word.length(); i++) {
      char iFromBeginning = word.charAt(i);
      char iFromEnd = word.charAt(word.length() - i);

      if (iFromEnd != iFromBeginning) {
        // It's not a palindrome! These two characters don't match.
        System.out.println(word + " is not a palindrome");

        // Quit the main() function early (so that we don't exit the loop and
        // accidentally print that it *is* a palindrome)
        return;
      }
    }

    // If we've checked every single character pair, and they *all* match, then
    // the word must be a palindrome.
    System.out.println(word + " is a palindrome");
  }
}
