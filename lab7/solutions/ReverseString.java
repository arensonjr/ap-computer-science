package lab7.solutions;

/**
 * A program which prints out its input in reverse.
 */
class ReverseString {
  public static void main(String[] args) {
    String input = args[0];
    for (int i = input.length() - 1; i >= 0; i--) {
      System.out.print(input.charAt(i));
    }
  }
}
