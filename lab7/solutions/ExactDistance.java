package lab7.solutions;

import java.awt.Point;

/**
 * A program which calculates the exact distance between four hard-coded points.
 */
class ExactDistance {
  public static void main(String[] args) {
    Point first = new Point(0, 0);
    Point second = new Point(5, 25);
    Point third = new Point(-3, 13);
    Point fourth = new Point(20, 1);

    System.out.println(
        first.distance(second)
        + second.distance(third)
        + third.distance(fourth));
  }
}
