package lab7.solutions;

/**
 * A program which prints out each character in its input on a separate line.
 */
class SeparateLines {
  public static void main(String[] args) {
    String input = args[0];
    for (int i = 0; i < input.length(); i++) {
      System.out.println(input.charAt(i));
    }
  }
}
