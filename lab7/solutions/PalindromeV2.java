package lab7.solutions;

/**
 * Prints out whether its input argument is a palindrome.
 */
class PalindromeV2 {
  public static void main(String[] args) {
    String word = args[0];

    // A word is a palindrome it reads the same forward as backwards.
    // So, let's just reverse the string, and compare it to the original!
    String reversedWord = reverse(word);
    if (word.equals(reversedWord)) {
      System.out.println(word + " is a palindrome");
    } else {
      System.out.println(word + " is not a palindrome");
    }
  }

  /**
   * Reverses a string.
   *
   * (this function is also discussed in ReverseString solution)
   */
  public static String reverse(String input) {
    String output = "";
    for (int i = input.length() - 1; i >= 0; i--) {
      output += input.charAt(i);
    }
    return output;
  }
}
