package lab1.solutions;

public class CelsiusToFahrenheit {
    public static void main(String[] args) {
        // Input: degrees celsius
        double celsius = Double.parseDouble(args[0]);

        // Output: degrees fahrenheit
        double fahrenheit = (9.0/5.0) * celsius + 32;

        System.out.println(celsius + " degrees Celsius converts to " + fahrenheit + " degrees Fahrenheit");
    }
}
