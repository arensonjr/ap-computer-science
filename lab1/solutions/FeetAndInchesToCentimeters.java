package lab1.solutions;

public class FeetAndInchesToCentimeters {
    public static void main(String[] args) {
        // Input: Feet and inches
        int feet = Integer.parseInt(args[0]);
        int inches = Integer.parseInt(args[1]);

        // First, convert everything to inches: 1 ft == 12 in
        int totalInches = inches + (feet * 12);

        // Then, convert to centimeters: 1 in == 2.54 cm
        double centimeters = totalInches * 2.54;
        System.out.println(feet + " feet and " + inches + " inches is " + centimeters + " centimeters");
    }
}
