package lab1.solutions;

public class CentimetersToFeetAndInches {
    public static void main(String[] args) {
        // Input: Centimeters
        double centimeters = Double.parseDouble(args[0]);

        // First, convert to inches: 1 in == 2.54 cm
        int inches = (int)(centimeters / 2.54);

        // Split into feet and inches
        int feet = inches / 12;
        int remainderInches = inches % 12;

        System.out.println(centimeters + " centimeters is " + feet + " feet and " + remainderInches + " inches");
    }
}
