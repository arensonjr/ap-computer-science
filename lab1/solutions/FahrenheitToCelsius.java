package lab1.solutions;

public class FahrenheitToCelsius {
    public static void main(String[] args) {
        // Input: degrees fahrenheit
        double fahrenheit = Double.parseDouble(args[0]);

        // Output: degrees celsius
        double celsius = (5.0/9.0) * (fahrenheit - 32);

        System.out.println(fahrenheit + " degrees Fahrenheit converts to " + celsius + " degrees Celsius");
    }
}
