package assignment9;

import java.util.ArrayList;

/**
 * Generates fake speech (a "riff") which is similar to an input text corpus,
 * by using Markov Models and Markov Chains.
 */
public class GeneratedSpeech {
  /**
   * Generates a random Markov Chain for a particular text corpus.
   *
   * The generated chain should contain exactly numWords words.
   */
  public static ArrayList<String> generateMarkovChain(String corpusName, int numWords) {
    ArrayList<String> markovChain = new ArrayList<>();

    // Make sure to write an algorithm first!
    //
    // Replace the following line with your code.
    return markovChain;
  }

  public static void main(String[] args) {
    // Parse & describe inputs
    String corpusName = args[0];
    int numWords = Integer.parseInt(args[1]);
    System.out.println(
        "Generating a " + numWords + "-long Markov Chain for corpus "
        + corpusName + ":");
    System.out.println();

    ArrayList<String> markovChain = generateMarkovChain(corpusName, numWords);

    // Print out the Markov Chain as if it were a sentence
    // (each word separated by spaces), rather than a list
    for (String word : markovChain) {
      System.out.print(word + " ");
    }
  }
}
