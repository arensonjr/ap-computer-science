package objects.solutions;

/**
 * A class to represent a time (in hours, minutes, and seconds).
 */
public class Time {
  int hours;
  int minutes;
  int seconds;

  /**
   * Creates a new time, to represent h:m:s.
   */
  public Time(int h, int m, int s) {
    hours = h;
    minutes = m;
    seconds = s;
  }

  public int getHours() {
    return hours;
  }

  public int getMinutes() {
    return minutes;
  }

  public int getSeconds() {
    return seconds;
  }

  /**
   * Pretty-prints this time as a string.
   */
  public String toString() {
    String hourStr = "" + getHours();
    String minutesStr = "" + getMinutes();
    if (minutesStr.length() < 2) {
      minutesStr = "0" + minutesStr;
    }
    String secondsStr = "" + getSeconds();
    if (secondsStr.length() < 2) {
      secondsStr = "0" + secondsStr;
    }
    return hourStr + ":" + minutesStr + "." + secondsStr;
  }

  /**
   * Returns true if this time is valid:
   *   - 0 <= hours < 24
   *   - 0 <= minutes < 60
   *   - 0 <= seconds < 60
   * Otherwise, returns false.
   */
  public boolean isValid() {
    return (hours >= 0 && hours < 24)
      && (minutes >= 0 && minutes < 60)
      && (seconds >= 0 && seconds < 60);
  }

  /**
   * Advances the time by h hours.
   */
  public void advanceHours(int h) {
    hours = (hours + h) % 24;
  }

  /**
   * Advances the time by the amount of otherTime.
   */
  public void advanceTime(Time otherTime) {
    seconds += otherTime.getSeconds();
    minutes += otherTime.getMinutes();
    hours += otherTime.getHours();

    // Make sure the time is still valid
    minutes += seconds / 60;
    seconds %= 60;

    hours += minutes / 60;
    minutes %= 60;

    hours %= 24;
  }
}
