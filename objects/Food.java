package objects;

/**
 * Represents a type of food.
 */
public class Food {
  /**
   * Name of the food.
   */
  String name;

  /**
   * The food's group in the food pyramid.
   */
  String category;

  /**
   * Number of calories in this food.
   */
  int numCalories;

  /**
   * Weight of this food, in ounces.
   */
  double weightOunces;

  /**
   * Creates a new type of food.
   */
  public Food(String n, String c, int nc, double w) {
    name = n;
    category = c;
    numCalories = nc;
    weightOunces = w;
  }

  /**
   * Returns the name of the food.
   */
  public String getName() {
    return name;
  }

  /**
   * Returns the number of calories in the food.
   */
  public int getCalories() {
    return numCalories;
  }

  /**
   * Returns the weight of this food.
   */
  public double getWeight() {
    return weightOunces;
  }

  /**
   * Returns the food group of the food.
   */
  public String getFoodGroup() {
    return category;
  }

  /**
   * Returns the number of calories per ounce in this food.
   */
  public double caloriesPerOunce() {
    return numCalories / weightOunces;
  }

  public String toString() {
    return name;
  }
}
