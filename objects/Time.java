package objects;

/**
 * A class to represent a time (in hours, minutes, and seconds).
 */
public class Time {
  int hours;
  int minutes;
  int seconds;

  /**
   * Creates a new time, to represent h:m:s.
   */
  public Time(int h, int m, int s) {
    hours = h;
    minutes = m;
    seconds = s;
  }

  public int getHours() {
    return hours;
  }

  public int getMinutes() {
    return minutes;
  }

  public int getSeconds() {
    return seconds;
  }
}
