package lab16;

/**
 * Performs a withdrawal from a bank account.
 */
public class ATM {
  public static void main(String[] args) {
    double amountToWithdraw = Double.parseDouble(args[0]);
    String password = args[1];

    // Starting balance of the bank account = $1,000
    BankAccount account = new BankAccount(1000.00, "supersecret");

    // 1. I can steal directly from your balance without any password at all!
    double withdrawal = 5000;
    account.balance -= withdrawal;
    System.out.println(
        "Successfully withdrew $" +  withdrawal + " from the account.");

    // 2. It still doesn't matter what the account's password is, I can
    //    withdraw whatever I want!
    account.password = password;
    withdrawal = account.withdraw(amountToWithdraw, password);
    System.out.println(
        "Successfully withdrew $" +  withdrawal + " from the account.");

    // 3. I can withdraw more than is in your account!
    withdrawal = account.withdraw(9000.00, password);
    System.out.println(
        "Successfully withdrew $" +  withdrawal + " from the account.");

    // 4. I can deposit negative money to steal from you!
    double oldBalance = account.getBalance();
    account.deposit(-50.00);
    double newBalance = account.getBalance();
    System.out.println(
        "Successfully 'deposited' " + (newBalance - oldBalance)
        + " into the account");
  }
}

