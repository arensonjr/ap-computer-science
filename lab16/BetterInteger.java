package lab16;

/**
 * Represents an integer, but provides even more useful methods.
 */
public class BetterInteger {
  // The actual integer value.
  int value;

  /**
   * Creates a new integer wrapper.
   */
  public BetterInteger(int v) {
    value = v;
  }

  /**
   * Adds a new digit to the end of this number.
   *
   * e.g.
   *    BetterInteger num = new BetterInteger(0);
   *    // 0 => 5
   *    num.addDigit(5);
   *    // 5 => 56
   *    num.addDigit(56);
   *    // 5 => 560
   *    num.addDigit(0);
   */
  public void addDigit(int digit) {
    if (digit < 0 || digit > 9) {
      throw new IllegalArgumentException("Digit must be between 0-9: " + digit);
    }

    // Shift the original number to the left, and add the new digit
    value = (value * 10) + digit;
  }

  /**
   * Returns the number of digits in this integer (in base 10).
   */
  public int numDigits() {
    // It looks like it has one more digit than its base-ten logarithm:
    //
    // log_10(10) => 1
    // log_10(100) => 2
    // log_10(500) => 2.69
    return 1 + (int) Math.log10(value);
  }

  /**
   * Returns the Nth digit of this integer, starting from the right-hand side.
   *
   * e.g.
   *    new BetterInteger(12345).getNthDigit(0) == 5
   *    new BetterInteger(12345).getNthDigit(4) == 1
   */
  public int getNthDigit(int n) {
    // Make a copy so we don't accidentally change our value
    int thisNum = value;

    // We need to keep chopping digits off the end until we reach n
    for (int digit = 0; digit < n; digit++) {
      thisNum /= 10;
    }
    // Now, we just have to take the first remaining digit
    return thisNum % 10;
  }

  /**
   * Gets the underlying number.
   */
  public int getValue() {
    return value;
  }

  /**
   * Prints out a pretty representation of the number.
   */
  public String toString() {
    return "BetterInteger(" + value + ")";
  }
}
