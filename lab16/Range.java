package lab16;

/**
 * Represents a range of numbers on a number line.
 */
public class Range {
  // Write your under-the-hood variables and constructor here.

  /**
   * Returns the start value of this range.
   */
  public double getStart() {
    // Implement this method.
  }

  /**
   * Returns the end value of this range.
   */
  public double getEnd() {
    // Implement this method.
  }

  /**
   * Extends the range forward by N.
   */
  public void extendForward(double n) {
    // Implement this method.
  }

  /**
   * Returns whether N is contained within this range.
   *
   * e.g.
   *    new Range(5, 10).contains(7)     => true
   *    new Range(5, 10).contains(9.99)  => true
   *    new Range(5, 10).contains(5)     => false
   *    new Range(5, 10).contains(10)    => false
   *    new Range(5, 10).contains(0)     => false
   *    new Range(5, 10).contains(50)    => false
   */
  public boolean contains(double number) {
    // Implement this method.
  }

  /**
   * Returns whether this range overlaps the other one.
   *
   * e.g.
   *    Range r1 = new Range(5, 10);
   *    new Range(3, 6).overlap(r1)   => true
   *    new Range(9, 20).overap(r1)   => true
   *    new Range(0, 100).overlap(r1) => true
   *
   *    new Range(3, 5).overlap(r1)   => false
   *    new Range(10, 20).overlap(r1) => false
   *    new Range(-2, -1).overlap(r1) => false
   */
  public boolean overlaps(Range other) {
    // Implement this method.
  }
}
