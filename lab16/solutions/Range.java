package lab16.solutions;

/**
 * Represents a range of numbers on a number line.
 */
public class Range {
  // The first number (where the range starts)
  double start;

  // The last number (where the range ends)
  double end;

  /**
   * Creates a new range, from low to high.
   */
  public Range(double low, double high) {
    start = low;
    end = high;
  }

  /**
   * Returns the start value of this range.
   */
  public double getStart() {
    return start;
  }

  /**
   * Returns the end value of this range.
   */
  public double getEnd() {
    return end;
  }

  /**
   * Extends the range forward by N.
   */
  public void extendForward(double n) {
    end += n;
  }

  /**
   * Returns whether N is contained within this range.
   *
   * e.g.
   *    new Range(5, 10).contains(7)     => true
   *    new Range(5, 10).contains(9.99)  => true
   *    new Range(5, 10).contains(5)     => false
   *    new Range(5, 10).contains(10)    => false
   *    new Range(5, 10).contains(0)     => false
   *    new Range(5, 10).contains(50)    => false
   */
  public boolean contains(double number) {
    return (number > start) && (number < end);
  }

  /**
   * Returns whether this range overlaps the other one.
   *
   * e.g.
   *    Range r1 = new Range(5, 10);
   *    new Range(3, 6).overlap(r1)   => true
   *    new Range(9, 20).overap(r1)   => true
   *    new Range(0, 100).overlap(r1) => true
   *
   *    new Range(3, 5).overlap(r1)   => false
   *    new Range(10, 20).overlap(r1) => false
   *    new Range(-2, -1).overlap(r1) => false
   */
  public boolean overlaps(Range other) {
    return
         other.contains(start)
      || other.contains(end)
      || contains(other.getStart())
      || contains(other.getEnd());
  }
}
