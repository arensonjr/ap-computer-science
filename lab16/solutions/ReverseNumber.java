package lab16.solutions;

import lab16.BetterInteger;

/**
 * Reverses the digits in a number.
 */
public class ReverseNumber {
  public static void main(String[] args) {
    int number = Integer.parseInt(args[0]);

    // Use BetterIntegers to make our lives easier
    BetterInteger wrappedNumber = new BetterInteger(number);
    BetterInteger reversed = new BetterInteger(0);

    // Copy digits from the input number to the reversed number
    for (int i = 0; i < wrappedNumber.numDigits(); i++) {
      int digit = wrappedNumber.getNthDigit(i);
      reversed.addDigit(digit);
    }

    // Print out the answer
    System.out.println(reversed.getValue());
  }
}
