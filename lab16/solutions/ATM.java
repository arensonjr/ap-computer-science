package lab16.solutions;

/**
 * Performs a withdrawal from a bank account.
 */
public class ATM {
  public static void main(String[] args) {
    double amountToWithdraw = Double.parseDouble(args[0]);
    String password = args[1];

    // Starting balance of the bank account = $1,000
    BankAccount account = new BankAccount(1000.00, "supersecret");

    // Everything has to be correct to withdraw money
    double withdrawal = account.withdraw(amountToWithdraw, password);
    System.out.println(
        "Successfully withdrew $" +  withdrawal + " from the account.");
  }
}
