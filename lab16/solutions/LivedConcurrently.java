package lab16.solutions;

/**
 * Figures out whether two people lived at the same time or not.
 */
public class LivedConcurrently {
  public static void main(String[] args) {
    Range firstPersonsLife =
      new Range(Integer.parseInt(args[0]), Integer.parseInt(args[1]));
    Range secondPersonsLife =
      new Range(Integer.parseInt(args[2]), Integer.parseInt(args[3]));

    if (firstPersonsLife.overlaps(secondPersonsLife)) {
      System.out.println("They lived at the same time.");
    } else {
      System.out.println("They never could have known each other.");
    }


    // What if this first person lived for another 20 years?
    firstPersonsLife.extendForward(20);
    System.out.print("If the first person had lived another 20 years, then ");
    if (firstPersonsLife.overlaps(secondPersonsLife)) {
      System.out.println("they would have lived at the same time.");
    } else {
      System.out.println("they still never could have known each other.");
    }
  }
}
