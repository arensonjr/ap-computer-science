package lab16.solutions;

/**
 * Represents a person's bank account.
 */
public class BankAccount {
  // Amount of money in the account
  private double balance;

  // Password required to withdraw money
  private String password;

  public BankAccount(double startingBalance, String p) {
    balance = startingBalance;
    password = p;
  }

  /**
   * Returns the current amount of money in the account.
   */
  public double getBalance() {
    return balance;
  }

  /**
   * Deposits money into the account.
   */
  public void deposit(double additionalFunds) {
    // You can't deposit negative money!
    if (additionalFunds < 0) {
      additionalFunds = 0;
    }

    balance += additionalFunds;
  }

  /**
   * Withdraws money from the account.
   *
   * Only works if the password is correct; if the password is incorrect,
   * returns $0 and doesn't change the account's balance.
   */
  public double withdraw(double amount, String passwordAttempt) {
    if (password.equals(passwordAttempt)) {
      // They can't withdraw more money than we have in the account
      if (amount > balance) {
        amount = balance;
      }

      balance -= amount;
      return amount;
    } else {
      // They got the password wrong! No money for them.
      return 0;
    }
  }
}
