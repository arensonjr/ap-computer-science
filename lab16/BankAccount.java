package lab16;

/**
 * Represents a person's bank account.
 */
public class BankAccount {
  // Amount of money in the account
  double balance;

  // Password required to withdraw money
  String password;

  public BankAccount(double startingBalance, String p) {
    balance = startingBalance;
    password = p;
  }

  /**
   * Returns the current amount of money in the account.
   */
  public double getBalance() {
    return balance;
  }

  /**
   * Deposits money into the account.
   */
  public void deposit(double additionalFunds) {
    balance += additionalFunds;
  }

  /**
   * Withdraws money from the account.
   *
   * Only works if the password is correct; if the password is incorrect,
   * returns $0 and doesn't change the account's balance.
   */
  public double withdraw(double amount, String passwordAttempt) {
    balance -= amount;
    return amount;
  }
}

