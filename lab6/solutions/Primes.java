package lab6.solutions;

/**
 * Generates prime numbers.
 */
class Primes {
  public static void main(String[] args) {
    int numPrimes = Integer.parseInt(args[0]);

    // Keep searching until we've found numPrimes primes.
    int numPrimesFound = 0;
    int number = 2;
    while (numPrimesFound < numPrimes) {
      // Check if this number is prime; if so, count it and print it.
      if (isPrime(number)) {
        System.out.println(number);
        numPrimesFound++;
      }

      // Either way, move on to the next number
      number++;
    }
  }

  /**
   * Returns whether or not number is prime.
   */
  public static boolean isPrime(int number) {
    // A number is prime if and only if its only divisors are 1 and itself.
    for (int divisor = 2; divisor < number; divisor++) {
      if (number % divisor == 0) {
        // Uh oh, we've found a factor of number -- it can't be prime
        return false;
      }
    }

    // We didn't find any factors of number, so it must be prime
    return true;
  }
}
