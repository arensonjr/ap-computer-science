package lab6.solutions;

class MultiplicationTable {
  public static void main(String[] args) {
    // Get table dimensions
    int dimensions = Integer.parseInt(args[0]);

    for (int i = 1; i <= dimensions; i++) {
      System.out.println(buildProducts(dimensions, i));
    }
  }

  /**
   * Builds the products of the numbers from 1 to size, each multiplied by
   * factor. For example:
   *
   * "  1    2   ...   35  ...  123 "
   *
   * @param size number of integers to include in the row
   * @param factor number to multiply each integer by
   */
  public static String buildProducts(int size, int factor) {
    String row = "";

    // Add each number in turn
    for (int i = 1; i <= size; i++) {
      row += " " + numberToString(i * factor) + " ";
    }
    return row;
  }

  /**
   * Pads the input number with spaces so that it's exactly 3 characters wide.
   */
  public static String numberToString(int number) {
    // If it's a one-digit number, center it
    if (number < 10) {
      return " " + number + " ";
    }

    // If it's a two digit number, right-align it
    else if (number < 100) {
      return " " + number;
    }

    // If it's a three digit number, it takes up all of the spaces, so we just
    // need to convert it to a string
    else {
      return "" + number;
    }
  }
}
