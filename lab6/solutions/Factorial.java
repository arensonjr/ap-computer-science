package lab6.solutions;

class Factorial {
  public static void main(String[] args) {
    // Get & check input
    int number = Integer.parseInt(args[0]);
    if (number <= 0) {
      System.out.println("Input number must be positive");
      return;
    }

    // Compute factorial
    int product = 1;
    for (int i = 1; i <= number; i++) {
      product *= i;
    }

    System.out.println(number + "! = " + product);
  }
}
