package lab6.solutions;

/**
 * Pretty prints some asterisks in a pleasant pattern.
 *
 * Since this is a challenge problem, we'll give you a challenging and
 * high-quality solution file, which uses some tricks and tips that we don't
 * expect you to have found or learned on your own.
 */
class PrettyPrintPart4V2 {
  public static void main(String[] args) {
    int sideLength = Integer.parseInt(args[0]);
    int numDiamonds = Integer.parseInt(args[1]);

    for (int diamond = 0; diamond < numDiamonds; diamond++) {
      printDiamond(sideLength);
    }
  }

  /**
   * Pretty prints a single diamond.
   */
  public static void printDiamond(int sideLength) {
    // Build the "down-facing" triangle
    String triangle = "";
    for (int row = 1; row <= sideLength; row++) {
      String leftHalf = "";
      for (int column = 1; column <= sideLength; column++) {
        // If we're left of the diagonal, add a space; otherwise, add an
        // asterisk.
        //
        // This is a programming feature that we haven't seen in class yet.
        // It's just "syntactic sugar" for a common programming pattern. The code
        //
        //     int foo = A ? B : C
        //
        // Is exactly identical to
        //
        //     int foo;
        //     if (A) {
        //         foo = B;
        //     } else {
        //         foo = C;
        //     }
        //
        // This doesn't need to be in the context of assigning to a variable;
        // for instance, I'm using it here to decide which string to add to the
        // row.
        leftHalf += (column < row) ? " " : "*";
      }

      // The right half is the mirror of the left half, so we only have to
      // build it once.
      //
      // We also add the newline character, '\n', so that we can put multiple
      // lines of asterisks into the same string. This is necessary because we
      // want to reverse the whole triangle later!
      triangle += leftHalf + reverse(leftHalf) + "\n";
    }

    // The "up-facing" triangle is just the opposite of the "down-facing"
    // triangle
    System.out.println(reverse(triangle));
    System.out.println(triangle);
  }

  /**
   * Reverses a string.
   */
  public static String reverse(String inputString) {
    // We're going to cheat, and use a helper utility to reverse some strings!
    //
    // We will cover this information in class over the next couple weeks, and
    // show you more examples of common libraries, functions, and classes that
    // can help you perform common tasks like this.
    return new StringBuilder(inputString).reverse().toString();
  }
}
