package lab6.solutions;

class MultiplicationTableBonus {
  public static void main(String[] args) {
    // Get table dimensions
    int dimensions = Integer.parseInt(args[0]);

    System.out.println(buildHeaderRow(dimensions));
    for (int i = 1; i <= dimensions; i++) {
      System.out.println(buildRowBoundary(dimensions));
      System.out.println(buildProductRow(dimensions, i));
    }
  }

  /**
   * Constructs the header row, with a blank space on the far left and
   * ascending digits on the right.
   *
   * @param size number of integers to include in the row
   */
  public static String buildHeaderRow(int size) {
    // Start with the empty cell
    String row = "     ";

    // The other digits are the same as multiplying all of the digits by 1
    row += buildProducts(size, 1);

    return row;
  }

  /**
   * Builds a complete row of products, starting with the factor that this row
   * is multiplying by.
   *
   * @param size number of integers to include in the row
   * @param factor factor that all of the integers in this row will be multiplied by
   */
  public static String buildProductRow(int size, int factor) {
    // Start by printing the current factor we're multiplying
    String row = " " + numberToString(factor) + " ";

    // Add the products for the rest of the row
    row += buildProducts(size, factor);

    return row;
  }

  /**
   * Builds the products of the numbers from 1 to size, each multiplied by
   * factor. For example:
   *
   * "|  1  |  2  | ... |  35 | ... | 123 "
   *
   * @param size number of integers to include in the row
   * @param factor number to multiply each integer by
   */
  public static String buildProducts(int size, int factor) {
    String row = "";

    // Add each number in turn
    for (int i = 1; i <= size; i++) {
      row += "| " + numberToString(i * factor) + " ";
    }
    return row;
  }

  /**
   * Pads the input number with spaces so that it's exactly 3 characters wide.
   */
  public static String numberToString(int number) {
    // If it's a one-digit number, center it
    if (number < 10) {
      return " " + number + " ";
    }

    // If it's a two digit number, right-align it
    else if (number < 100) {
      return " " + number;
    }

    // If it's a three digit number, it takes up all of the spaces, so we just
    // need to convert it to a string
    else {
      return "" + number;
    }
  }

  /**
   * Constructs the boundary between table rows, e.g.:
   *
   * "-----+-----+-----+-----+-----+-----+-----+-----+-----"
   */
  public static String buildRowBoundary(int size) {
    // Define "one cell's width" of dashes
    String cell = "-----";

    // Start with the edge on the left, where we put the current index
    String row = cell;

    // Add one column for each integer
    for (int i = 0; i < size; i++) {
      row += "+" + cell;
    }

    return row;
  }
}
