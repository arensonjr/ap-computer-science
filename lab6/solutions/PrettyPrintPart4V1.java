package lab6.solutions;

/**
 * Pretty prints some asterisks in a pleasant pattern.
 *
 * Since this is a challenge problem, we'll give you a challenging and
 * high-quality solution file, which uses some tricks and tips that we don't
 * expect you to have found or learned on your own.
 */
class PrettyPrintPart4V1 {
  public static void main(String[] args) {
    int sideLength = Integer.parseInt(args[0]);
    int numDiamonds = Integer.parseInt(args[1]);

    for (int diamondIndex = 0; diamondIndex < numDiamonds; diamondIndex++) {
      printDiamond(sideLength);
    }
  }

  /**
   * Pretty prints a single diamond.
   */
  public static void printDiamond(int sideLength) {
    // Build the "upward-facing" half
    for (int row = 1; row <= sideLength; row++) {
      printDiamondRow(sideLength, row);
    }

    // Build the "downward-facing" half
    for (int row = sideLength; row > 0; row--) {
      printDiamondRow(sideLength, row);
    }
  }

  /**
   * Prints one row of a diamond of asterisks.
   *
   * @param sideLength the total side length of the diamond
   * @param row which row we're printing (anything from 1 to sideLength, where
   *     1 is the shortest row)
   */
  public static void printDiamondRow(int sideLength, int row) {
    // Each row is build up of (sideLength - row) spaces and (row * 2)
    // asterisks
    String line = "";
    for (int space = 0; space < (sideLength - row); space++) {
      line += " ";
    }
    for (int asterisk = 0; asterisk < (row * 2); asterisk++) {
      line += "*";
    }
    System.out.println(line);
  }
}
