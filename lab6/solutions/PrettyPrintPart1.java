package lab6.solutions;

/**
 * Pretty prints some asterisks in a pleasant pattern.
 */
class PrettyPrintPart1 {
  public static void main(String[] args) {
    int sideLength = Integer.parseInt(args[0]);

    // Loop for sideLength rows
    for (int i = 1; i <= sideLength; i++) {
      // It's a triangle, so we need to reach the diagonal, which is at (i, i).
      // The way to do this is to print i asterisks.
      printRow(i);
    }
  }

  /**
   * Prints a row of len asterisks.
   */
  public static void printRow(int len) {
    String row = "";
    for (int i = 0; i < len; i++) {
      row += "*";
    }
    System.out.println(row);
  }
}
