package lab6.solutions;

/**
 * Pretty prints some asterisks in a pleasant pattern.
 */
class PrettyPrintPart3 {
  public static void main(String[] args) {
    int sideLength = Integer.parseInt(args[0]);
    int numTriangles = Integer.parseInt(args[1]);

    for (int triangleIndex = 0; triangleIndex < numTriangles; triangleIndex++) {
      // Print the triangle right-side-up

      // Loop for sideLength rows
      for (int i = 1; i <= sideLength; i++) {
        // It's a triangle, so we need to reach the diagonal, which is at (i, i).
        // The way to do this is to print i asterisks.
        for (int j = 1; j <= i; j++) {
          System.out.print("*");
        }
        // End of the row -- add a newline
        System.out.println();
      }

      // Print the triangle upside-down

      // Loop for sideLength rows
      for (int i = sideLength; i > 0; i--) {
        // It's a triangle, so we need to reach the diagonal, which is at (i, i).
        // The way to do this is to print i asterisks.
        for (int j = 1; j <= i; j++) {
          System.out.print("*");
        }
        // End of the row -- add a newline
        System.out.println();
      }
    }
  }
}
