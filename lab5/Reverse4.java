package lab5;

public class Reverse4 {
  public static void main(String[] args) {
    int[] a = { 6, 22, -8, 0 };
    int[] b = { 123, -5, 456, -55 };
    int[] c = { 1, 1, 3, 1 };

    System.out.println("a:");
    // This function is provided; to see how it works, check out the
    // code in lab5/PrintArray.java
    PrintArray.ofIntegers(a);
    System.out.println("b:");
    PrintArray.ofIntegers(b);
    System.out.println("c:");
    PrintArray.ofIntegers(c);

    // Write your code here.
    int[] a2;
    int[] b2;
    int[] c2;
  }
}
