package lab5.solutions;

import lab4.solutions.ParseInput;

public class Arithmetic {
  public static void main(String[] args) {
    // Remember this lab problem from week 4? If not, go take another look to
    // remind yourself what it does.
    int[] input = ParseInput.asIntegers(args);

    System.out.println("Sum: " + add(input));
    System.out.println("Product: " + multiply(input));
    System.out.println("Sum of squares: " + sumOfSquares(input));
  }

  /**
   * Returns the sum of all of the elements of the input array.
   */
  public static int add(int[] numbers) {
    int total = 0;
    for (int number : numbers) {
      total += number;
    }
    return total;
  }

  /**
   * Returns the product of all of the elements of the input array.
   */
  public static int multiply(int[] numbers) {
    int total = 1;
    for (int number : numbers) {
      total *= number;
    }
    return total;
  }

  /**
   * Returns the sum of squares of all of the elements of the input array.
   */
  public static int sumOfSquares(int[] numbers) {
    int total = 0;
    for (int number : numbers) {
      total += (number * number);
    }
    return total;
  }
}

