package lab5.solutions;

/**
 * Helper functions to print arrays.
 */
public class PrintArray {
  /**
   * Prints an array of integers.
   */
  public static void ofIntegers(int[] array) {
    // Begin the array with a '['
    System.out.print("[");

    // Print each integer in the array.
    //
    // We want to know the indices so that we can print ", " in between each
    // element, but *not* print it after the very last number in the array --
    // otherwise, the output would look like "[3, 4, 5, ]" which just looks
    // silly.
    int i = 0;
    while (i < array.length) {
      System.out.print(array[i]);

      // Don't print ", " after the last number!
      if (i < array.length - 1) {
        System.out.print(", ");
      }

      i++;
    }

    // All of the numbers have been printed; end the array with a ']'
    System.out.println("]");
  }
}

