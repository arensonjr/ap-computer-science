package lab5.solutions;

public class Reverse4 {
  public static void main(String[] args) {
    int[] a = { 6, 22, -8, 0 };
    int[] b = { 123, -5, 456, -55 };
    int[] c = { 1, 1, 3, 1 };

    System.out.println("a:");
    PrintArray.ofIntegers(a);
    System.out.println("b:");
    PrintArray.ofIntegers(b);
    System.out.println("c:");
    PrintArray.ofIntegers(c);

    System.out.println("Reverse of a:");
    PrintArray.ofIntegers(reverseLengthFour(a));
    System.out.println("Reverse of b:");
    PrintArray.ofIntegers(reverseLengthFour(b));
    System.out.println("Reverse of c:");
    PrintArray.ofIntegers(reverseLengthFour(c));

    System.out.println("Another Reverse of a:");
    PrintArray.ofIntegers(reverseLengthFourAlternate(a));
    System.out.println("Another Reverse of b:");
    PrintArray.ofIntegers(reverseLengthFourAlternate(b));
    System.out.println("Another Reverse of c:");
    PrintArray.ofIntegers(reverseLengthFourAlternate(c));
  }

  /**
   * Creates a reversed copy of a length-four array.
   */
  public static int[] reverseLengthFour(int[] array) {
    int[] reversed = new int[4];
    reversed[0] = array[3];
    reversed[1] = array[2];
    reversed[2] = array[1];
    reversed[3] = array[0];
    return reversed;
  }

  /**
   * Creates a reversed copy of a length-four array.
   */
  public static int[] reverseLengthFourAlternate(int[] array) {
    int[] reversed = { array[3], array[2], array[1], array[0] };
    return reversed;
  }
}
