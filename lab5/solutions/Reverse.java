package lab5.solutions;

public class Reverse {
  public static void main(String[] args) {
    int[] a = { -8, 0 };
    int[] b = { 123, -5, 456, -55 };
    int[] c = { 1, 3, 1, 1, 3, 1, 1, 1, 3 };
    int[] d = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 };

    System.out.println("a:");
    PrintArray.ofIntegers(a);
    System.out.println("b:");
    PrintArray.ofIntegers(b);
    System.out.println("c:");
    PrintArray.ofIntegers(c);
    System.out.println("d:");
    PrintArray.ofIntegers(d);

    System.out.println("Reverse of a:");
    PrintArray.ofIntegers(reverse(a));
    System.out.println("Reverse of b:");
    PrintArray.ofIntegers(reverse(b));
    System.out.println("Reverse of c:");
    PrintArray.ofIntegers(reverse(c));
    System.out.println("Reverse of d:");
    PrintArray.ofIntegers(reverse(d));
  }

  /**
   * Reverses an array.
   */
  public static int[] reverse(int[] array) {
    int len = array.length;
    int[] reversed = new int[len];

    int i = 0;
    while (i < len) {
      // If len is 5, we want to match up positions like:
      //   0 <---> 4
      //   1 <---> 3
      //   2 <---> 2
      // So we figure out the following formula:
      //   4 = 5 - 0 - 1
      //   3 = 5 - 1 - 1
      //   2 = 5 - 2 - 1
      // It looks like we need 'len - i - 1'.
      reversed[i] = array[len - i - 1];
    }

    return reversed;
  }
}
