package lab5.solutions;

public class ReverseInPlace {
  public static void main(String[] args) {
    int[] a = { -8, 0 };
    int[] b = { 123, -5, 456, -55 };
    int[] c = { 1, 3, 1, 1, 3, 1, 1, 1, 3 };
    int[] d = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 };

    System.out.println("a:");
    PrintArray.ofIntegers(a);
    System.out.println("b:");
    PrintArray.ofIntegers(b);
    System.out.println("c:");
    PrintArray.ofIntegers(c);
    System.out.println("d:");
    PrintArray.ofIntegers(d);

    System.out.println("Reverse of a:");
    reverse(a);
    PrintArray.ofIntegers(a);
    System.out.println("Reverse of b:");
    reverse(b);
    PrintArray.ofIntegers(b);
    System.out.println("Reverse of c:");
    reverse(c);
    PrintArray.ofIntegers(c);
    System.out.println("Reverse of d:");
    reverse(d);
    PrintArray.ofIntegers(d);
  }

  /**
   * Reverses an array in-place (by modifying it).
   */
  public static void reverse(int[] array) {
    // We want to step through and swap the things on the left and the right,
    // so we need to keep track of both the left and the right.
    int left = 0;
    int right = array.length - 1;
    while (left < right) {
      // Swap the numbers at index left and index right.
      //
      // Note that we have to be careful here -- if I assign array[left] to
      // array[right], then I've lost the number that used to be at
      // array[left]! I need to stick it into a temporary variable, so that my
      // program doesn't forget what it was before I have a chance to store it
      // back at array[right].
      int temp = array[left];
      array[left] = array[right];
      array[right] = temp;

      // Advance by one element on both sides
      left++;
      right--;
    }
  }
}
