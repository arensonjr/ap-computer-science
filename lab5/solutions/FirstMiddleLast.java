package lab5.solutions;

public class FirstMiddleLast {
  public static void main(String[] args) {
    String[] a = { "one", "two", "three" };
    String[] b = { "first", "blah", "foo", "middle", "bar", "quux", "last" };
    String[] c = { "first, and middle, and last" };

    System.out.println("First element of a:  " + a[0]);
    System.out.println("Middle element of a: " + a[1]);
    System.out.println("Last element of a:   " + a[2]);

    System.out.println("First element of b:  " + b[0]);
    System.out.println("Middle element of b: " + b[3]);
    System.out.println("Last element of b:   " + b[6]);

    System.out.println("First element of c:  " + c[0]);
    System.out.println("Middle element of c: " + c[0]);
    System.out.println("Last element of c:   " + c[0]);

    System.out.println("\n\n===== CHALLENGE SECTION =====\n");

    System.out.println("For a:");
    challengePrint(a);
    System.out.println("For b:");
    challengePrint(b);
    System.out.println("For c:");
    challengePrint(c);
  }

  /**
   * Prints out the first, middle, and last elements of the array
   */
  public static void challengePrint(String[] array) {
    System.out.println("First:  " + array[0]);
    System.out.println("Middle: " + array[array.length / 2]);
    System.out.println("Last:   " + array[array.length - 1]);
  }
}

