package lab5;

public class ReverseInPlace {
  public static void main(String[] args) {
    int[] a = { -8, 0 };
    int[] b = { 123, -5, 456, -55 };
    int[] c = { 1, 3, 1, 1, 3, 1, 1, 1, 3 };
    int[] d = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 };

    System.out.println("a:");
    PrintArray.ofIntegers(a);
    System.out.println("b:");
    PrintArray.ofIntegers(b);
    System.out.println("c:");
    PrintArray.ofIntegers(c);
    System.out.println("d:");
    PrintArray.ofIntegers(d);

    // Write your code here.
  }
}
