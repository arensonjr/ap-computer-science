package lab5;

import lab4.solutions.ParseInput;

public class Arithmetic {
  public static void main(String[] args) {
    // Remember this lab problem from week 4? If not, go take another look to
    // remind yourself what it does.
    int[] input = ParseInput.asIntegers(args);

    // Write your code here
  }

  /**
   * Returns the sum of all of the elements of the input array.
   */
  public static int add(int[] numbers) {
    // Replace the following line with your code.
    return 0;
  }

  /**
   * Returns the product of all of the elements of the input array.
   */
  public static int multiply(int[] numbers) {
    // Replace the following line with your code.
    return 1;
  }

  /**
   * Returns the sum of squares of all of the elements of the input array.
   */
  public static int sumOfSquares(int[] numbers) {
    // Replace the following line with your code.
    return 0;
  }
}
