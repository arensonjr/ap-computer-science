package assignment8;

import java.util.ArrayList;
import java.util.HashMap;

import provided.Maps;

/**
 * Generates Markov Models for text corpora.
 */
public class MarkovModel {
  /**
   * Generates a first-order Markov Model for the provided text corpus.
   *
   * This model should be a map from each word in the input corpus to a list of
   * all of the other words that follow it (including duplicates). For
   * instance, the sentence "I like you and you like you and you like me"
   * should create the following model:
   *
   *     {
   *       I: [like]
   *       like: [you, you, me]
   *       you: [and, like, and, like]
   *       and: [you, you]
   *     }
   */
  public static HashMap<String, ArrayList<String>> generateFirstOrderMarkovModel(String corpusName) {
    // +---------------------------------------------+
    // | Replace the following lines with your code. |
    // +---------------------------------------------+
    return new HashMap<>();
  }

  /**
   * Reads input, calls other functions, and prints out the generated Markov Model.
   *
   * DO NOT EDIT THIS CODE! It is just here to help you visualize the results
   * of generating your Markov Model. The only code you need to edit is above,
   * in generateFirstOrderMarkovModel().
   */
  public static void main(String[] args) {
    // DO NOT EDIT CODE BELOW THIS LINE
    String corpus = args[0];
    System.out.println("Building a Markov Model from corpus '" + corpus + "'");

    HashMap<String, ArrayList<String>> firstOrderMarkovModel =
      generateFirstOrderMarkovModel(corpus);

    System.out.println("First order Markov Model:");
    Maps.prettyPrint(firstOrderMarkovModel);
    // DO NOT EDIT CODE ABOVE THIS LINE
  }
}
