package assignment8.solutions;

import java.util.ArrayList;
import java.util.HashMap;

import provided.Maps;

/**
 * Generates Markov Models for text corpora.
 */
public class MarkovModel {
  /**
   * Generates a first-order Markov Model for the provided text corpus.
   *
   * This model should be a map from each word in the input corpus to a list of
   * all of the other words that follow it (including duplicates). For
   * instance, the sentence "I like you and you like you and you like me"
   * should create the following model:
   *
   *     {
   *       I: [like]
   *       like: [you, you, me]
   *       you: [and, like, and, like]
   *       and: [you, you]
   *     }
   */
  public static HashMap<String, ArrayList<String>> generateFirstOrderMarkovModel(String corpusName) {
    // Get all the words from the corpus
    ArrayList<String> words = Provided.getWords(corpusName);

    HashMap<String, ArrayList<String>> markovChain = new HashMap<>();

    // Careful -- only iterate until the second-to-last word, because the last
    // word doesn't have a successor!
    for (int i = 0; i < words.size() - 1; i++) {
      String word = words.get(i);

      // Fetch the successors of the current word. If we haven't seen any yet,
      // create a new list for them.
      if (!markovChain.containsKey(word)) {
        markovChain.put(word, new ArrayList<String>());
      }
      ArrayList<String> successors = markovChain.get(word);

      // Add the *next* word as a successor to the current word
      successors.add(words.get(i + 1));
    }

    return markovChain;
  }

  /**
   * Reads input, calls other functions, and prints out the generated Markov Model.
   *
   * DO NOT EDIT THIS CODE! It is just here to help you visualize the results
   * of generating your Markov Model. The only code you need to edit is above,
   * in generateFirstOrderMarkovModel().
   */
  public static void main(String[] args) {
    // DO NOT EDIT CODE BELOW THIS LINE
    String corpus = args[0];
    System.out.println("Building a Markov Model from corpus '" + corpus + "'");

    HashMap<String, ArrayList<String>> firstOrderMarkovModel =
      generateFirstOrderMarkovModel(corpus);

    System.out.println("First order Markov Model:");
    Maps.prettyPrint(firstOrderMarkovModel);
    // DO NOT EDIT CODE ABOVE THIS LINE
  }
}
