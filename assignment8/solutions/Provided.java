package assignment8.solutions;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Provided code for Assignment 7 (generated speech).
 */
class Provided {
  /**
   * Returns all of the words from a corpus, in order.
   */
  public static ArrayList<String> getWords(String corpusName) {
    String corpus;
    try {
      Path corpusFile = FileSystems.getDefault()
            .getPath("assignment8", "corpora", corpusName + ".txt");
      corpus = new String(Files.readAllBytes(corpusFile));
    } catch (IOException e) {
      throw new IllegalArgumentException("Could not find corpus " + corpusName, e);
    }

    // Split on any space character.
    String[] words = corpus.split("\\s+");

    // We have to do some gymnastics to convert it into an ArrayList; this is
    // why this part is provided code!
    return new ArrayList<String>(Arrays.asList(words));
  }
}

