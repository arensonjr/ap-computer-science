package lab17.solutions;

/**
 * Prints the greatest common denominator of two input integers.
 */
public class Gcd {
  public static void main(String[] args) {
    int numOne = Integer.parseInt(args[0]);
    int numTwo = Integer.parseInt(args[1]);
    int gcd = greatestCommonDenominator(numOne, numTwo);
    System.out.println(gcd);
  }

  /**
   * Returns the greatest common denominator of the two integers.
   */
  public static int greatestCommonDenominator(int a, int b) {
    // Make it so that a is always the larger number
    if (b > a) {
      // Swap a and b
      int tmp = a;
      a = b;
      b = tmp;
    }

    // We'll use Euclid's algorithm (from Lecture 6B) to find the GCD.
    int remainder = a % b;
    while (remainder > 0) {
      a = b;
      b = remainder;
      remainder = a % b;
    }
    return b;
  }
}
