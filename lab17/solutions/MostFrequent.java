package lab17.solutions;

import java.util.HashMap;

/**
 * Prints the most frequently occurring input arg.
 */
public class MostFrequent {
  public static void main(String[] args) {
    HashMap<String, Integer> wordCounts = countWords(args);
    String mostFrequent = getBestKey(wordCounts);
    System.out.println(mostFrequent);
  }

  /**
   * Returns a map from each unique string to the number of times it occurred.
   */
  public static HashMap<String, Integer> countWords(String[] words) {
    HashMap<String, Integer> counts = new HashMap<>();
    for (int i = 0; i < words.length; i++) {
      String word = words[i];
      if (!counts.containsKey(word)) {
        counts.put(word, 1);
      } else {
        int oldCount = counts.get(word);
        counts.put(word, oldCount + 1);
      }
    }
    return counts;
  }

  /**
   * Returns the key with the greatest value.
   */
  public static String getBestKey(HashMap<String, Integer> countMap) {
    int maxValue = 0;
    String bestKey = "";
    for (String key : countMap.keySet()) {
      int value = countMap.get(key);
      if (value > maxValue) {
        maxValue = value;
        bestKey = key;
      }
    }
    return bestKey;
  }
}
