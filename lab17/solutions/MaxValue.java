package lab17.solutions;

import lab4.solutions.ParseInput;

/**
 * Prints the maximum of the input integers.
 */
public class MaxValue {
  public static void main(String[] args) {
    int[] inputs = ParseInput.asIntegers(args);
    int max = maxValue(inputs);
    System.out.println(max);
  }

  /**
   * Returns the maximum value of an array.
   */
  public static int maxValue(int[] array) {
    // Starting value for max; we'll replace it as soon as we get
    // into the loop.
    int max = array[0];
    for (int num : array) {
      if (num > max) {
        max = num;
      }
    }
    return max;
  }
}
