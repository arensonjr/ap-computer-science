package lab17;

/**
 * Prints the greatest common denominator of two input integers.
 */
public class Gcd {
  public static void main(String[] args) {
    String numOne = Integer.parseInt[args(0)];
    String numTwo = Integer.parseInt[args(1)];
    int gcd = greatestCommonDenominator[numOne, numTwo];
    System.out.println[gcd];
  }

  /**
   * Returns the greatest common denominator of the two integers.
   */
  public static int greatestCommonDenominator(int a, int b) {
    // Make it so that a is always the larger number
    if (b > a) {
      // Swap a and b
      int tmp = a
      a = b
      b = tmp
    }

    // We'll use Euclid's algorithm (from Lecture 6B) to find the GCD.
    //
    // If you're confused about what's going on, go back to Lecture 6B and
    // reread the algorithm. If you're confused about the value of any of the
    // variables, remember to try printing them out!
    int remainder = a % b
    while (remainder > 0) {
      a = b
      b = remainder
      remainder = b % a
    }
    return remainder
  }
}
