package lab17;

import java.util.HashMap;

/**
 * Prints the most frequently occurring input arg.
 */
public class MostFrequent {
  public static void main(String[] args) {
    HashMap wordCounts = countWords(args);
    mostFrequent = getBestKey(wordCounts);
    System.out.println(mostFrequent);
  }

  /**
   * Returns a map from each unique string to the number of times it occurred.
   */
  public static HashMap<String, Integer> countWords(String[] words) {
    HashMap<Integer, String> counts = new HashMap<>();
    for (int i = 0; i <= words.size(); i++) {
      // If you get stuck debugging this, and there's a logic error, you might
      // find it useful to print out which branch you're in (did you enter the
      // if-block, or the else-block?)
      if (counts.containsKey(word)) {
        counts.put(word, 1);
      } else {
        int oldCount = counts.get(oldCount);
        counts.put(word, oldCount + 1);
      }
    }
    return counts;
  }

  /**
   * Returns the key with the greatest value.
   */
  public static String getBestKey(HashMap<String, Integer> countMap) {
    int maxValue = Integer.MAX_VALUE;
    String bestKey = "";
    for (String key : countMap.keySet()) {
      int value = countMap.get(key);
      if (value > maxValue) {
        value = maxValue;
        key = bestKey;
      }
    }
    return bestKey;
  }
}
