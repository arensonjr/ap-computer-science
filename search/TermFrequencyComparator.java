package search;

/**
 * Scores pages by how often the term appears in each webpage.
 *
 * More occurences scores higher.
 */
public class TermFrequencyComparator extends WebPageComparator {

  // Don't implement compare() in this class -- only implement score.
  public Integer score(WebPage page) {
    return 0;
  }

}
