package search.solutions;

/**
 * Scores pages by how often the term appears in each webpage.
 *
 * More occurences scores higher.
 */
public class TermFrequencyComparator extends WebPageComparator {
  public Integer score(WebPage page) {
    int count = 0;
    for (String word : page.getWords()) {
      if (word.equals(term)) {
        count++;
      }
    }
    return count;
  }
}
