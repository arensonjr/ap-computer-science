package search.solutions;

/**
 * Sorts pages alphabetically by their URLs.
 */
public class AlphabeticalUrlComparator extends WebPageComparator {
  /**
   * We never use the score, because we've overriden compare() instead.
   */
  public Integer score(WebPage page) {
    return 0;
  }

  /**
   * Compares two WebPages alphabetically by URL.
   *
   * Since the SearchEngine sorts in decreasing order, we flip the direction
   * of the comparison. That way, we reverse the decreasing order,
   * to end up with plain old alphabetical sort.
   */
  public int compare(WebPage page1, WebPage page2) {
    String page1Url = page1.getUrl().toString();
    String page2Url = page2.getUrl().toString();

    return page2Url.compareTo(page1Url); // Flip the order of the comparison
  }
}
