package search.solutions;

import java.io.Console;
import java.io.IOException;

import java.net.URL;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import java.util.ArrayList;
import java.util.List;


/**
 * This class wraps the SearchEngine object into a web server.
 */
public class SearchEngineServer {

  /**
   * Directory containing files which specify lists of URLs to explore.
   */
  private static final Path URL_LISTS_DIR =
      Paths.get("search", "static", "url_lists");

  private static WebPageComparator termFrequencyComparator =
      new TermFrequencyComparator();

  private static WebPageComparator firstIsBestComparator =
      new FirstIsBestComparator();

  /**
   * Read a text file in URL_LISTS_DIR, returning the contents as a List<URL>
   *
   * If filename can't be read, this function returns an empty list.
   */
  public static List<URL> readUrls(String filename) {
    System.out.format("Reading URLs from [%s]...\n", filename);

    List<URL> urls = new ArrayList<>();
    try {
      Path file = URL_LISTS_DIR.resolve(filename);
      for (String line: Files.readAllLines(file, StandardCharsets.UTF_8)) {
        urls.add(new URL(line));
      }
      System.out.format("Read %d URLs.\n", urls.size());
      return urls;
    } catch (IOException e) {
      System.err.format("Failed to read file:\n", filename);
      System.err.println(e);
    }
    return urls;
  }

  public static void main(String[] args) {
    if (args.length == 0) {
      System.err.println("Please specify a URL list.");
      System.exit(1);
    }

    // Set up the Search Engine
    List<URL> urls = readUrls(args[0]);
    Index index = Index.from(urls);
    WebPageComparator comparator = termFrequencyComparator;
    SearchEngine searchEngine = SearchEngine.from(index, comparator);

    // Prep to get queries from the command line
    // (console the System, it is doing a good job)
    Console console = System.console();

    // Introduction to the user.
    System.out.println("-----------------------------------------------");
    System.out.println("Welcome to WackySearch! (type \"quit\" to exit)");
    System.out.println("-----------------------------------------------");

    // Go into the main query loop
    String query = console.readLine("WackySearch > ");
    while (!query.equals("quit")) {
      for (WebPage result: searchEngine.search(query)) {
        System.out.printf(
            "%s (%d)\n", result.getUrl(), comparator.score(result));
      }

      query = console.readLine("WackySearch > ");
    }
  }

}