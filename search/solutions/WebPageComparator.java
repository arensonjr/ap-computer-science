package search.solutions;

import java.util.Comparator;

public abstract class WebPageComparator implements Comparator<WebPage> {

  String term;

  /**
   * Compute a score for this page. The default compare uses this.
   *
   * You will need to implement this method for your comparator to work!
   */
  public abstract Integer score(WebPage page);

  /**
   * Default compare method, which calls the score() method above on the two
   * pages and compares the two numbers.
   *
   * You do not need to implement this if you implement score()!
   * However, if you decide that you want to write your own compare, you can.
   *
   * Note that you still have to write a score() method if you override this.
   */
  @Override
  public int compare(WebPage page1, WebPage page2) {
    // Sort from largest to smallest score
    return Integer.compare(score(page1), score(page2));
  }

  /**
   * The Search Engine will use this to inform your comparator of the term
   * currently being searched.
   *
   * For example, if someone searches for "kittens", then the SearchEngine will
   * call yourComparator.setTerm("kittens"), so that you could compare based on
   * that term.
   */
  public void setTerm(String newTerm) {
    term = newTerm;
  }
}
