package search.solutions;

/**
 * Scores pages by how early the term appears. Earlier occurrences score higher.
 */
public class FirstIsBestComparator extends WebPageComparator {

  private static final int MAX_SCORE = 10000;

  public Integer score(WebPage page) {
    // We want pages where the word appears first to score higher, but if we
    // just return its index, the score will be really low.
    //
    // Instead, we can think of it as _subtracting_ points based on how long
    // it takes us to reach the word -- "minus one point" for every word we had
    // to read before we found it.
    return MAX_SCORE - page.getWords().indexOf(term);
  }
}
