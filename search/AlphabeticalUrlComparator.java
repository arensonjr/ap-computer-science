package search;

/**
 * Compares pages alphabetically by their URLs.
 */
public class AlphabeticalUrlComparator extends WebPageComparator {

  /**
   * Compares two WebPages alphabetically by URL.
   */
  public int compare(WebPage page1, WebPage page2) {
    // Implement this! Don't just return 0!
    return 0;
  }

  /**
   * Since we've overriden compare(), we don't need to compute anything here.
   * i.e. don't change this method.
   */
  public Integer score(WebPage page) {
    return 0; // This is done.
  }

}
