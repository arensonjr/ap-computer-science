package search;

/**
 * Scores pages by how early the term appears. Earlier occurrences score higher.
 */
public class FirstIsBestComparator extends WebPageComparator {

  // Don't implement compare() in this class -- only implement score.
  public Integer score(WebPage page) {
    return 0;
  }

}
