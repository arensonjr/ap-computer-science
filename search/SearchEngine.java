package search;

import java.util.Collections;
import java.util.List;

public class SearchEngine {

  private Index index;
  private WebPageComparator comparator;
  private boolean printRankingInformation;

  public SearchEngine(Index i, WebPageComparator c) {
    index = i;
    comparator = c;
    printRankingInformation = false;
  }

  /**
   * This is the magic method.
   */
  public List<WebPage> search(String query) {
    // First, sanitize the input search term -- maybe it's been done already,
    // but just to be safe.
    String sanitizedQuery = sanitizeQuery(query);

    // Look up the term in the index
    List<WebPage> results = index.get(sanitizedQuery);
    if (results != null) {
      // If the index has mappings, return the ranked pages
      return rank(results, sanitizedQuery);
    } else {
      // Otherwise, return an empty list.
      return Collections.emptyList();
    }
  }

  /**
   * Rank a list of results for a given search term.
   *
   * Since Search Engines present their results as most significant first,
   * we reverse the comparator, so that larger numbers appear first.
   *
   * If printRankingInformation is true, this method will output each page's
   * score to the console.
   */
  private List<WebPage> rank(List<WebPage> results, String term) {
    comparator.setTerm(term); // Set the comparator's term
    Collections.sort(results, Collections.reverseOrder(comparator)); // Sort

    if (printRankingInformation) {
      for (WebPage page: results) {
        System.out.format("%s: %d\n", page.getUrl(), comparator.score(page));
      }
    }

    return results;
  }

  /**
   * Sanitize user input.
   */
  private String sanitizeQuery(String query) {
    return query.toLowerCase().trim();
  }

  /**
   * Set true to have the rank() method print website scores.
   */
  public void setPrintRankingInformation(boolean value) {
    printRankingInformation = value;
  }

  public WebPageComparator getWebPageComparator() {
    return comparator;
  }

  public void setWebPageComparator(WebPageComparator newComparator) {
    comparator = newComparator;
  }

  public void setIndex(Index newIndex) {
    index = newIndex;
  }

}
