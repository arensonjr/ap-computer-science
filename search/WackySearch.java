package search;

import java.io.Console;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;


/**
 * This class runs the Search Engine within a command-line interface.
 *
 * The user can also enter the following input commands to change th behavior
 * of the search engine:
 *
 * ? - Print command help message
 *
 * c:alpha - Switch to AlphabeticalUrlComparator
 * c:first - Switch to FirstIsBestComparator
 * c:myst - Switch to MysteryMagicComparator
 * c:freq - Switch to TermFrequencyComparator
 *
 * f:<filename> - Switch the Index to use <filename> URL list.
 *                (<filename> must be in static/url-lists)
 */
public class WackySearch {

  /**
   * Directory containing files which specify lists of URLs to explore.
   */
  private static final Path URL_LISTS_DIR =
      Paths.get("search", "static", "url-lists");

  private static WebPageComparator termFrequencyComparator =
      new TermFrequencyComparator();

  private static WebPageComparator firstIsBestComparator =
      new FirstIsBestComparator();

  private static WebPageComparator alphabeticalUrlComparator =
      new AlphabeticalUrlComparator();

  private static WebPageComparator mysteryMagicComparator =
      new MysteryMagicComparator();

  /**
   * Read a text file in URL_LISTS_DIR, returning the contents as a List<URL>
   *
   * If filename can't be read, this function returns an empty list.
   */
  public static List<URL> readUrls(String filename) {
    System.out.format("Reading URLs from %s...\n", filename);

    List<URL> urls = new ArrayList<>();
    try {
      Path file = URL_LISTS_DIR.resolve(filename);
      for (String line: Files.readAllLines(file, StandardCharsets.UTF_8)) {
        urls.add(new URL(line));
      }
      System.out.format("Read %d URLs.\n", urls.size());
      return urls;
    } catch (IOException e) {
      System.err.format("Failed to read file:\n", filename);
      System.err.println(e);
    }
    return urls;
  }

  /**
   * The user can switch the comparator being used via the following commands:
   *
   * ? - Print command help message
   *
   * c:alpha - Switch to AlphabeticalUrlComparator
   * c:first - Switch to FirstIsBestComparator
   * c:myst - Switch to MysteryMagicComparator
   * c:freq - Switch to TermFrequencyComparator
   *
   * f:<filename> - Switch the Index to use <filename> URL list.
   *                (<filename> must be in static/url-lists)
   */
  private static void handleCommand(SearchEngine engine, String command) {
    if (command.equals("?")) {
      printHelpMessage();
    } else if (command.contains(":")) {
      if (command.startsWith("c")) { // Switch the comparator
        handleComparatorCommand(engine, command);
      } else if (command.startsWith("f")) { // Switch the URL file
        handleIndexCommand(engine, command);
      }
    }
  }

  /**
   * Switch the comparator being used by the search engine based on the command.
   */
  private static void handleComparatorCommand(
      SearchEngine engine, String command) {
    switch (command) {
      case "c:alpha":
        engine.setWebPageComparator(alphabeticalUrlComparator);
        break;
      case "c:first":
        engine.setWebPageComparator(firstIsBestComparator);
        break;
      case "c:myst":
        engine.setWebPageComparator(mysteryMagicComparator);
        break;
      case "c:freq":
        engine.setWebPageComparator(termFrequencyComparator);
        break;
      default:
        System.out.println("Unrecognized comparator.");
    }
  }

  /**
   * Switch the index being used by the search engine based on the command.
   */
  private static void handleIndexCommand(SearchEngine engine, String command) {
    // Figure out the filename
    String[] splitCommand = command.split(":");
    String filename = splitCommand[splitCommand.length - 1];
    System.out.format("Switching index to use URL list \"%s\"...\n", filename);

    // If the filename is an empty string, give up.
    // Otherwise, try to switch the index
    if (!"".equals(filename)) {
      List<URL> newUrls = readUrls(filename);
      if (newUrls.isEmpty()) { // No URLs to use, don't switch
        System.err.format(
            "Couldn't switch to \"%s\", keeping original index.\n", filename);
      } else {
        Index newIndex = new Index(newUrls);
        engine.setIndex(newIndex);
        System.out.println("Switched to new index.");
      }
    }
  }

  /**
   * Print a help message to a SearchEngine user.
   *
   * Prompted by the user typing "?" into the search console.
   */
  private static void printHelpMessage() {
    System.out.println("?            - Print command help message");
    System.out.println("c:alpha      - Switch to AlphabeticalUrlComparator");
    System.out.println("c:first      - Switch to FirstIsBestComparator");
    System.out.println("c:myst       - Switch to MysteryMagicComparator");
    System.out.println("c:freq       - Switch to TermFrequencyComparator");
    System.out.println(
        "f:<filename> - Switch the Index to use <filename> URL list.");
    System.out.println(
        "               (<filename> must be in static/url-lists)");
  }

  public static void main(String[] args) {
    if (args.length == 0) {
      System.err.println(
          "Please specify a URL list (e.g. 'dog-wikiloop.txt').");
      System.exit(1);
    }

    // Set up the Search Engine
    List<URL> urls = readUrls(args[0]);
    Index index = new Index(urls);
    SearchEngine searchEngine =
        new SearchEngine(index, termFrequencyComparator);

    // Prep to get queries from the command line
    // (console the System, it is doing a good job)
    Console console = System.console();

    // Introduction to the user.
    System.out.println(
        "-------------------------------------------------------------------");
    System.out.println(
        "Welcome to WackySearch! (type \"quit\" to exit, ? for command help)");
    System.out.println(
        "-------------------------------------------------------------------");

    // Go into the main query loop
    String query = console.readLine("WackySearch > ");
    while (!query.equals("quit")) {

      // Hand user commands to change search behavior
      handleCommand(searchEngine, query);

      // Do the search and print the results
      for (WebPage result: searchEngine.search(query)) {
        System.out.format(
            "%s (%d)\n",
            result.getUrl(),
            searchEngine.getWebPageComparator().score(result));
      }

      query = console.readLine("WackySearch > ");
    }
    System.out.println("BYE S2");
  }

}
