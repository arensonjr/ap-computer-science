package lab12;

import java.util.ArrayList;
import java.util.HashSet;

import objects.Food;

/**
 * Simulates a restaurant.
 */
public class MyRestaurant {
  public static void main(String[] args) {
    // Write your code here.
  }

  /**
   * Creates the restaurant's menu.
   */
  public static ArrayList<Meal> generateMenu() {
    // Write your code here.
  }

  /**
   * Picks the heaviest food for Carlos.
   */
  public static Meal getCarlosMeal(ArrayList<Meal> menu) {
    // Write your code here.
  }

  // Write the rest of your meal-picking functions here.
}
