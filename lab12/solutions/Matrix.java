package lab12.solutions;

/**
 * Represents a matrix of numbers.
 *
 * For example:
 *
 *  +--        --+
 *  | 5  3  3  1 |
 *  | 5  3  3  1 |
 *  | 5  3  3  1 |
 *  +--        --+
 */
public class Matrix {
  int[][] numbers;

  public Matrix(int height, int width) {
    numbers = new int[height][width];
  }

  /**
   * Returns the height of the matrix.
   */
  public int getHeight() {
    return numbers.length;
  }

  /**
   * Returns the width of the matrix
   */
  public int getWidth() {
    return numbers[0].length;
  }

  /**
   * Pretty-prints a matrix.
   */
  public String toString() {
    String matrixStr = "";
    for (int row = 0; row < getHeight(); row++) {
      for (int col = 0; col < getWidth(); col++) {
        matrixStr += getNumber(row, col);
        matrixStr += " ";
      }
      matrixStr += "\n"; // newline
    }
    return matrixStr;
  }

  /**
   * Gets a value from the matrix.
   */
  public int getNumber(int row, int column) {
    return numbers[row][column];
  }

  /**
   * Changes a value in the matrix.
   */
  public void setNumber(int row, int column, int newValue) {
    numbers[row][column] = newValue;
  }

  /**
   * Sums a row of the matrix.
   */
  public int sumRow(int row) {
    int total = 0;
    for (int col = 0; col < getWidth(); col++) {
      total += getNumber(row, col);
    }
    return total;
  }

  /**
   * Sums a column of the matrix.
   */
  public int sumColumn(int column) {
    int total = 0;
    for (int row = 0; row < getHeight(); row++) {
      total += getNumber(row, column);
    }
    return total;
  }

  /**
   * Sums the entire matrix.
   */
  public int sum() {
    int total = 0;
    for (int row = 0; row < getHeight(); row++) {
      for (int col = 0; col < getWidth(); col++) {
        total += getNumber(row, col);
      }
    }
    return total;
  }
}
