package lab12.solutions;

import java.util.ArrayList;
import java.util.HashSet;

import objects.Food;

/**
 * Simulates a restaurant.
 */
public class MyRestaurant {
  public static void main(String[] args) {
    // Part 1: Generate and print a menu
    ArrayList<Meal> menu = generateMenu();
    System.out.println("Menu:");
    System.out.println("========");
    System.out.println(menu);
    System.out.println();

    // Part 2: Help people choose what to eat
    Meal carlos = getCarlosMeal(menu);
    System.out.println("Carlos eats " + carlos);
    Meal diana = getDianaMeal(menu);
    System.out.println("Diana eats " + diana);
    Meal eddy = getEddyMeal(menu);
    System.out.println("Eddy eats " + eddy);
  }

  /**
   * Creates the restaurant's menu.
   */
  public static ArrayList<Meal> generateMenu() {
    ArrayList<Meal> menu = new ArrayList<>();

    // Burgers
    HashSet<Food> burgerIngredients = new HashSet<>();
    burgerIngredients.add(new Food("beef", "meat", 500, 4));
    burgerIngredients.add(new Food("bun", "bread", 300, 2));
    burgerIngredients.add(new Food("cheese", "dairy", 400, 2));
    menu.add(new Meal("Hamburger", 5.95, burgerIngredients));

    // Fries
    HashSet<Food> friesIngredients = new HashSet<>();
    friesIngredients.add(new Food("potatoes", "vegetable", 500, 6));
    friesIngredients.add(new Food("oil", "fats", 1000, 3));
    menu.add(new Meal("Fries", 2.99, friesIngredients));

    // Apple slices
    HashSet<Food> appleSpliceIngredients = new HashSet<>();
    appleSpliceIngredients.add(new Food("apples", "fruit", 100, 5));
    menu.add(new Meal("Apple Slices", 1.99, appleSpliceIngredients));

    // Milkshake
    HashSet<Food> milkshakeIngredients = new HashSet<>();
    milkshakeIngredients.add(new Food("milk", "dairy", 200, 12));
    milkshakeIngredients.add(new Food("ice cream", "dairy", 1000, 4));
    menu.add(new Meal("Milkshake", 4.99, milkshakeIngredients));

    // Kale Chips
    HashSet<Food> kaleChipsIngredients = new HashSet<>();
    kaleChipsIngredients.add(new Food("kale", "vegetable", 50, 12));
    kaleChipsIngredients.add(new Food("salt", "???", 0, 2));
    menu.add(new Meal("Kale Chips", 0.99, kaleChipsIngredients));

    return menu;
  }

  /**
   * Picks the heaviest food for Carlos.
   */
  public static Meal getCarlosMeal(ArrayList<Meal> menu) {
    Meal bestMeal = menu.get(0);
    double highestWeight = 0;
    for (Meal meal : menu) {
      if (meal.totalWeight() > highestWeight) {
        bestMeal = meal;
        highestWeight = meal.totalWeight();
      }
    }
    return bestMeal;
  }

  /**
   * Picks the most cost-efficient meal for Diana.
   */
  public static Meal getDianaMeal(ArrayList<Meal> menu) {
    Meal bestMeal = menu.get(0);
    double highestCaloriesPerDollar = 0;
    for (Meal meal : menu) {
      double caloriesPerDollar = meal.totalCalories() / meal.getPrice();
      if (caloriesPerDollar > highestCaloriesPerDollar) {
        bestMeal = meal;
        highestCaloriesPerDollar = caloriesPerDollar;
      }
    }
    return bestMeal;
  }

  /**
   * Picks the healthiest meal for Eddy.
   */
  public static Meal getEddyMeal(ArrayList<Meal> menu) {
    Meal bestMeal = menu.get(0);
    double lowestCaloriesPerOunce = Double.MAX_VALUE;
    for (Meal meal : menu) {
      double caloriesPerOunce = meal.totalCalories() / meal.totalWeight();
      if (caloriesPerOunce < lowestCaloriesPerOunce) {
        bestMeal = meal;
        lowestCaloriesPerOunce = caloriesPerOunce;
      }
    }
    return bestMeal;
  }
}
