package lab12.solutions;

import java.util.HashMap;
import java.util.HashSet;

import objects.Food;

/**
 * Represents a meal at a restaurant.
 */
public class Meal {
  String name;
  double price;
  HashSet<Food> ingredients;

  public Meal(String n, double p, HashSet<Food> i) {
    name = n;
    price = p;
    ingredients = i;
  }

  /**
   * Returns the name of this meal.
   */
  public String getName() {
    return name;
  }

  /**
   * Returns the price of this meal.
   */
  public double getPrice() {
    return price;
  }

  /**
   * Changes the name of this meal.
   */
  public void setName(String newName) {
    name = newName;
  }

  /**
   * Changes the price of this meal.
   */
  public void setPrice(double newPrice) {
    price = newPrice;
  }

  /**
   * Returns the ingredients in this meal.
   */
  public HashSet<Food> getIngredients() {
    return ingredients;
  }

  /**
   * Adds an ingredient to the meal.
   */
  public void addIngredient(Food ingredient) {
    ingredients.add(ingredient);
  }

  /**
   * Deletes all of the ingredients in this meal.
   */
  public void clearIngredients() {
    ingredients.clear();
  }

  /**
   * Formats the meal nicely into a string.
   */
  public String toString() {
    return "(Meal: Name=" + getName()
      + "; Price=" + getPrice()
      + "; Ingredients=" + getIngredients() + ")";
  }

  /**
   * Returns the total number of calories in this food.
   */
  public int totalCalories() {
    int total = 0;
    for (Food ingredient : ingredients) {
      total += ingredient.getCalories();
    }
    return total;
  }

  /**
   * Returns the total weight of this food
   */
  public double totalWeight() {
    double total = 0;
    for (Food ingredient : ingredients) {
      total += ingredient.getWeight();
    }
    return total;
  }

  /**
   * Returns the primary food group (the food group that most of the ingredients belong to).
   */
  public String primaryFoodGroup() {
    HashMap<String, Integer> foodGroupCounts = new HashMap<>();
    // Initialize all the counts to 0
    for (Food ingredient : ingredients) {
      foodGroupCounts.put(ingredient.getFoodGroup(), 0);
    }
    // Now, count for each ingredient
    for (Food ingredient : ingredients) {
      int oldCount = foodGroupCounts.get(ingredient.getFoodGroup());
      foodGroupCounts.put(ingredient.getFoodGroup(), oldCount + 1);
    }
    // Return the max
    String maxGroup = "";
    int maxCount = 0;
    for (Food ingredient : ingredients) {
      String group = ingredient.getFoodGroup();
      int count = foodGroupCounts.get(group);
      if (count > maxCount) {
        maxCount = count;
        maxGroup = group;
      }
    }
    return maxGroup;
  }
}
