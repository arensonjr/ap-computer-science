package chess;

import java.awt.Point;
import java.util.List;

/**
 * Represents a chess piece that can be moved around a chess board.
 */
public interface Piece {
  /**
   * Returns the filename for this piece's image, e.g. "pawn".
   */
  String getName();

  /**
   * Returns this piece's color.
   */
  String getColor();

  /**
   * Calculates all legal moves from the given board position.
   *
   * A legal move is a new location such that the piece can get to this location
   * in a single move, and the new location is within the bounds of the board.
   *
   * For example, if a piece that can move one square in any direction is at
   * coordinate (0,7) on an 8x8 board, then its legal moves would just be
   * [(0,6),  (1,7),  (1,6)] because other moves (like (0,8) or (-1,6)))
   * would be off the edge of the board.
   */
  List<Point> getLegalMoves(int x, int y);
}
