package chess;

import java.awt.Point;

/**
 * Creates new games of chess.
 */
public class Chess {
  // Colors for each player in the game
  public static final String WHITE = "White";
  public static final String BLACK = "Black";

  public static void main(String[] args) {
    // Start a new game!
    Provided.start();
  }

  /**
   * Sets all of the pieces up for a new game of chess.
   *
   * This function will be called from the provided code whenever someone loads
   * the webpage, to give them a fresh board to play on.
   */
  public static void setupBoard(Board board) {
    // Fill the second rows with pawns
    for (int col = 0; col < board.getSize(); col++) {
      board.putPiece(new Point(1, col), new Pawn(BLACK, board, 1));
      board.putPiece(new Point(6, col), new Pawn(WHITE, board, -1));
    }

    // ===== Once you've written classes for real pieces, fill the board with them! =====
    // board.putPiece(new Point(0, 0), new Rook(BLACK, board));
    // board.putPiece(new Point(0, 1), new Knight(BLACK, board));
    // board.putPiece(new Point(0, 2), new Bishop(BLACK, board));
    // board.putPiece(new Point(0, 3), new Queen(BLACK, board));
    // board.putPiece(new Point(0, 4), new King(BLACK, board));
    // board.putPiece(new Point(0, 5), new Bishop(BLACK, board));
    // board.putPiece(new Point(0, 6), new Knight(BLACK, board));
    // board.putPiece(new Point(0, 7), new Rook(BLACK, board));

    // board.putPiece(new Point(7, 0), new Rook(WHITE, board));
    // board.putPiece(new Point(7, 1), new Knight(WHITE, board));
    // board.putPiece(new Point(7, 2), new Bishop(WHITE, board));
    // board.putPiece(new Point(7, 3), new Queen(WHITE, board));
    // board.putPiece(new Point(7, 4), new King(WHITE, board));
    // board.putPiece(new Point(7, 5), new Bishop(WHITE, board));
    // board.putPiece(new Point(7, 6), new Knight(WHITE, board));
    // board.putPiece(new Point(7, 7), new Rook(WHITE, board));
  }
}
