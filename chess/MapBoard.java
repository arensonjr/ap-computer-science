package chess;

import java.awt.Point;
import java.util.HashMap;
import java.util.Map;

import chess.Board;
import chess.Piece;

/**
 * Implements a board by mapping coordinates to the pieces they contain.
 */
public class MapBoard implements Board {
  /**
   * Sparse map from locations to which piece they contain.
   */
  private final Map<Point, Piece> pieces;

  /**
   * Board dimensions.
   */
  private final int size;

  public MapBoard(int s) {
    size = s;
    pieces = new HashMap<>();
  }

  /**
   * Adds a new piece at the specified point.
   *
   * WARNING: this will overwrite whatever piece used to be at that location!
   *
   * This method won't change the state of the board if the point isn't on the
   * board.
   */
  public void putPiece(Point where, Piece what) {
    if (!isPointOnBoard(where)) {
      throw new IllegalArgumentException(
          "I can't put a piece at " + where + "; it's not on the board!");
    }
    pieces.put(where, what);
  }

  public int getSize() {
    return size;
  }

  public boolean isPointOnBoard(Point coordinate) {
    return coordinate.getX() < size
      && coordinate.getX() >= 0
      && coordinate.getY() < size
      && coordinate.getY() >= 0;
  }

  public boolean isSquareFilled(Point coordinate) {
    return pieces.containsKey(coordinate);
  }

  public Piece getPieceAt(Point coordinate) {
    Piece piece = pieces.get(coordinate);
    if (piece == null) {
      throw new IllegalArgumentException("No piece at " + coordinate);
    }
    return piece;
  }

  public void movePiece(Point oldLocation, Point newLocation) {
    Piece piece = getPieceAt(oldLocation);
    if (!isPointOnBoard(newLocation)) {
      throw new IllegalArgumentException(
          "I can't move a piece to " + newLocation + "; it's not on the board!");
    }
    pieces.remove(oldLocation);
    pieces.put(newLocation, piece);
  }
}
