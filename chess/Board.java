package chess;

import java.awt.Point;

/**
 * Represents a chessboard.
 */
public interface Board {
  /**
   * Returns the size of the game board.
   *
   * The board is square; this is equal to both its height and width.
   */
  int getSize();

  /**
   * Puts a new piece at the selected location on the board.
   *
   * WARNING: This will overwrite whatever piece was there previously! This
   * should only be used for initial setup of the game.
   */
  void putPiece(Point coordinate, Piece newPiece);

  /**
   * Returns true if the coordinate is a square on the board; returns
   * false if the coordinate is off the end of the board.
   */
  boolean isPointOnBoard(Point coordinate);

  /**
   * Returns true if the coordinate is filled by a game piece; returns
   * false if the coordinate is off the board or empty.
   */
  boolean isSquareFilled(Point coordinate);

  /**
   * Returns the piece at the requested square.
   *
   * If the square is empty, or coordinate is not a square on the board,
   * this method will throw a runtime error.
   */
  Piece getPieceAt(Point coordinate);

  /**
   * Moves a piece from oldLocation to newLocation.
   *
   * If either location isn't on the board, this method will throw a runtime
   * error.
   */
  void movePiece(Point oldLocation, Point newLocation);
}
