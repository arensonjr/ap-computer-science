const React = require("react");
const ReactDOM = require("react-dom");

const ChessController = require("./controller.jsx");
require("./chess.css");


const target = document.getElementById("target");
ReactDOM.render(<ChessController
    gameToken={target.getAttribute("data-game-token")}
    board={JSON.parse(target.getAttribute("data-board"))}
/>, target);
