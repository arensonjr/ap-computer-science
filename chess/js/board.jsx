const React = require("react");


const Board = React.createClass({
  render() {
    // We're gonna iterate over the whole chess board, and create a square
    // for each piece:
    const rows = []
    for (let r = 0; r < this.props.board.size; r++) {
      const row = [];
      for (let c = 0; c < this.props.board.size; c++) {
        let idx = r + "," + c;
        // Is there a piece in this cell, or no?
        let CurrComponent;
        if (this.props.board[idx]) {
          CurrComponent = Piece;
        } else {
          CurrComponent = Square;
        }

        // Is it selected? Is it highlighted?
        const selected = (this.props.selected === r + "," + c);
        let option = false;
        for (let i = 0; i < this.props.options.length; i++) {
          const opt = this.props.options[i];
          if (opt.x === r && opt.y === c) {
            option = true;
            break;
          }
        }
        // Render it
        row.push(<CurrComponent
            key={r + ":" + c}
            row={r}
            col={c}
            selected={selected}
            option={option}
            piece={this.props.board[idx]}
            onPieceSelected={this.props.onPieceSelected}
            onDestination={this.props.onDestination}
          />);
      }
      rows.push(<div key={"row" + r}>{row}</div>);
    }

    return <div>
        <p>It's <strong className="turn" style={{
          color: this.props.board.player, // hax =)
        }}>{this.props.board.player}</strong>'s turn!</p>
        <div className="board">{rows}</div>
      </div>;
  }
});

const Square = React.createClass({
  render() {
    const totalIndex = (this.props.row + this.props.col) % 2;
    const parity = (totalIndex == 0) ? "even" : "odd";
    return <div
      className="square"
      data-parity={parity}
      data-selected={this.props.selected}
      data-option={this.props.option}
      onClick={this.props.option ? this.moveTo : function() {}}
    />;
  },
  moveTo() {
    this.props.onDestination(this.props.row, this.props.col);
  }
});

const Piece = React.createClass({
  render() {
    const totalIndex = (this.props.row + this.props.col) % 2;
    const parity = (totalIndex == 0) ? "even" : "odd";
    return <div
      className="square piece"
      data-parity={parity}
      data-selected={this.props.selected}
      data-option={this.props.option}
      onClick={this.props.option ? this.moveTo : this.choosePiece}
      style={{
        backgroundImage: 'url(' + this.props.piece + ')',
      }}
    />;
  },
  choosePiece() {
    this.props.onPieceSelected(this.props.row, this.props.col);
  },
  moveTo() {
    this.props.onDestination(this.props.row, this.props.col);
  }
});

module.exports = Board;
