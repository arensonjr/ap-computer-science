const React = require("react");

const Board = require("./board.jsx");

const ChessController = React.createClass({
  getInitialState() {
    return {
      board: this.props.board,
      selected: "",
      options: [],
    };
  },
  render() {
    return <Board
      gameToken={this.props.gameToken}
      onPieceSelected={this.onPieceSelected}
      onDestination={this.onDestination}
      board={this.state.board}
      selected={this.state.selected}
      options={this.state.options}
    />;
  },

  onPieceSelected(row, col) {
    // Deselect the previous selection first
    this.setState({selected: "", options: []});

    // Re-select the new choice, and its options
    var req = new XMLHttpRequest();
    req.open('GET', '/legalmoves?' + [this.props.gameToken, row, col].join('&'), true);
    req.onreadystatechange = function() {
      if (req.readyState === 4 && req.status !== 200) {
        alert("Uh oh! Looks like your code doesn't work. Check out the Cloud9 console.");
      } else if (req.readyState === 4 && req.status === 200) {
        const moves = JSON.parse(req.responseText);
        if (moves.length <= 0) {
          // No legal moves --> do nothing
          return;
        }

        this.setState({
          selected: row + "," + col,
          options: moves,
        });
      }
    }.bind(this);
    req.send();
  },

  onDestination(row, col) {
    const src = this.state.selected.split(',');
    const dest = [row, col];

    // Click --> clear out selections
    this.setState({selected: "", options: []});

    const req = new XMLHttpRequest();
    req.open('GET', '/move?' + [this.props.gameToken].concat(src).concat(dest).join('&'), true);
    req.onreadystatechange = function() {
      if (req.readyState === 4 && req.status !== 200) {
        alert("Uh oh! Looks like your code doesn't work. Check out the Cloud9 console.");
      } else if (req.readyState === 4 && req.status === 200) {
        const board = JSON.parse(req.responseText);
        this.setState({
          board: board,
        })
      }
    }.bind(this);
    req.send();
  }
});

module.exports = ChessController;
