module.exports = {
    entry:  __dirname + "/entry.jsx",
    output: {
        path: __dirname,
        filename: "bundle.js",
    },
    module: {
        loaders: [
            { test: /\.css$/, loader: "style!css" },
            { test: /\.js$/, exclude: /node_modules/, loader: "babel",
              query: { presets: ["es2015"] } },
            { test: /\.jsx$/, exclude: /node_modules/, loader: "babel",
              query: { presets: ["react", "es2015"] } },
        ]
    },
    devtool: "source-map",
};
