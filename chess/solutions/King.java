package chess.solutions;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import chess.Board;

public class King extends AbstractPiece {

  public King(String color, Board board) {
    super(color, "king", board);
  }

  public List<List<Point>> getCandidateVectors(int x, int y) {
    // Kings can move one space in any direction.
    List<List<Point>> moves = new ArrayList<>();
    for (int dx : new int[]{ -1, 0, 1 }) {
      for (int dy : new int[]{ -1, 0, 1}) {
        // Don't move to ourselves
        if (dx == 0 && dy == 0) {
          continue;
        }
        moves.add(Arrays.asList(new Point(x + dx, y + dy)));
      }
    }

    return moves;
  }
}
