package chess.solutions;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import chess.Board;
import chess.Piece;

/**
 * Abstracts some of the common logic necessary to chess pieces.
 */
public abstract class AbstractPiece implements Piece {
  /**
   * The piece's color.
   */
  private String color;

  /**
   * The name of this piece.
   */
  private String pieceName;

  /**
   * The chess board.
   */
  private Board board;

  public AbstractPiece(String c, String n, Board b) {
    color = c;
    pieceName = n;
    board = b;
  }

  public String getColor() {
    return color;
  }

  public String getName() {
    return pieceName;
  }

  /**
   * Returns all possible squares to which this piece can move,
   * regardless of which other piece is on that square or whether
   * the square is on the board.
   *
   * These squares should be returned in the form of multiple lines,
   * such that if one space in the line is occupied, the piece
   * wouldn't be able to move any farther.
   */
  protected abstract List<List<Point>> getCandidateVectors(int x, int y);

  public List<Point> getLegalMoves(int x, int y) {
    List<List<Point>> candidates = getCandidateVectors(x, y);

    System.err.println("\tMoves for " + getName() + ":");
    List<Point> moves = new ArrayList<Point>();
    for (List<Point> vector : candidates) {
      System.err.println("\t\tAlong vector " + vector);
      for (Point move : vector) {
        // If it's not on the board, ignore it (and we can't move farther).
        if (!board.isPointOnBoard(move)) {
          System.err.println("\t\t\t" + move + " not on board -- stopping");
          break;
        }

        // If there's a piece of its own color, ignore it (and we can't move
        // farther).
        if (board.isSquareFilled(move) && board.getPieceAt(move).getColor().equals(color)) {
          System.err.println("\t\t\t" + move + " is our color -- stopping");
          break;
        }

        // Otherwise, add it to our set of moves!
        moves.add(move);
        System.err.println("\t\t\t" + move + " is a legal move!");

        // If we ran into another piece, stop moving along this vector -- we
        // can't go any farther.
        if (board.isSquareFilled(move)) {
          System.err.println("\t\t\t" + move + " was filled, too -- stopping");
          break;
        }
      }
    }

    return moves;
  }

  /**
   * Generates a line of points going away from (x, y) in the specified direction.
   *
   * There are at least enough points in the returned line to make it off the end of the board.
   */
  protected List<Point> generateVector(int x, int y, int dx, int dy) {
    List<Point> vector = new ArrayList<>();
    for (int i = 1; i < board.getSize(); i++) {
      vector.add(new Point(x + (i * dx), y + (i * dy)));
    }
    return vector;
  }
}
