package chess.solutions;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import chess.Board;

/**
 * Implements a Rook in the game of chess.
 */
public class Rook extends AbstractPiece {
  public Rook(String color, Board board) {
    super(color, "rook", board);
  }

  public List<List<Point>> getCandidateVectors(int x, int y) {
    // A rook can move up to 8 spaces along any of the four cardinal directions
    List<List<Point>> moves = new ArrayList<>();
    moves.add(generateVector(x, y, -1,  0));
    moves.add(generateVector(x, y,  1,  0));
    moves.add(generateVector(x, y,  0, -1));
    moves.add(generateVector(x, y,  0,  1));
    return moves;
  }
}
