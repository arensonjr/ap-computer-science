package chess.solutions;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import chess.Board;

/**
 * Implements a Queen in the game of chess.
 */
public class Queen extends AbstractPiece {
  public Queen(String color, Board board) {
    super(color, "queen", board);
  }

  public List<List<Point>> getCandidateVectors(int x, int y) {
    // A queen can move up to 8 spaces along any of the eight
    // possible directions!
    List<List<Point>> moves = new ArrayList<>();
    moves.add(generateVector(x, y, -1, -1));
    moves.add(generateVector(x, y, -1,  1));
    moves.add(generateVector(x, y, -1,  0));
    moves.add(generateVector(x, y,  1, -1));
    moves.add(generateVector(x, y,  1,  1));
    moves.add(generateVector(x, y,  1,  0));
    moves.add(generateVector(x, y,  0, -1));
    moves.add(generateVector(x, y,  0,  1));
    return moves;
  }
}
