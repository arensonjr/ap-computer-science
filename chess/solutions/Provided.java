package chess.solutions;

import java.awt.Point;
import java.io.IOException;
import java.math.BigInteger;
import java.net.InetSocketAddress;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import chess.Board;
import chess.Piece;


/**
 * Java server to host a website for a game of Chess.
 */
public class Provided {
  public static void start() {
    try {
      HttpServer server = HttpServer.create();
      server.createContext("/", new NewGameHandler());
      server.createContext("/legalmoves", new LegalMovesHandler());
      server.createContext("/move", new MoveHandler());
      server.createContext("/js", new JsFilesHandler());
      server.createContext("/piece", new PieceImageHandler());

      int port = Integer.parseInt(System.getenv("C9_PORT"));
      String ip = System.getenv("C9_IP");
      server.bind(new InetSocketAddress(ip, port), port);

      server.start();
      String hostname = System.getenv("C9_HOSTNAME");
      System.out.format("Running at http://%s\n", hostname);
    } catch (Exception e) {
      System.err.println("Running chess failed:");
      e.printStackTrace();
      System.exit(1);
    }
  }

  /**
   * All currently running games.
   */
  private static final HashMap<String, GameState> games = new HashMap<>();
  private static class GameState {
    public Board game;
    public String currPlayer;
    public GameState(Board g) {
      game = g;
      currPlayer = Chess.WHITE;
    }
  }

  /**
   * Random number generator.
   */
  private static final Random random = new Random();

  /**
   * The bundle of Javascript/CSS.
   */
  private static final Path JS = Paths.get("chess", "js");

  /**
   * The HTML template for the page.
   */
  private static final Path TEMPLATE = Paths.get("chess", "static", "chess_game.html");

  /**
   * Converts a board to JSON.
   */
  private static String jsonify(GameState game) {
    Board board = game.game;
    StringBuilder json = new StringBuilder("{");
    json.append("\"player\":\"")
      .append(game.currPlayer)
      .append("\",\"size\":")
      .append(board.getSize());
    for (int row = 0; row < board.getSize(); row++) {
      for (int col = 0; col < board.getSize(); col++) {
        Point coord = new Point(row, col);
        if (board.isSquareFilled(coord)) {
          Piece piece = board.getPieceAt(coord);
          json.append(',')
            .append("\"").append(row).append(',').append(col).append("\"")
            .append(":\"piece/")
            .append(piece.getColor().toLowerCase())
            .append('-')
            .append(piece.getName().toLowerCase())
            .append(".png\"");
        }
      }
    }
    json.append('}');
    return json.toString();
  }

  /**
   * Generates a new game ID.
   */
  private static String newGameId() {
    return new BigInteger(130, random).toString();
  }

  /**
   * Handle requests for the Chess homepage, by starting a new game.
   */
  private static class NewGameHandler extends AbstractHandler {
    @Override public String doWork(HttpExchange exchange) throws IOException {
      // Create a new game
      Board game = new MapBoard(8);
      Chess.setupBoard(game);
      GameState state = new GameState(game);
      String gameId = newGameId();

      games.put(gameId, state);

      // Serialize the game
      String template =
          new String(Files.readAllBytes(TEMPLATE), StandardCharsets.UTF_8);
      return template
          .replaceAll("\\$\\{board\\}", jsonify(state).replaceAll("\"", "&quot;"))
          .replaceAll("\\$\\{gameid\\}", gameId);
    }
  }

  /**
   * Get the legal moves for a piece.
   */
  private static class LegalMovesHandler extends AbstractHandler {
    @Override public String doWork(HttpExchange exchange) throws IOException {
      String[] params = exchange.getRequestURI().getQuery().split("&");
      String gameId = params[0];
      int row = Integer.parseInt(params[1]);
      int col = Integer.parseInt(params[2]);

      GameState state = games.get(gameId);
      if (!state.game.isSquareFilled(new Point(row, col))) {
        // No piece --> no moves
        return "[]";
      }

      // You can only move if it's your turn
      Piece piece = state.game.getPieceAt(new Point(row, col));
      if (!state.currPlayer.equals(piece.getColor())) {
        return "[]";
      }

      List<Point> moves = piece.getLegalMoves(row, col);
      StringBuilder json = new StringBuilder().append('[');;
      for (int i = 0; i < moves.size(); i++) {
        if (i > 0) {
          json.append(',');
        }
        Point move = moves.get(i);
        json.append("{\"x\":").append((int)move.getX())
          .append(",\"y\":").append((int)move.getY())
          .append("}");
      }
      json.append(']');
      return json.toString();
    }
  }

  /**
   * Makes a move and returns the board state, as JSON.
   */
  private static class MoveHandler extends AbstractHandler {
    @Override public String doWork(HttpExchange exchange) throws IOException {
      String[] params = exchange.getRequestURI().getQuery().split("&");
      String gameId = params[0];
      int srcRow = Integer.parseInt(params[1]);
      int srcCol = Integer.parseInt(params[2]);
      int destRow = Integer.parseInt(params[3]);
      int destCol = Integer.parseInt(params[4]);

      GameState state = games.get(gameId);
      state.game.movePiece(new Point(srcRow, srcCol), new Point(destRow, destCol));
      if (state.currPlayer == Chess.WHITE) {
        state.currPlayer = Chess.BLACK;
      } else {
        state.currPlayer = Chess.WHITE;
      }

      return jsonify(state);
    }
  }

  /**
   * Serves the image of pieces.
   */
  private static final class PieceImageHandler extends AbstractHandler {
    @Override public byte[] doWorkInBytes(HttpExchange exchange) throws IOException {
      String imageFile = exchange.getRequestURI().getPath().split("/")[2];
      exchange.getResponseHeaders().set("Cache-Control", "no-transform,public,max-age=900,s-maxage=900");
      exchange.getResponseHeaders().set("Expires", "Mon, 04 Jul 2017 00:00:00 GMT");
      return Files.readAllBytes(Paths.get("chess", "static", imageFile));
    }
  }

  /**
   * Serves static files.
   */
  private static final class JsFilesHandler extends AbstractHandler {
    @Override public byte[] doWorkInBytes(HttpExchange exchange) throws IOException {
      String jsFile = exchange.getRequestURI().getPath().substring("/js/".length());
      return Files.readAllBytes(JS.resolve(jsFile));
    }
  }

  /**
   * Class that does HTTP error handling.
   */
  private static abstract class AbstractHandler implements HttpHandler {
    @Override public void handle(HttpExchange exchange) throws IOException {
      URI url = exchange.getRequestURI();
      try {
        byte[] pageBytes = doWorkInBytes(exchange);
        exchange.sendResponseHeaders(200, pageBytes.length);
        exchange.getResponseBody().write(pageBytes);
        exchange.close();
        System.err.format("[200] [%s] OK\n", url);
      } catch (Throwable t) {
        t.printStackTrace();
        exchange.sendResponseHeaders(500, 0);
        exchange.close();
        System.err.format("[500] [%s] Error: %s\n", url, t);
      }
    }

    /**
     * Actually does the work.
     *
     * Can be overridden to not return a string.
     */
    public byte[] doWorkInBytes(HttpExchange exchange) throws IOException {
      return doWork(exchange).getBytes();
    }

    /**
     * Actually does work.
     *
     * Can be overridden to return a string instead of raw bytes.
     */
    public String doWork(HttpExchange exchange) throws IOException {
      return "";
    }
  }
}
