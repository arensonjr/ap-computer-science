package chess.solutions;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import chess.Board;

/**
 * Implements a Bishop in the game of chess.
 */
public class Bishop extends AbstractPiece {
  public Bishop(String color, Board board) {
    super(color, "bishop", board);
  }

  public List<List<Point>> getCandidateVectors(int x, int y) {
    // A bishop can move up to 8 spaces along any of the four diagonals
    List<List<Point>> moves = new ArrayList<>();
    moves.add(generateVector(x, y, -1, -1));
    moves.add(generateVector(x, y, -1,  1));
    moves.add(generateVector(x, y,  1, -1));
    moves.add(generateVector(x, y,  1,  1));
    return moves;
  }
}
