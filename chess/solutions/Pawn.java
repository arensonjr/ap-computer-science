package chess.solutions;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import chess.Board;
import chess.Piece;

/**
 * Represents a pawn in the game of chess.
 *
 * ♫ "But it ain't him to blame / He's only a pawn in their game" ♫
 */
public class Pawn implements Piece {

  private String color;
  private Board board;
  private int forwardX;

  public Pawn(String c, Board b, int fx) {
    color = c;
    board = b;
    forwardX = fx;
  }

  public String getName() {
    return "pawn";
  }

  public String getColor() {
    return color;
  }

  public List<Point> getLegalMoves(int x, int y) {
    List<Point> moves = new ArrayList<>();

    // Pawns can move "forward", unless there's a piece in their way
    Point forward = new Point(x + forwardX, y);
    if (board.isPointOnBoard(forward) && !board.isSquareFilled(forward)) {
      moves.add(forward);
    }

    // Pawns can move diagonally forward, if there's an enemy to take there!
    for (int dy : new int[]{ -1, 1 }) {
      Point diagonal = new Point(x + forwardX, y + dy);
      if (board.isSquareFilled(diagonal)) {
        Piece otherPiece = board.getPieceAt(diagonal);
        if (!otherPiece.getColor().equals(color)) {
          // It's an enemy piece. We can take it!
          moves.add(diagonal);
        }
      }
    }

    return moves;
  }
}
