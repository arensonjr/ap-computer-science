package chess.solutions;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import chess.Board;

/**
 * Implements a Knight in the game of Chess.
 */
public class Knight extends AbstractPiece {
  public Knight(String color, Board board) {
    super(color, "knight", board);
  }

  public List<List<Point>> getCandidateVectors(int x, int y) {
    List<List<Point>> moves = new ArrayList<>();

    // A knight moves in weird two-one-way, one-the-other steps
    moves.add(Arrays.asList(new Point(x + 2, y + 1)));
    moves.add(Arrays.asList(new Point(x + 1, y + 2)));

    moves.add(Arrays.asList(new Point(x - 2, y + 1)));
    moves.add(Arrays.asList(new Point(x + 1, y - 2)));

    moves.add(Arrays.asList(new Point(x + 2, y - 1)));
    moves.add(Arrays.asList(new Point(x - 1, y + 2)));

    moves.add(Arrays.asList(new Point(x - 2, y - 1)));
    moves.add(Arrays.asList(new Point(x - 1, y - 2)));

    return moves;
  }
}
