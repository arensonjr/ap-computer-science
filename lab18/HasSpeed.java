package lab18;

/**
 * Represents a thing that has a speed.
 */
public interface HasSpeed {
  /**
   * Returns the distance this thing travels in the input time.
   */
  double distanceTraveled(double seconds);

  /**
   * Returns the number of seconds it takes to move a certain distance.
   */
  double timeToFinish(double feet);
}
