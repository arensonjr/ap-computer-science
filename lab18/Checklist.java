package lab18;

import java.util.ArrayList;
import java.util.List;

/**
 * A checklist of things to do.
 */
public class Checklist implements Chart {
  public Checklist() {
    // Write your code here
  }

  public void addChore(String chore) {
    // Write your code here
  }

  // ========== Chart methods ==========

  public int getNumRows() {
    // Write your code here
  }

  public int getNumColumns() {
    // Write your code here
  }

  public String getCellContents(int row, int col) {
    // Write your code here
  }
}
