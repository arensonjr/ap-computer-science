package lab18;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Prints the results for the SF ("So Fake") Marathon.
 */
public class SFMarathon {
  public static void main(String[] args) {
    // Parse the runners
    List<MarathonRunner> runners = new ArrayList<>();
    int i = 0;
    while (i < args.length) {
      String name = args[i];
      int time = Integer.parseInt(args[i + 1]);
      i += 2;

      runners.add(new MarathonRunner(name, time));
    }

    // Sort them by completion time
    // ===== (write your code here) =====

    // Pretty-print them
    // ===== (write your code here) =====
  }
}
