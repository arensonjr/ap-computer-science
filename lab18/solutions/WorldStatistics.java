package lab18.solutions;

import java.util.ArrayList;
import java.util.List;

/**
 * Prints some statistics about the world.
 */
public class WorldStatistics {
  public static void main(String[] args) {
    City nyc = new City("NYC", "USA", 123, 456, 15.5);
    City lax = new City("Los Angeles", "USA", 1, 2, 5.5);
    City paris = new City("Paris", "France", 4, 5, 10.0);
    List<City> cities = new ArrayList<>();
    cities.add(nyc);
    cities.add(lax);
    cities.add(paris);

    List<Building> buildings = new ArrayList<>();
    buildings.add(new Building("Eiffel Tower", paris, 12345.0, 1945));
    buildings.add(new Building("Empire State", nyc, 456.0, 1990));

    List<Mountain> mountains = new ArrayList<>();
    mountains.add(new Mountain("Everest", "Himalaya", 28, 87, 8848));
    mountains.add(new Mountain("Pike's Peak", "Rockies", 123, 456, 4500));

    System.out.println(
        "Tallest building before 1990: "
        + tallestBuildingBefore(1990, buildings));

    System.out.println(
        "Total population: " + totalPopulationInMillions(cities));

    PointOfInterest ourLocation = new City("Mountain View", "USA", 5, 6, 10.0);
    List<PointOfInterest> allPoints = new ArrayList<>();
    allPoints.addAll(buildings);
    allPoints.addAll(cities);
    allPoints.addAll(mountains);
    System.out.println(
        "Nearest to us: " + nearest(ourLocation, allPoints));
  }

  /**
   * Returns the tallest building built before `year`.
   */
  public static Building tallestBuildingBefore(int year, List<Building> buildings) {
    Building tallest = buildings.get(0);
    for (Building b : buildings) {
      if (b.getHeight() > tallest.getHeight()) {
        tallest = b;
      }
    }
    return tallest;
  }

  /**
   * Returns the total population of all of the cities.
   */
  public static double totalPopulationInMillions(List<City> cities) {
    double total = 0;
    for (City c : cities) {
      total += c.getPopulationInMillions();
    }
    return total;
  }

  /**
   * Returns the nearest point to the target.
   */
  public static PointOfInterest nearest(PointOfInterest target, List<PointOfInterest> options) {

    PointOfInterest nearest = options.get(0);
    for (PointOfInterest point : options) {
      double thisDistance = point.getLocation().distance(target.getLocation());
      double nearestDistance = nearest.getLocation().distance(target.getLocation());

      if (thisDistance < nearestDistance) {
        nearest = point;
      }
    }
    return nearest;
  }

}
