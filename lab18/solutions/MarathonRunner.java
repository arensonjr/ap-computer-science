package lab18.solutions;

/**
 * Represents a single racer in a marathon.
 */
public class MarathonRunner implements Comparable<MarathonRunner> {
  private String name;
  private int completionMinutes;

  public MarathonRunner(String n, int m) {
    name = n;
    completionMinutes = m;
  }

  public String getName() {
    return name;
  }

  public int getElapsedMinutes() {
    return completionMinutes;
  }

  public int compareTo(MarathonRunner other) {
    return completionMinutes - other.getElapsedMinutes();
  }

  public String toString() {
    return name;
  }
}
