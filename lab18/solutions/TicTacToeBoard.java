package lab18.solutions;

/**
 * Implements a board for a Tic-Tac-Toe game.
 */
public class TicTacToeBoard implements Chart {
  private String[][] board;

  public TicTacToeBoard() {
    board = new String[][]{
      { " ", " ", " " },
      { " ", " ", " " },
      { " ", " ", " " },
    };
  }

  public void move(int row, int col, String player) {
    board[row][col] = player;
  }

  public int getNumRows() {
    return board.length;
  }

  public int getNumColumns() {
    return board[0].length;
  }

  public String getCellContents(int row, int col) {
    return board[row][col];
  }
}
