package lab18.solutions;

import java.awt.Point;

/**
 * Represents one of Earth's Mountains.
 */
public class Mountain implements PointOfInterest {
  private String name;
  private String range;
  private int latitude;
  private int longitude;
  private double height;

  public Mountain(String n, String r, int lat, int lon, double h) {
    name = n;
    range = r;
    latitude = lat;
    longitude = lon;
    height = h;
  }

  public String getName() {
    return name;
  }

  public void setName(String newValue) {
    name = newValue;
  }

  public String getRange() {
    return range;
  }

  public void setRange(String newRange) {
    range = newRange;
  }

  public int getLatitude() {
    return latitude;
  }

  public void setLatitude(int newValue) {
    latitude = newValue;
  }

  public int getLongitude() {
    return longitude;
  }

  public void setLongitude(int newValue) {
    longitude = newValue;
  }

  public double getHeight() {
    return height;
  }

  public void setHeight(double newHeight) {
    height = newHeight;
  }

  public Point getLocation() {
    return new Point(latitude, longitude);
  }
}
