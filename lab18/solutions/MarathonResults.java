package lab18.solutions;

import java.util.List;

/**
 * Represents results for a race.
 */
public class MarathonResults implements Chart {
  private List<MarathonRunner> runners;

  public MarathonResults(List<MarathonRunner> r) {
    runners = r;
  }

  // ========== Chart Methods ==========

  public int getNumRows() {
    // one header row + one row per runner
    return 1 + runners.size();
  }

  public int getNumColumns() {
    // name, time
    return 2;
  }

  public String getCellContents(int row, int col) {
    // Header == row #0
    if (row == 0) {
      if (col == 0) {
        return "[Name]";
      } else { // col == 1
        return "[Time (minutes)]";
      }
    }

    // Runners = other rows
    MarathonRunner runner = runners.get(row - 1);
    if (col == 0) {
      return runner.getName();
    } else { // col == 1
      // (convert to a string by adding to the empty string)
      return "" + runner.getElapsedMinutes();
    }
  }
}
