package lab18.solutions;

/**
 * Represents a thing that has height/width.
 */
public interface HasDimensions {
  /**
   * Returns its width, in inches.
   */
  double getWidth();

  /**
   * Returns its height, in inches.
   */
  double getHeight();
}

