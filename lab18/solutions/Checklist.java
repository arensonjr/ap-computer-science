package lab18.solutions;

import java.util.ArrayList;
import java.util.List;

/**
 * A checklist of things to do.
 */
public class Checklist implements Chart {
  private List<String> chores;

  public Checklist() {
    chores = new ArrayList<>();
  }

  public void addChore(String chore) {
    chores.add(chore);
  }

  // ========== Chart methods ==========

  public int getNumRows() {
    // one row for header + one row per chore
    return 1 + chores.size();
  }

  public int getNumColumns() {
    // chores always have two columns
    return 2;
  }

  public String getCellContents(int row, int col) {
    // Is it the header?
    if (row == 0) {
      if (col == 0) {
        return " ";
      } else {
        return "[Chores]";
      }
    }

    // Otherwise, which chore?
    if (col == 1) {
      return chores.get(row - 1);
    } else {
      return " ";
    }
  }
}
