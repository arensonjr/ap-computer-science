package lab18.solutions;

import java.awt.Point;

/**
 * Represents one of the cities of the world.
 */
public class Building implements PointOfInterest {
  private String name;
  private City city;
  private double height;
  private int yearBuilt;

  public Building(String n, City c, double h, int y) {
    name = n;
    city = c;
    height = h;
    yearBuilt = y;
  }

  public String getName() {
    return name;
  }

  public void setName(String newValue) {
    name = newValue;
  }

  public City getCity() {
    return city;
  }

  public void setCity(City newCity) {
    city = newCity;
  }

  public double getHeight() {
    return height;
  }

  public void setHeight(double newHeight) {
    height = newHeight;
  }

  public int getYearBuilt() {
    return yearBuilt;
  }

  public void setYear(int newYear) {
    yearBuilt = newYear;
  }

  public Point getLocation() {
    return city.getLocation();
  }
}
