package lab18.solutions;

/**
 * Prints a list of things for me to do.
 */
public class MyChecklists {
  public static void main(String[] args) {
    Checklist list = new Checklist();
    list.addChore("wash dog");
    list.addChore("mow lawn");
    list.addChore("fold socks");
    list.addChore("buy groceries");
    Provided.printChart(list);

    Checklist frabjousList = new Checklist();
    frabjousList.addChore("frobulate widget");
    frabjousList.addChore("acquire whizbang");
    frabjousList.addChore("combobulate quuxes");
    frabjousList.addChore("disestablish quaternions");
    Provided.printChart(frabjousList);
  }
}
