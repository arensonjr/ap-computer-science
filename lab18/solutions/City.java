package lab18.solutions;

import java.awt.Point;

/**
 * Represents one of the cities of the world.
 */
public class City implements PointOfInterest {
  private String name;
  private String country;
  private int latitude;
  private int longitude;
  private double populationInMillions;

  public City(String n, String c, int lat, int lon, double p) {
    name = n;
    country = c;
    latitude = lat;
    longitude = lon;
    populationInMillions = p;
  }

  public String getName() {
    return name;
  }

  public void setName(String newValue) {
    name = newValue;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String newValue) {
    country = newValue;
  }

  public int getLatitude() {
    return latitude;
  }

  public void setLatitude(int newValue) {
    latitude = newValue;
  }

  public int getLongitude() {
    return longitude;
  }

  public void setLongitude(int newValue) {
    longitude = newValue;
  }

  public double getPopulationInMillions() {
    return populationInMillions;
  }

  public void setPopulationInMillions(double newValue) {
    populationInMillions = newValue;
  }

  public Point getLocation() {
    return new Point(latitude, longitude);
  }
}
