package lab18.solutions;

import java.awt.Point;

/**
 * Represents an interesting location in the world.
 */
public interface PointOfInterest {
  /**
   * Returns the name of the point of interest.
   */
  String getName();

  /**
   * Returns the (latitude, longitude) of this place.
   */
  Point getLocation();
}
