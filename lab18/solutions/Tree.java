package lab18.solutions;

/**
 * Represents a tree.
 */
public class Tree implements HasDimensions {
  private String type;
  private int age;
  private double width;
  private double height;

  public Tree(String t, int a, double w, double h) {
    type = t;
    age = a;
    width = w;
    height = h;
  }

  public double getWidth() {
    return width;
  }

  public double getHeight() {
    return height;
  }

  public String toString() {
    return type;
  }
}

