package lab18.solutions;

import lab19.solutions.*;

/**
 * Prints the results of some competitions.
 */
public class Competitions {
  public static void main(String[] args) {
    Car accord = new Car("Honda Accord", 57, 190, 125);
    Car bmw = new Car("BMW Z3", 50, 158, 150);
    Car flintstones = new Car("Flintstones Car", 65, 120, 30);
    oneMileRace(accord, bmw);
    collision(accord, bmw);

    Animal sloth = new Animal("Sloth", 0.076);
    Animal cheetah = new Animal("Cheetah", 29);
    Animal bear = new Animal("Bear", 11);
    Animal human = new Animal("Human", 12.5);
    oneMileRace(accord, sloth);
    oneMileRace(flintstones, cheetah);
    oneMileRace(bear, human);

    HasSpeed greyhound = new Greyhound("Westy", 40);
    oneMileRace(greyhound, cheetah);

    HasSpeed fish = new Guppy("Freddie");
    oneMileRace(fish, accord);

    Tree oak = new Tree("Oak", 100, 72, 600);
    Tree sapling = new Tree("Sapling", 1, 6, 36);
    collision(accord, oak);
    collision(bmw, sapling);
  }

  /**
   * Prints out the results of a one mile race between two speedy things.
   */
  public static void oneMileRace(HasSpeed left, HasSpeed right) {
    double timeLeft = left.timeToFinish(5280);
    double timeRight = right.timeToFinish(5280);
    System.out.println(left + " finishes in " + timeLeft + " seconds.");
    System.out.println(right + " finishes in " + timeRight + " seconds.");

    if (timeLeft < timeRight) {
      System.out.println(left + " wins!");
    } else if (timeRight < timeLeft) {
      System.out.println(right + " wins!");
    } else {
      System.out.println("It's a tie!");
    }
  }

  /**
   * Prints out the results of a collision between two sizable objects.
   */
  public static void collision(HasDimensions left, HasDimensions right) {
    double leftSize = left.getWidth() * left.getHeight();
    double rightSize = right.getWidth() * right.getHeight();

    if (leftSize > rightSize) {
      System.out.println(left + " wins in a collision against " + right);
    } else if (rightSize > leftSize) {
      System.out.println(right + " wins in a collision against " + left);
    } else {
      System.out.println(
          "Both " + left + " and " + right + " would be pretty beat up in a collision");
    }
  }
}
