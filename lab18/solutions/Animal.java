package lab18.solutions;

/**
 * Represents an animal.
 */
public class Animal implements HasSpeed {
  private String species;
  private double topSpeedMetersPerSecond;

  public Animal(String s, double mps) {
    species = s;
    topSpeedMetersPerSecond = mps;
  }

  public double distanceTraveled(double seconds) {
    double meters = topSpeedMetersPerSecond * seconds;
    return meters * 3.28;
  }

  public double timeToFinish(double feet) {
    double meters = feet / 3.28;
    return meters / topSpeedMetersPerSecond;
  }

  public String toString() {
    return species;
  }
}
