package lab18;

import java.util.ArrayList;
import java.util.List;

import lab12.solutions.Meal;

/**
 * A restaurant's menu.
 */
public class Menu implements Chart {
  private List<Meal> meals;

  public Menu() {
    meals = new ArrayList<>();
  }

  public void addMeal(Meal newMeal) {
    meals.add(newMeal);
  }

  public List<Meal> getMeals() {
    return meals;
  }

  // ========== Chart methods! ==========

  public int getNumRows() {
    // one header row + one row per meal
    return 1 + meals.size();
  }

  public int getNumColumns() {
    // 1. Meal name
    // 2. Meal calories
    // 3. Meal cost
    // 4. Ingredients
    return 4;
  }

  public String getCellContents(int row, int col) {
    // Special case: The header gives labels for each menu item
    if (row == 0) {
      if (col == 0) {
        return "[Meal]";
      } else if (col == 1) {
        return "[Calories]";
      } else if (col == 2) {
        return "[Cost]";
      } else { // col == 3
        return "[Ingredients]";
      }
    }

    // Otherwise, which meal do we print info for?
    Meal meal = meals.get(row - 1);
    if (col == 0) {
      return meal.getName();
    } else if (col == 1) {
      // (convert to a string)
      return "" + meal.totalCalories();
    } else if (col == 2) {
      // (convert to a string)
      return "" + meal.getPrice();
    } else { // col == 3
      return meal.getIngredients().toString();
    }
  }
}
