package lab18;

import java.util.HashSet;

import objects.Food;

import lab12.solutions.Meal;

/**
 * Prints a menu for my restaurant.
 */
public class PrintMyMenu {
  public static void main(String[] args) {
    Menu menu = new Menu();

    // Burgers
    HashSet<Food> burgerIngredients = new HashSet<>();
    burgerIngredients.add(new Food("beef", "meat", 500, 4));
    burgerIngredients.add(new Food("bun", "bread", 300, 2));
    burgerIngredients.add(new Food("cheese", "dairy", 400, 2));
    menu.addMeal(new Meal("Hamburger", 5.95, burgerIngredients));

    // Fries
    HashSet<Food> friesIngredients = new HashSet<>();
    friesIngredients.add(new Food("potatoes", "vegetable", 500, 6));
    friesIngredients.add(new Food("oil", "fats", 1000, 3));
    menu.addMeal(new Meal("Fries", 2.99, friesIngredients));

    // Apple slices
    HashSet<Food> appleSpliceIngredients = new HashSet<>();
    appleSpliceIngredients.add(new Food("apples", "fruit", 100, 5));
    menu.addMeal(new Meal("Apple Slices", 1.99, appleSpliceIngredients));

    // Milkshake
    HashSet<Food> milkshakeIngredients = new HashSet<>();
    milkshakeIngredients.add(new Food("milk", "dairy", 200, 12));
    milkshakeIngredients.add(new Food("ice cream", "dairy", 1000, 4));
    menu.addMeal(new Meal("Milkshake", 4.99, milkshakeIngredients));

    // Kale Chips
    HashSet<Food> kaleChipsIngredients = new HashSet<>();
    kaleChipsIngredients.add(new Food("kale", "vegetable", 50, 12));
    kaleChipsIngredients.add(new Food("salt", "???", 0, 2));
    menu.addMeal(new Meal("Kale Chips", 0.99, kaleChipsIngredients));

    // Print everything out!
    // ===== WRITE YOUR CODE HERE =====
  }
}
