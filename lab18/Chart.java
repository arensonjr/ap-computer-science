package lab18;

/**
 * Represents data that can be charted.
 */
public interface Chart {
  /**
   * Returns the number of rows that should be charted.
   */
  int getNumRows();

  /**
   * Returns the number of columns that should be charted.
   */
  int getNumColumns();

  /**
   * Returns the data in the row'th row and col'th column
   * of the chart (starting from 0).
   *
   * This data could be a column header, or a number, or whatever -- it
   * just needs to be formatted as a string.
   *
   * Multiple calls to this method with the same values for (row, col) should
   * return the same results. For example, if the following code is called:
   *
   *   Chart chart = ...
   *   String a = getCellContents(5, 6);
   *   String b = getCellContents(5, 6);
   *
   * then the values of `a` and `b` must always be the same.
   */
  String getCellContents(int row, int col);
}
