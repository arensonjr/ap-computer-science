package lab18;

import java.awt.Point;

/**
 * Represents one of Earth's Mountains.
 */
public class Mountain implements PointOfInterest {
  public Mountain(String n, String r, int lat, int lon, double h) {
    // Write your code here
  }

  public String getName() {
    // Write your code here
  }

  public String getRange() {
    // Write your code here
  }

  public int getLatitude() {
    // Write your code here
  }

  public int getLongitude() {
    // Write your code here
  }

  public double getHeight() {
    // Write your code here
  }

  public Point getLocation() {
    // Write your code here
  }
}
