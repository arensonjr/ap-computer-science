package lab18;

public class Car implements HasDimensions, HasSpeed {
  private String makeAndModel;
  private double height;
  private double width;
  private double topSpeedMph;

  public Car(String m, double h, double w, double mph) {
    makeAndModel = m;
    height = h;
    width = w;
    topSpeedMph = mph;
  }

  public String getMakeAndModel() {
    return makeAndModel;
  }

  public double getHeight() {
    return height;
  }

  public double getWidth() {
    return width;
  }

  public double getTopSpeedMph() {
    return topSpeedMph;
  }

  public double distanceTraveled(double seconds) {
    double hours = seconds / 60 / 60;
    double miles = topSpeedMph * hours;
    return miles * 5280;
  }

  public double timeToFinish(double feet) {
    double miles = feet / 5280;
    double hours = miles / topSpeedMph;
    return hours * 60 * 60;
  }

  public String toString() {
    return getMakeAndModel();
  }
}
