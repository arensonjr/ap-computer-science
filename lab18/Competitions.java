package lab18;

/**
 * Prints the results of some competitions.
 */
public class Competitions {
  public static void main(String[] args) {
    Car accord = new Car("Honda Accord", 57, 190, 125);
    Car bmw = new Car("BMW Z3", 50, 158, 150);
    Car flintstones = new Car("Flintstones Car", 65, 120, 30);
    oneMileRace(accord, bmw);
    collision(accord, bmw);

    Animal sloth = new Animal("Sloth", 0.076);
    Animal cheetah = new Animal("Cheetah", 29);
    Animal bear = new Animal("Bear", 11);
    Animal human = new Animal("Human", 12.5);
    oneMileRace(accord, sloth);
    oneMileRace(flintstones, cheetah);
    oneMileRace(bear, human);

    Tree oak = new Tree("Oak", 100, 72, 600);
    Tree sapling = new Tree("Sapling", 1, 6, 36);
    collision(accord, oak);
    collision(bmw, sapling);
  }

  /**
   * Prints out the results of a one mile race between two speedy things.
   */
  public static void oneMileRace(HasSpeed left, HasSpeed right) {
    // Write your code here
  }

  /**
   * Prints out the results of a collision between two sizable objects.
   */
  public static void collision(HasDimensions left, HasDimensions right) {
    // Write your code here
  }
}
