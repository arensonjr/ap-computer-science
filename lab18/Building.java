package lab18;

import java.awt.Point;

/**
 * Represents one of the cities of the world.
 */
public class Building implements PointOfInterest {
  public Building(String n, City c, double h, int y) {
    // Write your code here
  }

  public String getName() {
    // Write your code here
  }

  public City getCity() {
    // Write your code here
  }

  public double getHeight() {
    // Write your code here
  }

  public int getYearBuilt() {
    // Write your code here
  }

  public Point getLocation() {
    // Write your code here
  }
}
