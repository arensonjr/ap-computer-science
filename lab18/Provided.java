package lab18;

/**
 * Provided functions for lab 18.
 */
public class Provided {
  /**
   * Prints the contents of a chart to the console.
   *
   * Takes care of all of the fancy formatting details for you, no matter
   * what type of data is in the chart!
   */
  public static void printChart(Chart chart) {
    // Figure out the max width of each column
    int[] colWidths = new int[chart.getNumColumns()];
    for (int col = 0; col < chart.getNumColumns(); col++) {
      for (int row = 0; row < chart.getNumRows(); row++) {
        colWidths[col] =
          Math.max(colWidths[col], chart.getCellContents(row, col).length());
      }
    }

    // Now, print all of the data
    printDivider(colWidths);
    for (int row = 0; row < chart.getNumRows(); row++) {
      // Left border
      System.out.print("| ");

      // ALl of the cell data, with dividers and padding
      for (int col = 0; col < chart.getNumColumns(); col++) {
        String data = chart.getCellContents(row, col);
        System.out.print(data);
        for (int i = data.length(); i < colWidths[col]; i++) {
          System.out.print(' ');
        }
        System.out.print(" | ");
      }

      // Divider between rows
      System.out.println();
      printDivider(colWidths);
    }
  }

  /**
   * Prints a divider between rows.
   */
  private static void printDivider(int[] colWidths) {
    // Left border
    System.out.print("+-");
    for (int i = 0; i < colWidths.length; i++) {
      // Cell length
      for (int j = 0; j < colWidths[i]; j++) {
        System.out.print('-');
      }
      // Border
      if (i < colWidths.length - 1) {
        System.out.print("-+-");
      }
    }
    // Right border
    System.out.println("-+");
  }
}
