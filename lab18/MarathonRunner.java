package lab18;

/**
 * Represents a single racer in a marathon.
 */
public class MarathonRunner implements Comparable<MarathonRunner> {
  public MarathonRunner(String n, int m) {
    // Write your code here
  }

  public String getName() {
    // Write your code here
  }

  public int getElapsedMinutes() {
    // Write your code here
  }

  public int compareTo(MarathonRunner other) {
    // Write your code here
  }

  public String toString() {
    // Write your code here
  }
}
