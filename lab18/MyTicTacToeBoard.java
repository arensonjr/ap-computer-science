package lab18;

/**
 * Prints out a partial game of tic-tac-toe.
 */
public class MyTicTacToeBoard {
  public static void main(String[] args) {
    TicTacToeBoard board = new TicTacToeBoard();
    board.move(0, 0, "X");
    board.move(1, 2, "Y");
    board.move(1, 1, "X");
    board.move(2, 2, "Y");
    board.move(0, 2, "X");
    Provided.printChart(board);
  }
}
