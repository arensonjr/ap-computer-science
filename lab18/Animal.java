package lab18;

/**
 * Represents an animal.
 */
public class Animal implements HasSpeed {
  public Animal(String s, double mps) {
    // Write your code here
  }

  public double distanceTraveled(double seconds) {
    // Write your code here
  }

  public double timeToFinish(double feet) {
    // Write your code here
  }

  public String toString() {
    // Write your code here
  }
}
