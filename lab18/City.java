package lab18;

import java.awt.Point;

/**
 * Represents one of the cities of the world.
 */
public class City implements PointOfInterest {
  public City(String n, String c, int lat, int lon, double p) {
    // Write your code here
  }

  public String getName() {
    // Write your code here
  }

  public String getCountry() {
    // Write your code here
  }

  public int getLatitude() {
    // Write your code here
  }

  public int getLongitude() {
    // Write your code here
  }

  public double getPopulationInMillions() {
    // Write your code here
  }

  public Point getLocation() {
    // Write your code here
  }
}
